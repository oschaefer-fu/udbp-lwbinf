--tiefergehende Kommentare in modell/query<nn>.txt

-- 01 Welche Schüler heißen Mohammed?
SELECT * FROM schueler WHERE vname='Mohammed';

-- 02 Welche Schüler heißen Mohammed oder Muhammad oder Muhamed oder Mohamed?
SELECT * FROM schueler WHERE vname='Mohammed' or vname='Muhammad' OR vname='Muhamed' OR vname='Mohamed';

-- 03 Welche Schüler heißen Benja und sind männlich?
SELECT * FROM schueler WHERE vname='Benja' AND geschlecht=1;

-- 04 Wie viele Schülerinnen heißen Benja und sind weiblich?
SELECT COUNT(*) FROM schueler WHERE vname='Benja' and geschlecht=0 GROUP BY vname;

-- 05 Welche Schüler gehören/gehörten in das Team Rainbowkids?
SELECT vname,nname FROM schueler NATURAL JOIN istinteam WHERE tname='Rainbowkids';

-- 06 Name des SchülerIn, der/die bei der Einzeldisziplin „3000mLauf“ den 3. Platz erreicht hat
SELECT vname,nname FROM (SELECT vname,nname,punkte FROM (SELECT disid FROM disziplin WHERE dname='3000mLauf') AS lauf NATURAL JOIN machted NATURAL JOIN schueler ORDER BY punkte DESC LIMIT 3) AS erg ORDER BY punkte ASC limit 1;

-- 07 Welche SchülerInnen haben über einer gewissen Punktezahl im Kugelstoßen?
SELECT vname, nname, jahrgang, lerngruppe,punkte FROM machted NATURAL JOIN klasse NATURAL JOIN schueler WHERE disid = (SELECT disid FROM disziplin WHERE dname='Kugelstossen') AND punkte > 2;

-- 08 Welche Schüler werden/wurden von welchem Lehrer wie oft beobachtet?
SELECT lname,vname,nname, count(*) FROM ( SELECT lid,schid FROM istinteam NATURAL JOIN team NATURAL JOIN istzuschauer UNION ALL SELECT lid,schid FROM machted NATURAL JOIN istzuschauer ) AS erg NATURAL JOIN (SELECT lname,lid FROM lehrer) AS lehrerred NATURAL JOIN (SELECT schid, vname,nname FROM schueler) AS schuelerred GROUP BY erg.lid,erg.schid, schuelerred.vname, schuelerred.nname, lehrerred.lname ORDER BY count DESC;

-- 09 Welche SchülerInnen haben Die Disziplin Kugelstoßen gemacht?
SELECT vname, nname, jahrgang, lerngruppe,punkte FROM machted NATURAL JOIN klasse NATURAL JOIN schueler WHERE disid = (SELECT disid FROM disziplin WHERE dname='Kugelstossen') AND punkte > 0;

-- 10 Welcher Lehrer wurden fälschlicherweise für eine Aufsicht eingeteilt und zu welcher?
SELECT lname,dname FROM disziplin NATURAL JOIN lehrer WHERE istklassenlehrer=true;

-- 11 Welche Disziplin hat Lehrer xy als Zuschauer besucht?
SELECT dname FROM lehrer NATURAL JOIN istzuschauer NATURAL JOIN (SELECT disid,dname FROM disziplin) AS tmp WHERE lname='Myrach';

-- 12 Welcher Lehrer ist/war Zuschauer beim Fußball
SELECT lname FROM istzuschauer NATURAL JOIN lehrer NATURAL JOIN (select dname, disid from disziplin) AS tmp WHERE dname='Fussball';

-- 13 Bei welcher der Disziplinen haben die meisten Lehrer zugesehen?
SELECT dname FROM disziplin NATURAL JOIN (SELECT count(*),disid FROM istzuschauer GROUP BY disid ORDER BY count DESC LIMIT 1) AS tmp; 

-- 14 Welche Lehrer beaufsichtigen das Kugelstoßen
SELECT lname FROM disziplin NATURAL JOIN lehrer WHERE dname='Kugelstossen';

-- 15 Welche Klasse hat im Kugelstoßen am besten abgeschnitten?
 SELECT klid,jahrgang,lerngruppe,SUM(punkte) FROM (SELECT * FROM machted WHERE disid = (SELECT disid FROM disziplin WHERE dname='Kugelstossen')) AS tmp NATURAL JOIN (SELECT * FROM (SELECT schid,klid,jahrgang,lerngruppe FROM schueler NATURAL JOIN klasse) AS foo) AS bar GROUP BY klid,jahrgang,lerngruppe ORDER BY sum DESC LIMIT 1;
 
 -- 16 Aus welchen Klassen setzt sich das Team  „Rainbowkids“ zusammen?
SELECT DISTINCT jahrgang,lerngruppe FROM istinTeam NATURAL JOIN klasse NATURAL JOIN schueler WHERE tname='Rainbowkids';

-- 17 Welches Team hat das Fußballtunier gewonnen?
SELECT tname FROM team NATURAL JOIN disziplin WHERE dname='Fussball' ORDER BY punkte DESC LIMIT 1;

-- 18 An welcher Teamdisziplin nehmen die meisten Mannschaften teil?
SELECT dname,count FROM (SELECT disid,count(*) FROM team GROUP BY disid ORDER BY count DESC LIMIT 1) AS tmp NATURAL JOIN disziplin;

-- 19 Wie viele Kugeln sind für das Kugelstoßen noch im Bestand, wenn die Disziplin Kugelstoßen stattfindet?
SELECT (bestand - anzahl) AS rest FROM benoetigt NATURAL JOIN equipment NATURAL JOIN disziplin WHERE dname='Kugelstossen' AND eqname='Kugeln';

-- 20 Welches Equipment wird für die Disziplin „Fussball“ benötigt?
SELECT eqname FROM disziplin NATURAL JOIN benoetigt NATURAL JOIN equipment WHERE dname='Fussball';