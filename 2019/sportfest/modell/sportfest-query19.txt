Wie viele Kugeln sind für das Kugelstoßen noch im Bestand, wenn die Disziplin Kugelstoßen stattfindet?
SELECT (bestand - anzahl) AS rest FROM benoetigt NATURAL JOIN equipment NATURAL JOIN disziplin WHERE dname='Kugelstossen' AND eqname='Kugeln';

Kommentar: Nutzung von arithmetischen Operationen.