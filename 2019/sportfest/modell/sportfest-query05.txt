Welche Schüler gehören/gehörten in das Team Rainbowkids?
SELECT vname,nname FROM schueler NATURAL JOIN istinteam WHERE tname='Rainbowkids';

Kommentar: Der natural join steht im Vordergrund. Die Abfrage fordert nicht explizit die Projektion.