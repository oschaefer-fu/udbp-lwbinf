Legende: <Schlüssel>,Fremdschlüssel*

‣ Schüler (VName, NName, Geschlecht, <SchID>, KlID*,LID*)
Integritätsbedingungen: Geschlecht: {0,1,2}

‣ Klasse (Jahrgang, Lerngruppe, <KlID>)
Integritätsbedingungen: Jahrgang: {7,8,9,10}, Lerngruppe {'a','b','c','d','e','f'}

‣ Lehrer ([LName], istKlassenlehrer, <LID>)

‣ Team (<TName>, Punkte, DisID*)

‣ Disziplin (<DName>, istTeamDis, DisID, LID*)

‣ Equipment (<EqName>, Bestand, EqID)

‣ istinT (<SchID>, <TName>)

‣ istZuschauer (<LID>, <DisID>)

‣ benoetigt (<DisID>, <EqID>, Anzahl)

‣ machtED (<SchID>, <DisID>, Punkte)

