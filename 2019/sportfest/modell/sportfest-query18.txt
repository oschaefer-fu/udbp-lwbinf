An welcher Teamdisziplin nehmen die meisten Mannschaften teil?
SELECT dname,count FROM (SELECT disid,count(*) FROM team GROUP BY disid ORDER BY count DESC LIMIT 1) AS tmp NATURAL JOIN disziplin;

Kommentar: Für das Anfrageergebnis wird zunächst nur die ID der Disziplin ermittelt. Via natural join wird der ID der Name der Disziplin zugeordnet.