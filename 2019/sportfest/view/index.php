<!doctype html>
<html lang="de">

<head>
    <link rel="stylesheet" type="text/css" href="lib/normalize.css">
    <link rel="stylesheet" type="text/css" href="lib/index.css">
    <title>DBSB Sportfest 2019</title>
</head>

<body>
    <h1>Sportfest 2019</h1>
    <!-- Schüler-->
    <form action="erg.php" method="get">
        <div class="container">
            <span class="label">
                Vorname:
            </span>
            <input type="text" name="vname">
        </div>
        <div class="container">
            <span class="label">
                Nachname:
            </span>
            <input type="text" name="nname">
        </div>
        <div class="container">
            <span class="label">
            </span>
            <input type="submit" value="OK">
        </div>
    </form>
</body>

</html>
