<!doctype html>
<!--
select vname,nname,geschlecht,jahrgang,lerngruppe,dname,punkte from schueler natural join machtED natural join (select klid,jahrgang,lerngruppe from klasse) as foo natural join (select dname,disid from disziplin) as tmp where vname='Sascha' and nname='Fokken';
-->
<html lang="de">

<head>
    <link rel="stylesheet" type="text/css" href="lib/normalize.css">
    <link rel="stylesheet" type="text/css" href="lib/erg.css">
    <title>DBSB Sportfest 2019</title>
</head>

<?php
try{
	$con = new PDO('pgsql:dbname=lewein','lewein','niewel');
	
	$nname = $_GET['nname'];
	$vname = $_GET['vname'];

	$list = array('nname' =>$nname,'vname'=>$vname);
		
	$erg = $con->query("SELECT vname,nname,geschlecht,jahrgang,lerngruppe,dname,punkte FROM schueler NATURAL JOIN machtED NATURAL JOIN (SELECT klid,jahrgang,lerngruppe FROM klasse) AS foo NATURAL JOIN (SELECT dname,disid FROM disziplin) AS tmp WHERE VName='$vname' and NName='$nname'");
		
	$erg->execute($list);
	
}catch(PDOException $e){
	echo "Fehler:".htmlspecialchars($e -> getMessage());
	die();
}
?>

<body>
    <h1>Sportfest 2019 Anfrageergebnis</h1>
    <table>
        <tr>
            <th>Vorname</th>
            <th>Nachname</th>
            <th>Geschlecht</th>
            <th>Klasse</th>
            <th>Disziplin</th>
            <th>Punktzahl</th>
        </tr>
        
	<?php
		foreach($erg as $row){
			echo"<tr>".
			"<td>".$row['vname']."</td>".
			"<td>".$row['nname']."</td>".
			"<td>".$row['geschlecht']."</td>".
			"<td>".$row['jahrgang'].$row['lerngruppe']."</td>".
			"<td>".$row['dname']."</td>".
			"<td>".$row['punkte']."</td>".
			"</tr>";
		}
		echo '</table>';
		$con=null;
		?>
  
</body>

</html>
