FUNKTIONALE ABHÄNGIKEITEN

VEREIN
 vid |       vname       |    strasse    | hnr |  plz  |  stadt   | vtelefon | liga 
-----+-------------------+---------------+-----+-------+----------+----------+------
   1 | Borussia Dortmund | Rheinlanddamm | 207 | 44137 | Dortmund | 23190200 |    1
(1 Zeile)
Primärschlüssel: vid
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.

LAENDER
 isocode |    lname    
---------+-------------
 AR      | Argentinien
(1 Zeile)
Primärschlüssel: isocode
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.

SPIELER
 sid | snachname | svorname |    sgeb    | snatio | groesse | gewicht | spielfuss | marktwert | technik | ausdauer | physis | mentalitaet | staerke 
-----+-----------+----------+------------+--------+---------+---------+-----------+-----------+---------+----------+--------+-------------+---------
   1 | Reus      | Marco    | 1983-07-20 | DE     |     188 |      74 | b         |   8.5e+07 |      10 |        9 |     10 |          10 |       9
(1 Zeile)
Primärschlüssel: sid
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.

TRAINER
 tid | tname | tvorname |    tgeb    | tnatio | ttelefon | system  
-----+-------+----------+------------+--------+----------+---------
   1 | Favre | Lucien   | 1957-11-02 | CH     | 176111   | 4-2-3-1
(1 Zeile)
Primärschlüssel: tid
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.

POSITION
 pid | pname 
-----+-------
   1 | TW
(1 Zeile)
Primärschlüssel: pid
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.

SPIEL
 spid | heim | gast | toreheim | toregast | datum 
------+------+------+----------+----------+-------
    1 |    2 |    1 |        0 |        5 | 
(1 Zeile)
Primärschlüssel: spid
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.

SPIELERVERTRAG
 sid | vid |    von     |    bis     
-----+-----+------------+------------
   2 |   1 | 2010-07-01 | 2021-06-30
(2 Zeilen)
Primärschlüssel: sid,vid
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.

TRAINERVERTRAG
 tid | vid |    von     |    bis     
-----+-----+------------+------------
   1 |   1 | 2018-07-01 | 2020-06-30
(1 Zeile)
Primärschlüssel: tid,vid
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.

SPIELERPOSITION
 sid | auspraegung | pid 
-----+-------------+-----
   1 |           1 |   9
   1 |         0.5 |   6
   1 |         0.5 |  10
(3 Zeilen)
Primärschlüssel: sid,pid
Abhängigkeiten: Alle Attribute hängen funktional vom Primärschlüssel ab.
