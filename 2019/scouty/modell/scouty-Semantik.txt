INTEGRITÄTSBEDINGUNGEN

# Ein Spieler kann zu jedem Zeitpunkt in keinem oder genau einem Verein spielen.
# Ein Spieler dieser Datenbank besitzt nur eine Nationalität. Bei Mehrfach-Nationalitäten ist entscheidend, für welche Nationalmannschaft er spielt.
# Zu den Spielerpositionen: Ein Spieler kann mindestens eine "Hauptposition" (Ausprägung: 1.0) und optional weitere "Nebenpositionen" (Ausprägung: 0.5) bespielen.
# Ein Trainer kann zu jedem Zeitpunkt keinen oder genau einen Verein trainieren.
