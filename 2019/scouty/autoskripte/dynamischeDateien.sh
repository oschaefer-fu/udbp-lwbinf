# modell/scouty-Anfragen.txt
cp ../sql/scouty-query.sql ../modell/scouty-Anfragen.txt

# Datenbank scouty einrichten
psql -f sql_resetScouty.sql

# modell/scouty-Daten.txt
rm -f ../modell/scouty-Daten.txt
for t in Spieler Verein Trainer Laender Position Spiel Spielervertrag Trainervertrag Spielerposition;
do echo $t >> ../modell/scouty-Daten.txt;
echo "SELECT * FROM $t" | psql -d scouty >> ../modell/scouty-Daten.txt;
done

# modell/scouty-Daten.csv und separate csv-Dateien in Unterordner
rm -f ../modell/scouty-Daten.csv
for t in Spieler Verein Trainer Laender Position Spiel Spielervertrag Trainervertrag Spielerposition;
do
echo "SELECT * FROM $t" | psql -d scouty -P format=unaligned -P fieldsep=\, > ../modell/scouty-Daten_separate-csv/scouty-$t.csv;
echo "SELECT * FROM $t" | psql -d scouty -P format=unaligned -P fieldsep=\,>> ../modell/scouty-Daten.csv;
done

# sql/scouty-query.ans
psql -f ../sql/scouty-query.sql -a > ../sql/scouty-query.ans

# relational/scouty-query-ra.ans
	#in relational-Ordner wechseln
	cd ../relational/

./sql2ddl.sh
echo -e "/p $PWD/Create-scouty.ddl \n /p $PWD/Insert-scouty-data.ddl \n /log $PWD/scouty-query-ra.ans \n /ra \n /p $PWD/scouty-query.ra /log" | des

	#zurueck in autoskript-Ordner
	cd ../autoskripte
