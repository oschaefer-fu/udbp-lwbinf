/* Wir arbeiten in einer separaten Datenbank. */
CREATE DATABASE scouty;

/* scouty zur aktuellen Datenbank machen. */
\c scouty

/* Tabellen anlegen */
CREATE TABLE Verein (
    vid SERIAL PRIMARY KEY,
    VName VARCHAR(50) NOT NULL,
    Strasse VARCHAR(50) NOT NULL,
    HNr VARCHAR(10) NOT NULL,
    Plz Integer NOT NULL,
    Stadt VARCHAR(50) NOT NULL,
    VTelefon VARCHAR(20) NOT NULL,
    Liga INTEGER CHECK (Liga in (1, 2, 3, 4, 5))
);

CREATE TABLE Position (
    pid SERIAL PRIMARY KEY,
    PName CHAR(2) NOT NULL
);

CREATE TABLE Laender (
    isocode CHAR(2) PRIMARY KEY,
    LName VARCHAR(50) NOT NULL
);

CREATE TABLE Trainer (
    tid SERIAL PRIMARY KEY,
    TName VARCHAR(50) NOT NULL,
    TVorname VARCHAR(50) NOT NULL,
    TGeb DATE NOT NULL,
    TNatio CHAR(2) REFERENCES Laender (isocode) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL,
    TTelefon VARCHAR(20) NOT NULL,
    System VARCHAR(20) NOT NULL
);

CREATE TABLE Spieler (
    sid SERIAL PRIMARY KEY,
    SNachname VARCHAR(50) NOT NULL,
    SVorname VARCHAR(50) NOT NULL,
    SGeb DATE NOT NULL,
    SNatio CHAR(2) REFERENCES Laender (isocode) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL,
    Groesse INTEGER NOT NULL,
    Gewicht INTEGER NOT NULL,
    Spielfuss CHAR NOT NULL CHECK (Spielfuss IN ('l', 'r', 'b')),
    Marktwert INTEGER NOT NULL,
    Technik INTEGER NOT NULL CHECK (Technik BETWEEN 0 AND 10),
    Ausdauer INTEGER NOT NULL CHECK (Ausdauer BETWEEN 0 AND 10),
    Physis INTEGER NOT NULL CHECK (Physis BETWEEN 0 AND 10),
    Mentalitaet INTEGER NOT NULL CHECK (Mentalitaet BETWEEN 0 AND 10),
    Staerke INTEGER NOT NULL CHECK (Staerke BETWEEN 0 AND 10)
);

CREATE TABLE Spiel (
    spid SERIAL PRIMARY KEY,
    Heim INTEGER REFERENCES Verein (vid) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL,
    Gast INTEGER REFERENCES Verein (vid) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL,
    ToreHeim INTEGER NOT NULL CHECK (ToreHeim >= 0) DEFAULT 0,
    ToreGast INTEGER NOT NULL CHECK (ToreGast >= 0) DEFAULT 0,
    Datum DATE 
);

CREATE TABLE Spielervertrag (
	sid INTEGER NOT NULL REFERENCES Spieler (sid) ON DELETE RESTRICT ON UPDATE CASCADE,
	vid INTEGER NOT NULL REFERENCES Verein (vid) ON DELETE RESTRICT ON UPDATE CASCADE,
	von DATE NOT NULL,
	bis DATE NOT NULL CHECK (von < bis),
	
	PRIMARY KEY (sid,vid,von)
);

CREATE TABLE Trainervertrag (
	tid INTEGER NOT NULL REFERENCES Trainer (tid) ON DELETE RESTRICT ON UPDATE CASCADE,
	vid INTEGER NOT NULL REFERENCES Verein (vid) ON DELETE RESTRICT ON UPDATE CASCADE,
	von DATE NOT NULL,
	bis DATE NOT NUll CHECK (von < bis),
	
	PRIMARY KEY (vid,tid,von)
);
 
CREATE TABLE Spielerposition (
	sid INTEGER NOT NULL REFERENCES Spieler (sid) ON DELETE RESTRICT ON UPDATE CASCADE,
	Auspraegung REAL NOT NULL CHECK (Auspraegung BETWEEN 0.0 AND 1.0),
	pid INTEGER NOT NULL REFERENCES Position (pid) ON DELETE RESTRICT ON UPDATE CASCADE,
	
	PRIMARY KEY (sid,pid)
);


--
-- Kompakte Spieleruebersicht
--
CREATE VIEW ViewSpieler AS 
    SELECT  sid, SNachname, SVorname, PName, Auspraegung, VName,
	    SGeb, SNatio, Groesse, Gewicht, Spielfuss, Marktwert,
	    Technik, Ausdauer, Physis, Mentalitaet, Staerke
	    FROM Spieler NATURAL JOIN Spielervertrag NATURAL JOIN Verein NATURAL JOIN Spielerposition NATURAL JOIN Position;


--
-- SpielerIDs mit ihren PositionsAuspraegungen
--
CREATE VIEW ViewSpielerPositionen AS
    SELECT DISTINCT S.sid,
    COALESCE(TW.Auspraegung, 0.0) AS AuspraegungTW,
    COALESCE(LV.Auspraegung, 0.0) AS AuspraegungLV,
    COALESCE(RV.Auspraegung, 0.0) AS AuspraegungRV,
    COALESCE(IV.Auspraegung, 0.0) AS AuspraegungIV,
    COALESCE(DM.Auspraegung, 0.0) AS AuspraegungDM,
    COALESCE(LA.Auspraegung, 0.0) AS AuspraegungLA,
    COALESCE(ZM.Auspraegung, 0.0) AS AuspraegungZM,
    COALESCE(RA.Auspraegung, 0.0) AS AuspraegungRA,
    COALESCE(OM.Auspraegung, 0.0) AS AuspraegungOM,
    COALESCE(ST.Auspraegung, 0.0) AS AuspraegungST
    FROM Spieler AS S
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS TW ON S.sid = TW.sid AND TW.Pname = 'TW'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS LV ON S.sid = LV.sid AND LV.Pname = 'LV'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS RV ON S.sid = RV.sid AND RV.Pname = 'RV'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS IV ON S.sid = IV.sid AND IV.Pname = 'IV'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS DM ON S.sid = DM.sid AND DM.Pname = 'DM'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS LA ON S.sid = LA.sid AND LA.Pname = 'LA'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS ZM ON S.sid = ZM.sid AND ZM.Pname = 'ZM'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS RA ON S.sid = RA.sid AND RA.Pname = 'RA'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS OM ON S.sid = OM.sid AND OM.Pname = 'OM'
    LEFT JOIN (Spielerposition NATURAL JOIN POSITION) AS ST ON S.sid = ST.sid AND ST.Pname = 'ST'
    ;

