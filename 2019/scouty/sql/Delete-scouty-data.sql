/*Scouty zur aktuellen Datenbank machen*/
\c scouty

/*Daten aus den Tabellen löschen*/
DELETE FROM Spieler *;
DELETE FROM Verein *;
DELETE FROM Trainer *;
DELETE FROM Laender *;
DELETE FROM Position *;
DELETE FROM Spiel *;
DELETE FROM Spielervertrag *;
DELETE FROM Trainervertrag *;
DELETE FROM Spielerposition *;
