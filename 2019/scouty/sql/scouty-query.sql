\c scouty

--1. Vereinsabfrabfrage (gesamte Vereinstabelle):
SELECT * FROM Verein;

--2. Vereinsabfrage (nur Dynamo Dresden):
SELECT * FROM Verein
		WHERE VName = 'SG Dynamo Dresden';

--3. Vereinsabfrage (nur die Telefonnummer der Bayern):
SELECT VName, VTelefon FROM Verein
		WHERE VName = 'Bayern München';

--4. Trainer mit Verein:
SELECT TName,TVorname,VName
		FROM Trainer NATURAL JOIN Trainervertrag NATURAL JOIN Verein; 

--5. Geburstdatum  von Christial Fiel (Trainer von Dynamo Dresden):
SELECT TName, TVorname, TGeb
		FROM Trainer
		WHERE TName = 'Fiel';

--6. Spieler mit Verein:
SELECT  SNachname,SVorname, VName
	    FROM Spieler NATURAL JOIN Spielervertrag NATURAL JOIN Verein;

--7.alle Spieler, die nicht deutscher Herkunft sind, geordnet nach Nation
SELECT 	SNachname, SVorname, SNatio
		FROM Spieler 
		WHERE NOT (SNatio = 'DE')
		ORDER BY SNatio;

--8.Anzahl der verschiedenen Nationen aller Spieler
SELECT COUNT (DISTINCT SNatio) AS "Anzahl der Nationen der Spieler"
		FROM Spieler;

--9. Alle Spieler deren Nachname mit "B" anfangen
SELECT SNachname, SVorname, VName
		FROM Spieler NATURAL JOIN Spielervertrag NATURAL JOIN Verein
		WHERE SNachname LIKE 'B%'; 

--10. Spieler mit Position, geordnet nach Position:
SELECT  SNachname,SVorname, PName
	    FROM Spieler NATURAL JOIN Spielerposition NATURAL JOIN Position
		Order By PName;

--11. Spieler mit Hauptposition (Auspraegung = 1.0), Vereinsname und Vertragslaufzeit, geordnet nach Namen:
SELECT  SNachname,SVorname, PName, VName, von AS Vertragsbeginn ,bis AS Vertragsende
	    FROM Spieler NATURAL JOIN Spielerposition Natural Join Position NATURAL JOIN Spielervertrag NATURAL JOIN Verein 
		WHERE Auspraegung = 1.0
		Order By SNachname;
		
--12. nur Torwartposition:
SELECT  SNachname,SVorname, PName
	    FROM Spieler NATURAL JOIN Spielerposition Natural Join Position
		WHERE PName = 'TW'		
		Order By SNachname;

--13. nur Stürmerposition:
SELECT  SNachname,SVorname, PName
	    FROM Spieler NATURAL JOIN Spielerposition Natural Join Position
		WHERE PName = 'ST'		
		Order By SNachname;

--14. alle Stürmer von Eintracht Frankfurt:
SELECT SNachname, SVorname, PName, VName
		FROM Spieler NATURAL JOIN Spielerposition Natural JOIN Position NATURAL JOIN Spielervertrag NATURAL JOIN Verein
		WHERE VName = 'Eintracht Frankfurt' AND PName = 'ST';

--15. alle Spieler deren Verträge am 30.06.2019 enden
SELECT SNachname, SVorname, VName, bis AS "Vertragsende"
		FROM Spieler Natural JOIN Spielervertrag NATURAL JOIN Verein
		WHERE bis = '2019-06-30';

--16. alle Spieler deren Verträge nicht 2019 enden
SELECT SNachname, SVorname, VName, bis AS "Vertragsende"
		FROM Spieler Natural JOIN Spielervertrag NATURAL JOIN Verein
		WHERE bis > '2019-06-30';
		
--17. alle Spieler deren Verträge 2020 und 2021 enden
SELECT SNachname, SVorname, VName, bis AS "Vertragsende"
		FROM Spieler Natural JOIN Spielervertrag NATURAL JOIN Verein
		WHERE bis BETWEEN '2020-06-30' AND '2021-06-30';

--18. alle Spieler, mit ihrem jeweiligen Verein und all ihren Positionen (Sortiert nach der Auspraegung der jeweiligen Position):
SELECT sid, SVorname, SNachname, VName, ARRAY_AGG(PName ORDER BY Auspraegung DESC) AS Positionen
		FROM ViewSpieler 
		GROUP BY sid, SVorname, SNachname, VName;

--19. Spiele eines Vereins (Borussia Dortmund) //AFB 5:
SELECT H.VName,G.VName, ToreHeim,ToreGast FROM Spiel LEFT JOIN Verein AS H ON H.vid = Heim LEFT JOIN Verein AS G ON G.vid = Gast 
		WHERE H.VName = 'Borussia Dortmund' OR G.VName = 'Borussia Dortmund';
		
--20. alle Spieler, die zentrales Mittelfeld oder Links Außen spielen können
SELECT sid, SVorname, SNachname, VName, ARRAY_AGG(PName ORDER BY Auspraegung DESC) AS Positionen 
		FROM Spieler NATURAL JOIN Spielerposition Natural JOIN Position NATURAL JOIN Verein NATURAL JOIN Spielervertrag
		WHERE PName = 'ZM' OR PName = 'LA'
		GROUP BY sid, SVorname, SNachname, VName;




