<?php
/**
 * Herstellen der Datenbankverbindung aus den Credentials in den Feldern in
 * der $_SESSION-Vaiablen.
 * Dort wird je ein Feld dbnutzer, dbnutzer_passwd, dbhost, dbname und dbservertyp
 * erwartet.
 * Es wird eine Variable $con angelegt, die ein PDO-Handle zur Datenbank ist.
 * Es wird eingestellt, dass auf Fehler mit Exceptions reagiert wird.
 * php version 7.0.33
 *
 * @category Minidatenbank_Für_Unterrichtszwecke
 * @package  Scouty
 * @author   Frank Huth <frank_huth@gmx.de>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @version  GIT: $Id$
 * @link     ???
 */


$db_location = $_SESSION['dbservertyp'] . ':host=' . $_SESSION['dbhost'] .
';dbname=' . $_SESSION['dbname'];
    //statusSchreiben($db_location);
    $con = new PDO(
        $db_location,
        $_SESSION['dbnutzer'],
        $_SESSION['dbnutzer_passwd']
    );
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>

