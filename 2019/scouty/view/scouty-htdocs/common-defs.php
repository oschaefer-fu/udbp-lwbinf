<?php
/**
 * Hier können Dinge definiert werden, die an verschiedenen Stellen gebraucht
 * werden.
 * php version 7.0.33
 *
 * @category Minidatenbank_Für_Unterrichtszwecke
 * @package  Scouty
 * @author   Frank Huth <frank_huth@gmx.de>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @version  GIT: $Id$
 * @link     ???
 */
?>

<link rel="stylesheet" type="text/css" href="scouty.css" />

<?php
// jeden Fehler und alle Komentare bei der Ausführung melden
error_reporting(E_ALL);
// error_reporting(E_ALL & ~E_NOTICE);
  
/**
 * Unbelegte Sessionvariablen-Arrayelemente beschreiben.
 * Belegte Arrayelemente werden nicht überschrieben.
 *
 * @param string $vn  Arrayelementname der belegt werden soll.
 * @param string $inh Wert, der in das Arrayelement eingetragen werden soll.
 *
 * @return void
 */
function presetS($vn, $inh)
{
    if (!isset($_SESSION[$vn])) {
        $_SESSION[$vn] = $inh;
    }
}


/**
 * Sessionvariablen-Arrayelement mit gegebenem Namen liefern.
 *
 * @param string $vn Elementname aus der Sessionvariable
 *
 * @return string Inhalt des Elementes.
 */
function getS($vn)//: string
{
    return $_SESSION[$vn];
}


/**
 *  Übernehmen eines gelieferten Formularfeldinhaltes unter dem gleichen
 *  Arrayelementnamen in die Sessionvariable.
 *
 *  @param string $vn Arrayelementname
 *
 *  @return void
 */
function post2session($vn)
{
    if (isset($_POST[$vn])) {
        $_SESSION[$vn] = $_POST[$vn];
    }
}

/**
 *  In eine Statuszeile schreiben.
 *
 *  @param string $m Zu schreibende Nachricht.
 *
 *  @return void
 */
function statusSchreiben($m)
{
    echo '<div class=statusLine> ' . $m . '</div>';
}

?>

