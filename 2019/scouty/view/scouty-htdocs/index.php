<?php
/**
 * Diese Datei wird als erstes Aufgerufen und verzweigt nur zum Loginbildschirm.
 * php version 7.0.33
 *
 * @category Minidatenbank_Für_Unterrichtszwecke
 * @package  Scouty
 * @author   Frank Huth <frank_huth@gmx.de>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @version  GIT: $Id$
 * @link     ???
 */

session_start();        // datenspeicher z.B. fuer login einschalten
//session_unset();        // Mr. Propper
//session_destroy();    // bei logout aufrufen

header('Location: login.php');
?>
