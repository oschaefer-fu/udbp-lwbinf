<?php
/**
 * Abfragen der Datenbankzugangsdaten in einem Formular.
 * Es können auch Defaults vorgegeben werden, die einen Nutzernamen,
 * ein Nutzerpasswort, den Datenbankserver, den Datenbankhost und den
 * Datenbanknamen vorgeben.
 * php version 7.0.33
 *
 * @category Minidatenbank_Für_Unterrichtszwecke
 * @package  Scouty
 * @author   Frank Huth <frank_huth@gmx.de>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @version  GIT: $Id$
 * @link     ???
 */

session_start();
?>
<!DOCTYPE html>
<html>
<?php
require 'common-defs.php';

try {
    // Sessionweite Voreinstellungen:
    /* ein Beispiel *
    presetS('dbnutzer', 'frank');
    presetS('dbnutzer_passwd', '');
    presetS('dbhost', 'localhost');
    //presetS('dbhost', 'franks-himbeertorte');

    /* und mit lewein */
    presetS('dbnutzer', 'lewein');
    presetS('dbnutzer_passwd', 'niewel');
    //presetS('dbnutzer_passwd', '');
    //presetS('dbhost', 'franks-himbeertorte');
    //presetS('dbhost', '192.168.2.24');
    presetS('dbhost', 'localhost');

    /* Keine Vorurteile zum Nutzer *
    presetS('dbnutzer', '');
    presetS('dbnutzer_passwd', '');
    **/

    presetS('dbname', 'scouty');
    presetS('dbservertyp', 'pgsql');

    print_r($_SESSION);

    //if (isset($_POST)) {
        post2session('dbservertyp');
        post2session('dbhost');
        post2session('dbname');
        post2session('dbnutzer');
        post2session('dbnutzer_passwd');

        require 'conn-inc.php';     // probeverbindung aufbauen

        header('Location: common-navbar.php');
    //}
} catch(PDOException $e) {
    statusSchreiben($e->getMessage());
}

?>

<head>
  <title>scouty--Datenbankzugang</title>
</head>

<body>
    <h1>Datenbankserver und -identität</h1>
    <form name="login" method="POST"><!--action='login.php'-->
        <table>
            <tr>
                <td>Nutzername:</td>
                <td><input type="text" name="dbnutzer" value="<?php echo getS('dbnutzer'); ?>"></td>
            </tr>
            <tr>
                <td>Passwort:</td>
                <td><input type="password" name="dbnutzer_passwd" value="<?php echo getS('dbnutzer_passwd'); ?>"></td>
            </tr>
            <tr>
                <td>Datenbankname:</td>
                <td><input type="text" name="dbname" value="<?php echo getS('dbname'); ?>"></td>
            </tr>
            <tr>
                <td>Server: </td>
                <td><input type="text" name="dbhost" value="<?php echo getS('dbhost'); ?>"></td>
            </tr>
            <tr>
                <td><input type="hidden" name="dbservertyp" value="<?php echo getS('dbservertyp'); ?>"/></td>
                <td><input type="submit" name="abschicken" value="Anmelden"></td>
            </tr>
        </table>
    </form>
</body>
</html>

