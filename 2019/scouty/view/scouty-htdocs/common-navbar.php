<?php
/**
 * Das ist ein Includefile, was auf jede betreffende Seite das Menue zaubern soll,
 * mit dem man die verschiedenen Abfrageseiten anspringen kann.
 * php version 7.0.33
 *
 * @category Minidatenbank_Für_Unterrichtszwecke
 * @package  Scouty
 * @author   Frank Huth <frank_huth@gmx.de>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @version  GIT: $Id$
 * @link     ???
 */
?>
<!DOCTYPE html>
<html>

<link rel="stylesheet" type="text/css" href="scouty.css" />

<body>
<title>Scouty</title>
<!--h1>Scouty</h1-->
<nav>
<a href="spielerliste_simpel.php">Spielerliste simpel</a>
<a href="spielerliste_suchen.php">Spielerliste mit Suchkriterium</a>
<a href="spielerliste.php">Spielerliste Mit Aggregat</a>
<a href="spielerscout.php">Spielerscout</a>
</nav>
</body>
</html>
