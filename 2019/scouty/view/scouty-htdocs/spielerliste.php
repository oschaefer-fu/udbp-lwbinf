<?php
/**
 * Abfrageformular bezüglich der Spieler.
 * php version 7.0.33
 *
 * @category Minidatenbank_Für_Unterrichtszwecke
 * @package  Scouty
 * @author   Frank Huth <frank_huth@gmx.de>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @version  GIT: $Id$
 * @link     ???
 */

 session_start();
?>
<!DOCTYPE html>
<html>
<body>
<title>Scouty Spielerliste</title>
<h1>Scouty Spielerliste</h1>
<?php
require 'common-defs.php';
require 'common-navbar.php';
echo '<br>';
try {
    include 'conn-inc.php';      // Datenbank anmelden

    $Frage = $con->prepare(
        'SELECT' .
        ' sid,' .
        ' SVorname,' .
        ' SNachname,' .
        ' VName,' .
        ' STRING_AGG(CONCAT(PName,' . "'(', AUSPRAEGUNG, ')'), ',' ORDER BY Auspraegung DESC) AS Positionen," .
        ' EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM SGeb) AS Alter, Marktwert' .
        ' FROM ViewSpieler' .
        ' GROUP BY sid, SVorname, SNachname, VName, Alter, Marktwert' .
        ' ORDER BY SNachname, SVorname;');
    $Frage->execute();
    $Erg = $Frage->setFetchMode(PDO::FETCH_ASSOC);
    echo '<table>';
    echo '<th>Vorname</th><th>Nachname</th><th>Verein</th><th>Positionen</th><th>Alter</th><th>Marktwert</th></tr>';
    foreach ($Frage->fetchAll() as $n => $v) {
        echo '<tr>';
        echo '<td>' . $v['svorname'] . '</td>';
        echo '<td>' . $v['snachname'] . '</td>';
        echo '<td>' . $v['vname'] . '</td>';
        echo '<td>' . $v['positionen'] . '</td>';
        echo '<td>' . $v['alter'] . '</td>';
        echo '<td>' . $v['marktwert'] . '</td>';
        echo '</tr>';
    }
    echo '</table>';
}
catch (PDOException $e)
{
    statusSchreiben($e->getMessage());
}
?>
</body>
</html>
