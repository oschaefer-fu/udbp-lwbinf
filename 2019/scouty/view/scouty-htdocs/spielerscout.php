<?php
/**
 * Abfrageformular bezüglich der Spieler.
 * php version 7.0.33
 *
 * @category Minidatenbank_Für_Unterrichtszwecke
 * @package  Scouty
 * @author   Frank Huth <frank_huth@gmx.de>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @version  GIT: $Id$
 * @link     ???
 */

 session_start();
?>
<!DOCTYPE html>
<html>

<head>
<title>Scouty Spielerscouting</title>
</head>

<body>
<h1>Scouty Spielerscouting</h1>
<?php
require 'common-defs.php';

echo '<section>';
require 'common-navbar.php';

echo '<article class=tabelle>';
/*echo '<h2>Spieler</h2>';*/
try {
    /*echo '<div class="tabelle">*/
    include 'conn-inc.php';      // Datenbank anmelden

    /* Wenn es eine $_POST-Variable gibt, dann ist das Formular auf Wiedervorlage */
    if (isset($_POST['minTW'])) {$minTW = $_POST['minTW'];} else {$minTW = 0;}
    if (isset($_POST['maxTW'])) {$maxTW = $_POST['maxTW'];} else {$maxTW = 1;}
    if (isset($_POST['minLV'])) {$minLV = $_POST['minLV'];} else {$minLV = 0;}
    if (isset($_POST['maxLV'])) {$maxLV = $_POST['maxLV'];} else {$maxLV = 1;}
    if (isset($_POST['minRV'])) {$minRV = $_POST['minRV'];} else {$minRV = 0;}
    if (isset($_POST['maxRV'])) {$maxRV = $_POST['maxRV'];} else {$maxRV = 1;}
    if (isset($_POST['minIV'])) {$minIV = $_POST['minIV'];} else {$minIV = 0;}
    if (isset($_POST['maxIV'])) {$maxIV = $_POST['maxIV'];} else {$maxIV = 1;}
    if (isset($_POST['minDM'])) {$minDM = $_POST['minDM'];} else {$minDM = 0;}
    if (isset($_POST['maxDM'])) {$maxDM = $_POST['maxDM'];} else {$maxDM = 1;}
    if (isset($_POST['minLA'])) {$minLA = $_POST['minLA'];} else {$minLA = 0;}
    if (isset($_POST['maxLA'])) {$maxLA = $_POST['maxLA'];} else {$maxLA = 1;}
    if (isset($_POST['minZM'])) {$minZM = $_POST['minZM'];} else {$minZM = 0;}
    if (isset($_POST['maxZM'])) {$maxZM = $_POST['maxZM'];} else {$maxZM = 1;}
    if (isset($_POST['minRA'])) {$minRA = $_POST['minRA'];} else {$minRA = 0;}
    if (isset($_POST['maxRA'])) {$maxRA = $_POST['maxRA'];} else {$maxRA = 1;}
    if (isset($_POST['minOM'])) {$minOM = $_POST['minOM'];} else {$minOM = 0;}
    if (isset($_POST['maxOM'])) {$maxOM = $_POST['maxOM'];} else {$maxOM = 1;}
    if (isset($_POST['minST'])) {$minST = $_POST['minST'];} else {$minST = 0;}
    if (isset($_POST['maxST'])) {$maxST = $_POST['maxST'];} else {$maxST = 1;}
    if (isset($_POST['minTechnik'])) {$minTechnik = $_POST['minTechnik'];} else {$minTechnik = 0;}
    if (isset($_POST['minAusdauer'])) {$minAusdauer = $_POST['minAusdauer'];} else {$minAusdauer = 0;}
    if (isset($_POST['minPhysis'])) {$minPhysis = $_POST['minPhysis'];} else {$minPhysis = 0;}
    if (isset($_POST['minMentalitaet'])) {$minMentalitaet = $_POST['minMentalitaet'];} else {$minMentalitaet = 0;}
    if (isset($_POST['minStaerke'])) {$minStaerke = $_POST['minStaerke'];} else {$minStaerke = 0;}

    $erg = $con->query('SELECT MAX(Marktwert) AS mmw FROM Spieler', PDO::FETCH_ASSOC);
    foreach ($erg as $row) {
        $maxMaxMarktwert = $row['mmw'];
    }
    
    if (isset($_POST['maxMarktwert'])) {$maxMarktwert = $_POST['maxMarktwert'];} else {$maxMarktwert = $maxMaxMarktwert;}

    $paramarray = array();
    $whereklausel = 'WHERE';
    
    $whereklausel = $whereklausel . " (AuspraegungTW BETWEEN :minTW AND :maxTW)";
    $paramarray[":minTW"] = $minTW;
    $paramarray[":maxTW"] = $maxTW;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungLV BETWEEN :minLV AND :maxLV)";
    $paramarray[":minLV"] = $minLV;
    $paramarray[":maxLV"] = $maxLV;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungRV BETWEEN :minRV AND :maxRV)";
    $paramarray[":minRV"] = $minRV;
    $paramarray[":maxRV"] = $maxRV;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungIV BETWEEN :minIV AND :maxIV)";
    $paramarray[":minIV"] = $minIV;
    $paramarray[":maxIV"] = $maxIV;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungDM BETWEEN :minDM AND :maxDM)";
    $paramarray[":minDM"] = $minDM;
    $paramarray[":maxDM"] = $maxDM;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungLA BETWEEN :minLA AND :maxLA)";
    $paramarray[":minLA"] = $minLA;
    $paramarray[":maxLA"] = $maxLA;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungZM BETWEEN :minZM AND :maxZM)";
    $paramarray[":minZM"] = $minZM;
    $paramarray[":maxZM"] = $maxZM;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungRA BETWEEN :minRA AND :maxRA)";
    $paramarray[":minRA"] = $minRA;
    $paramarray[":maxRA"] = $maxRA;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungOM BETWEEN :minOM AND :maxOM)";
    $paramarray[":minOM"] = $minOM;
    $paramarray[":maxOM"] = $maxOM;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (AuspraegungST BETWEEN :minST AND :maxST)";
    $paramarray[":minST"] = $minST;
    $paramarray[":maxST"] = $maxST;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (Technik >= :minTechnik)";
    $paramarray[":minTechnik"] = $minTechnik;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (Ausdauer >= :minAusdauer)";
    $paramarray[":minAusdauer"] = $minAusdauer;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (Physis >= :minPhysis)";
    $paramarray[":minPhysis"] = $minPhysis;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (Mentalitaet >= :minMentalitaet)";
    $paramarray[":minMentalitaet"] = $minMentalitaet;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (Staerke >= :minStaerke)";
    $paramarray[":minStaerke"] = $minStaerke;

    $whereklausel = $whereklausel . ' AND ';
    $whereklausel = $whereklausel . " (Marktwert <= :maxMarktwert)";
    $paramarray[":maxMarktwert"] = $maxMarktwert;

    /*echo $whereklausel;*/

    $Frage = $con->prepare(
        'SELECT DISTINCT sid, SVorname, SNachname, VName' .
        ', AuspraegungTW, AuspraegungLV, AuspraegungRV, AuspraegungIV, AuspraegungRA' .
        ', AuspraegungDM, AuspraegungLA, AuspraegungZM, AuspraegungOM, AuspraegungST' .
        ', Technik, Ausdauer, Physis, Mentalitaet, Staerke' .
        ', EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM SGeb) AS Alter, Marktwert' .
        ' FROM ViewSpieler NATURAL JOIN ViewSpielerPositionen ' . $whereklausel .
        ' ORDER BY Marktwert' .
        ';');

    $Frage->execute($paramarray);
    $Erg = $Frage->setFetchMode(PDO::FETCH_ASSOC);
?>

<article class=suchbedingungen>
<form method="POST">
<!--h2>Ausprägung</h2-->
<br>
<table>
<tr><th></th>
<th>Vorname</th><th>Nachname</th><th>Verein</th><th>Alter</th>
<th>TW</th><th>LV</th><th>RV</th><th>IV</th><th>DM</th><th>LA</th><th>ZM</th><th>RA</th><th>OM</th><th>ST</th>
<th>Technik</th><th>Ausdauer</th><th>Physis</th><th>Mentalitaet</th><th>Staerke</th><th>Marktwert</th>
</tr>
<tr><th>min</th>
<th></th>
<th></th>
<th></th>
<th></th>
<th><input name="minTW" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minTW; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minLV" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minLV; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minRV" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minRV; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minIV" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minIV; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minDM" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minDM; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minLA" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minLA; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minZM" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minZM; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minRA" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minRA; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minOM" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minOM; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minST" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minST; ?>" min="0" max="1" step="0.01"></th>
<th><input name="minTechnik" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minTechnik; ?>" min="0" max="10" step="1"></th>
<th><input name="minAusdauer" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minAusdauer; ?>" min="0" max="10" step="1"></th>
<th><input name="minPhysis" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minPhysis; ?>" min="0" max="10" step="1"></th>
<th><input name="minMentalitaet" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minMentalitaet; ?>" min="0" max="10" step="1"></th>
<th><input name="minStaerke" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $minStaerke; ?>" min="0" max="1" step="0.01"></th>
<th></th>
</tr>
<tr><th>max</th>
<th colspan="4">
<input type="submit" value="Wer?">
</th>
<th><input name="maxTW" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxTW; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxLV" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxLV; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxRV" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxRV; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxIV" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxIV; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxDM" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxDM; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxLA" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxLA; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxZM" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxZM; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxRA" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxRA; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxOM" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxOM; ?>" min="0" max="1" step="0.01"></th>
<th><input name="maxST" type="number" class="PositionsAuspraegungsFeld" value="<?php echo $maxST; ?>" min="0" max="1" step="0.01"></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th><input name="maxMarktwert" type="number" class="MarktwertFeld" value="<?php echo $maxMarktwert; ?>" min="0" max="<?php echo $maxMaxMarktwert?>" step="1000"></th>
</tr>
<?php
    foreach ($Frage->fetchAll() as $n => $v) {
        echo '<tr>';
        echo '<td></td>';
        echo '<td>' . $v['svorname'] . '</td>';
        echo '<td>' . $v['snachname'] . '</td>';
        echo '<td>' . $v['vname'] . '</td>';
        echo '<td>' . $v['alter'] . '</td>';
        echo '<td>' . $v['auspraegungtw'] . '</td>';
        echo '<td>' . $v['auspraegunglv'] . '</td>';
        echo '<td>' . $v['auspraegungrv'] . '</td>';
        echo '<td>' . $v['auspraegungiv'] . '</td>';
        echo '<td>' . $v['auspraegungdm'] . '</td>';
        echo '<td>' . $v['auspraegungla'] . '</td>';
        echo '<td>' . $v['auspraegungzm'] . '</td>';
        echo '<td>' . $v['auspraegungra'] . '</td>';
        echo '<td>' . $v['auspraegungom'] . '</td>';
        echo '<td>' . $v['auspraegungst'] . '</td>';
        echo '<td>' . $v['technik'] . '</td>';
        echo '<td>' . $v['ausdauer'] . '</td>';
        echo '<td>' . $v['physis'] . '</td>';
        echo '<td>' . $v['mentalitaet'] . '</td>';
        echo '<td>' . $v['staerke'] . '</td>';
        echo '<td>' . $v['marktwert'] . '</td>';
        echo '</tr>';
    }
    echo '</table>';
?>
</table>
<br>
</form>
</article>
<?php
}
catch (PDOException $e)
{
    echo '<aside class=errorMsg>';
    statusSchreiben($e->getMessage());
    echo '</aside>';
}
echo '</section>';
?>
</body>
</html>

