Hier wird auf ein etwas komplexeres Suchbeispiel eingegangen, was eine
"Scouting"-Abfrage implementieren soll. Die Einzelteile sollten an sich
zugänglich sein.

Auf Erweiterungen wie Formulare zur Eingabe/Änderung von Daten in die/der
Datenbank mit den entsprechenden html, php, sql Implementierungen wird hier auf
Grund derer Komplexität nicht eingegangen.

Der View Spielerscout
---------------------
Diese php-html-SQL-Implementierung stellt eine Tabelle mit einigen Suchkriterien
im Tabellenkopf zur Verfügung. Dieses Beispiel soll in Richtung Relevanz weisen
und ist trotz seines Umfangs aus wiederholten verständlichen Einzelteilen
zusammengesetzt. Bei Einsatz des SQL-Views ViewSpielerpositionen, sollte die
Idee für Schüler erfassbar sein.


Der SQL-View ViewSpielerpositionen
----------------------------------
Dieser View ist eine Anwendung des LEFT JOIN, wobei ein und dieselbe Tabelle
mehrfach unter verschiedenen Namen unter Verwendung der AS-Klausel gejoint
wird. Dieses Beispiel kann vom zweifachen LEFT JOIN derselen Tabelle aus aufgebaut
werden, wobei die Notwendigkeit entsteht, die wiederholt verwendtete Tabelle
mittels AS mit verschiedenen Namen zu versehen.  Es wird eine Tabelle aufgebaut,
in der jedem Spieler die Auspraegungen (im Zweifelsfalle 0.0) aller möglichen
Feldpositionen zugeordnet werden. Das ist eine Basis für einfache Abfragen mit
WHERE ... BETWEEN...

