#!/bin/sh

# postgres-sql-source-file nach ddl converter

C_SEDSCRIPT=$(mktemp --suffix=.sed)

cat >$C_SEDSCRIPT << EOF
s%^CREATE DATABASE.*$%%
s%^[\\].*$%%
s%SERIAL%INTEGER%g
#s%	%    %
s/ON DELETE//g
s/ON UPDATE//g
s/RESTRICT//g
s/CASCADE//g
#s/\(COALESCE(\)\(.*\)\(,.*)\)/\2/
#s%NOT NULL CHECK%CHECK%
#/REFERENCES/s%NOT NULL%%
#s%/\*.*\*/%%g
EOF

CF=Create-scouty
echo '/multiline on' >$CF.ddl
#echo '/verbose on' >>$CF.ddl
echo '/sql' >>$CF.ddl
sed -f $C_SEDSCRIPT ../sql/$CF.sql | grep -ve '^$' | head -n 1000 | tail -n 1000 >> $CF.ddl
echo '/list_relations' >>$CF.ddl

I_SEDSCRIPT=$(mktemp --suffix=.sed)

cat >>$I_SEDSCRIPT << EOF
s%^[\\].*$%%
s%\('\)\([0-9]\{4\}\)\(-\)\([0-9]\{1,2\}\)\(-\)\([0-9]\{1,2\}\)\('\)%date &%g
EOF

IF=Insert-scouty-data
echo '/multiline on' >$IF.ddl
#echo '/verbose on' >>$IF.ddl
echo '/sql' >>$IF.ddl
sed -f $I_SEDSCRIPT ../sql/$IF.sql | grep -ve '^$' | head -n 1000 | tail -n 1000 >> $IF.ddl
echo '/list_relations' >>$IF.ddl


rm $C_SEDSCRIPT
rm $I_SEDSCRIPT

