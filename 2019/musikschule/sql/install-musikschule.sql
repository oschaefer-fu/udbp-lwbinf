CREATE Table schueler (
  SID           integer PRIMARY KEY,
  S_Vorname       varchar(20),
  S_Nachname      varchar(20),
  S_Strasse       varchar(30),
  S_Hausnummer    integer,
  S_PLZ           integer,
  S_Ort           varchar(20),
  S_Telefonnummer varchar(20)
);

CREATE Table lehrer (
  LID           integer PRIMARY KEY,
  L_Vorname       varchar(20),
  L_Nachname      varchar(20),
  L_Strasse       varchar(30),
  L_Hausnummer    integer,
  L_PLZ           integer,
  L_Ort           varchar(20),
  L_Telefonnummer varchar(20)
);

CREATE Table instrumente (
  IID       	integer PRIMARY KEY,
  I_Art      	varchar(20),
  I_Hersteller	varchar(20),
  I_Preis 		integer,
  I_Kaufdatum		date
);


CREATE Table raeume (
  RID       	integer PRIMARY KEY,
  R_Klavier      	boolean,
  R_Plaetze	integer
);


CREATE Table kurse (
  KID       	integer PRIMARY KEY,
  K_Name      	varchar(20),
  LID		integer references lehrer(LID),
  K_Wochentag	varchar(10),
  K_Uhrzeit	time,
  K_Dauer		integer,
  RID		integer references raeume(RID)
);

CREATE Table veranstaltung (
 VID  integer PRIMARY KEY,	
 V_Name  varchar(20),
 RID integer REFERENCES raeume(RID),
 V_Datum date, 
 V_Uhrzeit time, 
 V_Dauer integer 
);

CREATE Table pruefung (
  KID           integer REFERENCES kurse (KID),
  SID		integer REFERENCES schueler(SID),
  PRIMARY KEY (KID, SID),
  LID           integer REFERENCES lehrer(LID),
  P_Note          integer,
  P_Datum         date
);

CREATE Table 	organisieren (
  VID       	integer REFERENCES veranstaltung(VID),
  LID       	integer REFERENCES lehrer(LID),
  PRIMARY KEY (VID, LID)
);


CREATE Table leihen (
  IID       	integer REFERENCES instrumente(IID),
  SID      	integer REFERENCES schueler(SID),
  PRIMARY KEY (IID, SID),
  LE_Ausleihdatum	date,
  LE_Rueckgabedatum date,
  LE_Pfand		integer
);

CREATE Table auftritt (
  VID integer,
  SID integer,
  PRIMARY KEY (VID, SID)
);

CREATE Table besucht (
 KID integer REFERENCES kurse(KID),
 SID integer REFERENCES schueler(SID),
 PRIMARY KEY (KID, SID)
);

INSERT INTO schueler VALUES (1, 'Werner', 'Brösel', 'Bölkstoffgasse',7 ,24940, 'Flensburg', '55534347');
INSERT INTO schueler VALUES (2, 'Hörni', 'Meier','Rotestrasse', 17, 24943, 'Flensburg', '55574748');
INSERT INTO schueler VALUES (3, 'Kalli', 'Meier','Rotestrasse', 17, 24943, 'Flensburg', '55574748');
INSERT INTO schueler VALUES (4, 'Wolfgang', 'Ußleber', 'Am Knüll', 14, 24955,'Harislee', '55599974');
INSERT INTO schueler VALUES (5, 'Holger', 'Henze', 'Am Sattelplatz', 8, 24941, 'Flensburg', '55577789');
INSERT INTO schueler VALUES (6, 'Bruno', 'Schnarup-Thumby', 'Kleine Lücke', 1,24939, 'Flensburg', '55598787');
INSERT INTO schueler VALUES (7, 'Helmut', 'Schnarup-Thumby', 'Kleine Lücke', 1, 24939, 'Flensburg', '55598787');
INSERT INTO schueler VALUES (8, 'Andi', 'Brösel', 'Bölkstoffgasse', 7, 24940, 'Flensburg', '55534347');
INSERT INTO schueler VALUES (9, 'Tina', 'Hansen', 'Pinienhof', 23, 24937, 'Flensburg', '55523269');
INSERT INTO schueler VALUES (10, 'Heidrun', 'Gloer', 'Tannenweg', 13, 24944, 'Flensburg', '55586865');

INSERT INTO lehrer VALUES (1, 'Walter', 'Röhrig', 'Kloschüsselgasse', 17, 24938, 'Flensburg', '55574483');
INSERT INTO lehrer VALUES (2, 'Eckhard', 'Unsinn', 'Flachzangenweg', 5, 24944, 'Flensburg', '55587873');
INSERT INTO lehrer VALUES (3, 'Erwin', 'Hüpenbecker', 'Glanzalle', 78, 24111 ,'Kiel', '55578888');
INSERT INTO lehrer VALUES (4, 'Ingrid', 'Günzelsen', 'Zum Kliff', 388, 24117, 'Kiel', '55567679');

INSERT INTO instrumente VALUES  (1, 'Geige', 'Yamaha', 2798, '12.5.1972');
INSERT INTO instrumente VALUES 	(2, 'Mundharmonika','Hohner', 80, '11.11.2009');
INSERT INTO instrumente VALUES 	(3, 'E-Gitarre', 'Hohner', 1455, '17.9.2013');
INSERT INTO instrumente VALUES 	(4, 'E-Bass', 'Ibanez', 566, '18.3.2015');
INSERT INTO instrumente VALUES 	(5, 'Cello', 'Hamburger-Geigen', 799, '19.4.2001');
INSERT INTO instrumente VALUES 	(6, 'Cello', 'Yamaha', 1899, '27.1.2005');
INSERT INTO instrumente VALUES 	(7, 'Klassik-Gitarre', 'Epiphone', 1199, '1.4.2019');
INSERT INTO instrumente VALUES 	(8, 'Westerngitarre', 'Gibson', 3488, '29.2.2000');
INSERT INTO instrumente VALUES 	(9, 'Geige', 'Yamaha', 1899, '27.1.2005');
INSERT INTO instrumente VALUES 	(10, 'Mundharmonika', 'Yamaha', 56, '22.11.2014');

INSERT INTO raeume VALUES (1, true, 3);
INSERT INTO raeume VALUES (2, false, 4);
INSERT INTO raeume VALUES (3, false, 6);
INSERT INTO raeume VALUES (4, true, 50);

INSERT INTO kurse VALUES (1, 'Bierflaschenflöte', 1, 'Montag', '18:00', 60, 1);
INSERT INTO kurse VALUES (2, 'Klavier', 4, 'Montag', '19:00', 30, 1);
INSERT INTO kurse VALUES (3, 'Streicherquartett', 3, 'Dienstag', '18:00', 90, 4);
INSERT INTO kurse VALUES (4, 'Klavier', 4, 'Montag', '18:00', 60, 4);
INSERT INTO kurse VALUES (5, 'Geige', 3, 'Mittwoch', '18:00', 60, 2);
INSERT INTO kurse VALUES (6, 'Bratsche', 3, 'Mittwoch', '19:00', 60, 2);
INSERT INTO kurse VALUES (7, 'Cello', 3, 'Mittwoch', '20:00', 60, 3);
INSERT INTO kurse VALUES (8, 'Gitarre', 2, 'Freitag', '20:00', 60, 2);
INSERT INTO kurse VALUES (9, 'Mundharmonika', 2, 'Dienstag', '19:30', 90, 2);
INSERT INTO kurse VALUES (10, 'Harmonielehre', 1, 'Freitag', '20:00', 60, 1);

INSERT INTO veranstaltung VALUES (1, 'Weihnachtskonzert', 4, '22.12.18', '18:30', 90);
INSERT INTO veranstaltung VALUES (2, 'Klavier-Konzert', 4, '23.3.19', '19:00', 60);
INSERT INTO veranstaltung VALUES (3, 'Tag-der-offenen-Tür', NULL, '17.4.19', '16:00', 180);
INSERT INTO veranstaltung VALUES (4, 'Kammermusik-Abend', 4, '22.2.19', '18:30', 60);
INSERT INTO veranstaltung VALUES (5, 'Volksliederabend', NULL, '18.9.2019', '19:30', 120);

INSERT INTO leihen VALUES (1, 3, '07.03.2018', '06.03.2020', 100);
INSERT INTO leihen VALUES (9, 2, '07.03.2018', '06.03.2020', 100);
INSERT INTO leihen VALUES (3, 9, '01.12.2017', '30.11.2018', 50);
INSERT INTO leihen VALUES (5, 10, '05.06.2017', '23.08.2020', 150);
INSERT INTO leihen VALUES (10, 1, '09.09.2016', '15.04.2021', 10);

INSERT INTO auftritt VALUES (1,1);
INSERT INTO auftritt VALUES (1,9);
INSERT INTO auftritt VALUES (1,3);
INSERT INTO auftritt VALUES (1,2);
INSERT INTO auftritt VALUES (1,4);
INSERT INTO auftritt VALUES (2,6);
INSERT INTO auftritt VALUES (2,7);
INSERT INTO auftritt VALUES (4,8);
INSERT INTO auftritt VALUES (4,2);
INSERT INTO auftritt VALUES (4,3);
INSERT INTO auftritt VALUES (5,2);
INSERT INTO auftritt VALUES (5,3);
INSERT INTO auftritt VALUES (5,4);
INSERT INTO auftritt VALUES (5,8);
INSERT INTO auftritt VALUES (6,1);
INSERT INTO auftritt VALUES (6,9);
INSERT INTO auftritt VALUES (6,5);
INSERT INTO auftritt VALUES (6,6);
INSERT INTO auftritt VALUES (6,10);

INSERT INTO pruefung VALUES (1, 1, 1, 2, '18.3.2019');
INSERT INTO pruefung VALUES (2, 7, 4, 1, '20.2.2019');
INSERT INTO pruefung VALUES (5, 3, 3, 3, '8.4.2019');
INSERT INTO pruefung VALUES (10, 2, 1, 3, '5.5.2019');
INSERT INTO pruefung VALUES (10, 4, 1, 3, '5.5.2019');
INSERT INTO pruefung VALUES (10, 6, 1, 3, '5.5.2019');
INSERT INTO pruefung VALUES (4, 8, 4, 2, '14.3.2019');
INSERT INTO pruefung VALUES (1, 9, 1, 4, '18.3.2019');

INSERT INTO organisieren VALUES (1,1);
INSERT INTO organisieren VALUES (2,4);
INSERT INTO organisieren VALUES (3,1);
INSERT INTO organisieren VALUES (4,3);
INSERT INTO organisieren VALUES (5,3);


INSERT INTO besucht VALUES (1,1);
INSERT INTO besucht VALUES (3,2);
INSERT INTO besucht VALUES (5,2);
INSERT INTO besucht VALUES (3,3);
INSERT INTO besucht VALUES (5,3);
INSERT INTO besucht VALUES (3,4);
INSERT INTO besucht VALUES (7,4);
INSERT INTO besucht VALUES (3,5);
INSERT INTO besucht VALUES (4,6);
INSERT INTO besucht VALUES (2,7);
INSERT INTO besucht VALUES (4,8);
INSERT INTO besucht VALUES (9,1);
INSERT INTO besucht VALUES (6,10);
INSERT INTO besucht VALUES (10,1);
INSERT INTO besucht VALUES (10,2);
INSERT INTO besucht VALUES (10,3);
INSERT INTO besucht VALUES (10,4);
INSERT INTO besucht VALUES (10,5);
INSERT INTO besucht VALUES (10,6);
