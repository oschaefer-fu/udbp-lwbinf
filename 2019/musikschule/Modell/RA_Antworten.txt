Einfache RA-Antworten:


Selektion:

1) Welche Angaben sind �ber den Sch�ler Werner gespeichert?
 

answer(schueler.SID:int,schueler.S_Vorname:string,schueler.S_Nachname:varchar(20),schueler.S_Strasse:varchar(30),schueler.S_Hausnummer:int,schueler.S_PLZ:int,schueler.S_Ort:varchar(20),schueler.S_Telefonnummer:varchar(20)) ->
{
  answer(1,'Werner','Br�sel','B�lkstoffgasse',7,24940,'Flensburg','55534347')
}
Info: 1 tuple computed. 

Sch�ler Werner hat die ID 1, hei�t Werner Br�sel, wohnt in der B�lkstoffgasse 7 24940 Flensburg und seine Telefonnummer lautet: '55534347'.

Projektion:

2) Wie sind die Vornamen der Sch�ler?


answer(schueler.S_Vorname:varchar(20)) ->
{
  answer('Andi'),
  answer('Bruno'),
  answer('Heidrun'),
  answer('Helmut'),
  answer('Holger'),
  answer('H�rni'),
  answer('Kalli'),
  answer('Tina'),
  answer('Werner'),
  answer('Wolfgang')
}
Info: 10 tuples computed.

Die Vornamen der Sch�ler sind Andi, Bruno, Heidrun, Helmut, Holger, H�rni, Kalli, Tina, Werner und Wolfgang.

3) Welche Instrumente kann man von der Musikschule leihen?

answer(instrumente.I_Art:varchar(20)) ->
{
  answer('Cello'),
  answer('E-Bass'),
  answer('E-Gitarre'),
  answer('Geige'),
  answer('Klassik-Gitarre'),
  answer('Mundharmonika'),
  answer('Westerngitarre')
}
Info: 7 tuples computed.       

Man kann die Instrumente Cello, E-Bass, E-Gitarre, Geige, Klassik-Gitarre, Mundharmonika und Westerngitarre von der Musikschule ausleihen.

4) Welche Kurse kann man besuchen?

answer(kurse.K_Name:varchar(20)) ->
{
  answer('Bierflasschenfl�te'),
  answer('Bratsche'),
  answer('Cello'),
  answer('Geige'),
  answer('Gitarre'),
  answer('Harmonielehre'),
  answer('Klavier'),
  answer('Mundharmonika'),
  answer('Streicherquartett')
}
Info: 9 tuples computed.      

Man kann die Kurse Bierflasschenfl�te, Bratsche, Cello, Geige, Gitarre, Harmonielehre, Klavier, Mundharmonika und Streicherquartett besuchen.

Projektion und Selektion:

5) Wie hei�en die Sch�ler mit Vornamen, die mit Nachnamen Meier hei�en?


answer(schueler.S_Vorname:varchar(20)) ->
{
  answer('H�rni'),
  answer('Kalli')
}
Info: 2 tuples computed.   

Die Sch�ler H�rni und Kalli hei�en mit Nachnamen Meier.

6) Wie sind die Vor- und Nachnamen der Sch�ler, die in Flensburg wohnen?

answer(schueler.S_Vorname:varchar(20),schueler.S_Nachname:varchar(20)) ->
{
  answer('Andi','Br�sel'),
  answer('Bruno','Schnarup-Thumby'),
  answer('Heidrun','Gloer'),
  answer('Helmut','Schnarup-Thumby'),
  answer('Holger','Henze'),
  answer('H�rni','Meier'),
  answer('Kalli','Meier'),
  answer('Tina','Hansen'),
  answer('Werner','Br�sel')
}
Info: 9 tuples computed.  

Die Sch�ler Andi Br�sel, Bruno Schnarup-Thumby, Heidrun Gloer, Helmut Schnarup-Thumby, Holger Henze, H�rni Meier, Kalli Meier, Tina Hansen und Werner Br�sel wohnen in Flensburg. 

7) Welche Kurse beginnen ab 19 Uhr?

answer(kurse.K_Name:varchar(20)) ->
{
  answer('Bratsche'),
  answer('Cello'),
  answer('Gitarre'),
  answer('Harmonielehre'),
  answer('Klavier'),
  answer('Mundharmonika')
}
Info: 6 tuples computed.

Kurse, die nach 19 Uhr beginnen sind Bratsche, Cello, Gitarre, Harmonielehre, Klavier und Mundharmonika. 

8) Welche Kurse dauern 90 Minuten?


answer(kurse.K_Name:varchar(20)) ->
{
  answer('Mundharmonika'),
  answer('Streicherquartett')
}
Info: 2 tuples computed.

Die Kurse Mundharmonika und Streichquartett dauern 90 Minuten.

9) Wie sind die Nachnamen und Telefonnummern der Lehrer aus Flensburg?


answer(lehrer.L_Nachname:varchar(20),lehrer.L_Telefonnummer:varchar(20)) ->
{
  answer('R�hrig','55574483'),
  answer('Unsinn','55587873')
}
Info: 2 tuples computed.

Die Lehrer aus Flensburg nach Nachname und Telefonnummer lauten: R�hrig (55574483) und Unsinn (55587873).

10)  Wie hei�en die Hersteller der Cellos?

answer(instrumente.I_Hersteller:varchar(20)) ->
{
  answer('Hamburger-Geigen'),
  answer('Yamaha')
}
Info: 2 tuples computed.    

Die Hersteller der Cellos hei�en Hamburger-Geigen und Yamaha.




