﻿Angabe der semantischen Bezüge, die sich im Modell nicht wiederfinden, sowie Schlüssel- und Fremdschlüsselbeziehungen:

schueler (SID, S_Vorname, S_Nachname, S_Strasse, S_Hausnummer, S_PLZ, S_Ort, S_Telefonnummer)
Die Attribute dürfen alle nicht NULL sein, damit die Schüler erreichbar sind.

lehrer (LID, L_Vorname, L_Nachname, L_Strasse, L_Hausnummer, L_PLZ, L_Ort, L_Telefonnummer)
dito
	
kurse (KID, K_Name, LID, K_Wochentag, K_Uhrzeit, K_Dauer, RID)
Auch hier darf kein Attribut NULL sein, weil es sonst z.B. nicht möglich ist, einen Stundenplan zu erstellen.
Ohne Lehrer gibt es trotzdem noch den Kurs. Es muss nur ein neuer Lehrer eingesetzt werden:
LID integer 	references lehrer.LID
		on delete cascade

raeume (RID, R_Klavier, R_Plaetze)
Alles ist NOT NULL

instrumente (IID, I_Art, I_Hersteller, I_Preis, I_Kaufdatum)
Aus buchhalterischen Gründen darf auch hier kein Attribut NULL sein.

Veranstaltungen(VID, V_Name, RID, V_Datum, V_Uhrzeit, V_Dauer)
Da auch Aufführungen ohne Raum denkbar sind, z.B. das Bierflaschenflöten-Open-Air, darf die RID NULL sein.
	
pruefung(KID, SID, LID, P_Note, P_Datum)
Hier besteht der Primärschlüssel aus der Kombination zweier Fremdschlüssel.
Außer der LID dürfen die Attribute nicht NULL sein.

Wird ein Schüler gelöscht, dann gibt es auch keine Prüfung mehr mit ihm:
SID integer		references schueler.SID
               		on delete cascade
		
Wird ein Lehrer gelöscht, dann bleibt die Prüfung bestehen. Ein anderer Lehrer muss dann eingesetzt werden. Dafür wird aber zunächst das Attribut LID auf NULL gesetzt.
LID integer references lehrer.LID
            on delete set null

Zu jeder Prüfung gibt es auch einen Kurs. Deshalb darf ein Kurs nicht gelöscht werden, solange es noch eine Prüfung dazu gibt.
KID integer	references kurse.KID
               	on delete no action,
	
leihen (IID, SID, Ausleih-Datum,Rückgabe-Datum, Pfand)
Kein Attibut darf NULL sein.
Wird ein Schüler gelöscht, dann muss er trotzdem noch seine Instrumente zurückgeben:

SID integer	references schueler.SID
               	on delete no action

Wird ein Instrument gelöscht, dann werden auch die restlichen Einträge mit ihm gelöscht:
IID integer	references instrumente.IID
               	on delete cascade

besucht (KID, SID)
Wird ein Schüler oder ein Kurs gelöscht, dann gibt es auch das Tupel nicht mehr:
SID integer	references schueler.SID
               	on delete cascade

KID integer	references kurse.KID
               	on delete cascade


auftritt(VID,SID)
dito
SID integer	references schueler.SID
               	on delete cascade

VID integer	references veranstaltungen.VID
               	on delete cascade

