Einfache Anfragen:

A. Selektion
1. Welche Angaben sind in der Relation schueler über Werner gespeichert?

	select * from schueler where S_Vorname='Werner';

	 sid | s_vorname | s_nachname |   s_strasse    | s_hausnummer | s_plz |  s_sort   | s_telefonnummer 
	-----+-----------+------------+----------------+--------------+-------+-----------+-----------------
	   1 | Werner    | Brösel     | Bölkstoffgasse |            7 | 24940 | Flensburg | 55534347

2. Wie heißen die Schüler mit Vornamen, die mit Nachnamen Meier heißen?
lewein=>  select s_vorname from schueler where S_Nachname = 'Meier';
 s_vorname 
-----------
 Hörni
 Kalli
(2 Zeilen)



3. Wie sind die Vor- und Nachnamen der Schüler, die in Flensburg wohnen?

	select * from schueler where S_Ort = 'Flensburg';

 sid | s_vorname |   s_nachname    |   s_strasse    | s_hausnummer | s_plz |   s_ort   | s_telefonnummer 
-----+-----------+-----------------+----------------+--------------+-------+-----------+-----------------
   1 | Werner    | Brösel          | Bölkstoffgasse |            7 | 24940 | Flensburg | 55534347
   2 | Hörni     | Meier           | Rotestrasse    |           17 | 24943 | Flensburg | 55574748
   3 | Kalli     | Meier           | Rotestrasse    |           17 | 24943 | Flensburg | 55574748
   5 | Holger    | Henze           | Am Sattelplatz |            8 | 24941 | Flensburg | 55577789
   6 | Bruno     | Schnarup-Thumby | Kleine Lücke   |            1 | 24939 | Flensburg | 55598787
   7 | Helmut    | Schnarup-Thumby | Kleine Lücke   |            1 | 24939 | Flensburg | 55598787
   8 | Andi      | Brösel          | Bölkstoffgasse |            7 | 24940 | Flensburg | 55534347
   9 | Tina      | Hansen          | Pinienhof      |           23 | 24937 | Flensburg | 55523269
  10 | Heidrun   | Gloer           | Tannenweg      |           13 | 24944 | Flensburg | 55586865
(9 Zeilen)


4. Welche Kurse beginnen ab 19:00 Uhr?
lewein=>  select k_name from kurse where k_uhrzeit >='19:00';
    k_name     
---------------
 Klavier
 Bratsche
 Cello
 Gitarre
 Mundharmonika
 Harmonielehre
(6 Zeilen)

5. Welche Kurse dauern 90 Minuten?
select k_name from kurse where k_dauer ='90';
      k_name       
-------------------
 Streicherquartett
 Mundharmonika
(2 Zeilen)

B. Projektion
6. Wie sind die Vornamnen der Schüler?
lewein=> select s_vorname from schueler;
 s_vorname 
-----------
 Werner
 Hörni
 Kalli
 Wolfgang
 Holger
 Bruno
 Helmut
 Andi
 Tina
 Heidrun
(10 Zeilen)




7. Welche Leihinstrumente besitzt die Musikschule?

lewein=> select i_art from instrumente;
      i_art      
-----------------
 Geige
 Mundharmonika
 E-Gitarre
 E-Bass
 Cello
 Cello
 Klassik-Gitarre
 Westerngitarre
 Geige
 Mundharmonika
(10 Zeilen)



8. Welche Kurse gibt es?
lewein=> select k_name from kurse;
       k_name       
--------------------
 Bierflasschenflöte
 Klavier
 Streicherquartett
 Klavier
 Geige
 Bratsche
 Cello
 Gitarre
 Mundharmonika
 Harmonielehre
(10 Zeilen)


C. Selektion und Projektion
9. Wie sind die Nachnamen und Telefonnummern der Lehrer aus Flensburg?
select l_nachname, l_telefonnummer from lehrer where l_ort='Flensburg';
 l_nachname | l_telefonnummer 
------------+-----------------
 Röhrig     | 55574483
 Unsinn     | 55587873
(2 Zeilen)



10. Wie heißen die Hersteller der Cellos?
select i_hersteller from instrumente where i_art = 'Cello';
   i_hersteller   
------------------
 Hamburger-Geigen
 Yamaha
(2 Zeilen)

11. An welchem Tag findet der Kurs Bierflaschenflöte statt?
select wochentag from kurse where K-name = 'Bierflaschenflöte';


12. Welches ist das teuerste Instrument?
select max(i_Preis) as teuerstes from instrumente; 
 teuerstes 
-----------
      3488
  
	
13. Eine Liste der Instrumente der Musikschule, sortiert nach Kaufdatum.
select Art, MAX(Kaufdatum) from instrumente group by Kauf-Datum;
lewein=> select I.i_art, MAX(I.i_kaufdatum) from instrumente I group by i.i_art, I.i_kaufdatum;
      i_art      |    max     
-----------------+------------
 Klassik-Gitarre | 2019-04-01
 Geige           | 2005-01-27
 Geige           | 1972-05-12
 Cello           | 2001-04-19
 Cello           | 2005-01-27
 Mundharmonika   | 2009-11-11
 E-Gitarre       | 2013-09-17
 Mundharmonika   | 2014-11-22
 E-Bass          | 2015-03-18
 Westerngitarre  | 2000-02-29
(10 Zeilen)
	
	
	
14. Wann findet der Volksliederabend statt?
lewein=> select v_datum from veranstaltung where v_name = 'Volksliederabend';
  v_datum   
------------
 2019-09-18
(1 Zeile)

	
15. Welche Räume haben ein Klavier?
select RID from raeume where r_klavier = true;
 rid 
-----
   1
   4
(2 Zeilen)


16. In welchem Raum findet der Klavierkurs statt?
lewein=> select RID from kurse where K_name = 'Klavier';
 rid 
-----
   1
   4
(2 Zeilen)

D. Verbund / Join


17. Welche Schüler (Nachname, Vorname) besuchen den Gitarrenkurs?
lewein=> select s_nachname, s_vorname from schueler natural join besucht natural join kurse where K_Name = 'Gitarre';
 s_nachname | s_vorname 
------------+-----------
(0 Zeilen)

	
18. Welche Schüler(Nachname, Vorname) haben eine Mundharmonika ausgeliehen?
lewein=> select s_nachname, s_vorname from schueler natural join leihen natural join instrumente where i_art = 'Mundharmonika'; s_nachname | s_vorname 
------------+-----------
 Brösel     | Werner
(1 Zeile)

	
19. Wer (Nachname) unterrichtet Harmonielehre?
lewein=> select L_Nachname from lehrer natural join kurse K where K_Name = 'Harmonielehre';
 l_nachname 
------------
 Röhrig
(1 Zeile)

	
20. Was unterrichtet er noch?
lewein=> select k_name from lehrer natural join kurse where l_nachname = (select L_Nachname from lehrer natural join kurse where K_Name = 'Harmonielehre') except select k_name from kurse where k_name = 'Harmonielehre';
       k_name       
--------------------
 Bierflasschenflöte
(1 Zeile)

	
	
21. Welche Lehrer sind montags in der Musikschule?
lewein=> select L_Nachname, L_Vorname from lehrer natural join kurse where K_Wochentag = 'Dienstag';
 l_nachname  | l_vorname 
-------------+-----------
 Unsinn      | Eckhard
 Hüpenbecker | Erwin
(2 Zeilen)
	

	Welche Schüler treten am Weihnachtskonzert auf?
select S.Nachname, S.Vorname from schueler S, Auftritt, auffuehreungen A where A.Name = 'Weihnachtskonzert';
	
	
22. Wer organisiert den Kammermusik-Abend?
lewein=> select L_Nachname, L_Vorname from lehrer natural join organisieren natural join veranstaltung where V_Name = 'Kammermusik-Abend';
 l_nachname  | l_vorname 
-------------+-----------
 Hüpenbecker | Erwin
(1 Zeile)

		
23. Welche Schüler nehmen an der Klavierprüfung teil?
lewein=> select S_Nachname, S_Vorname from schueler natural join pruefung natural join kurse where K_Name = 'Klavier';
   s_nachname    | s_vorname 
-----------------+-----------
 Schnarup-Thumby | Helmut
 Brösel          | Andi
(2 Zeilen)

	

E. Komplizierte Abfragen
24. Nachname, Vorname aller Musikschüler ohne Prüfung
lewein=> select S_Nachname, S_Vorname from schueler  where SID not in (select SID from pruefung p);
 s_nachname | s_vorname 
------------+-----------
 Henze      | Holger
 Gloer      | Heidrun
(2 Zeilen)

	
25. Wie viele Schüler haben Klavierunterricht?
lewein=> select count(B.SID) as Anzahl from schueler natural join besucht B natural join Kurse  where K_Name = 'Klavier';
 anzahl 
--------
      3
(1 Zeile)

	
26. Welches ist das teuerste verliehene Instrument?
select max(I.preis) from schueler, leihen, instrumente I;
	
27. Welches Instrument besitzt das geringste Pfand im Vergleich zum Kaufpreis?
select min(L.Pfand/I.Preis) from instrumente I, leihen L;
	
28. Wer unterrichtet Kurse ab 19:30 Uhr?
selct L.Nachname, L.Vorname from lehrer L, kurse K where Uhrzeit > 19:30;
	
29. Welcher Lehrer unterrichtet Kurse mit weniger als 4 Schülern?
select L.Nachname, L.Vorname from lehrer L, belegen, kurse...
	
30. Gebe die Kurse an, ansteigend ihrer Schülerzahlen mit Kursnamen, Lehrer, Schlülerzahl.
	
	
31. Welche Schüler müssen noch in Musiktheorie geprüft werden, d.h. wer hat noch keine Prüfung abgelegt? 
