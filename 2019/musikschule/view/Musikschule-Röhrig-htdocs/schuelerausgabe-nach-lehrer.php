<?php
/*
 * Autor: Rörich
 * Datum: 21.05.2019
 * Zweck: Konstruktion von Abfragen und Views für die Musikschule Röhrig 
 */

?>
<!--Dekleration der Html-Bedingungen (V)-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="sw">

<!--Anbindung der CSS-Datei zur Formatierung (V)-->
<link rel="stylesheet" href="lwbstyle.css">

<head>
	<title>Schuelerausgabe-nach-Lehrerwahl</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>

<!--Beginn des eigentlichen Dokuments (I)-->
<body>

<!--Form bezeichnet die Seite als Formular und lget die Get- oder 
	Post-Methode fest, mit der die entsprechenden Formular eingaben 
	ausgewertet werden.(I) -->
	
<form action="lehrerauswahl.php" method="GET">	
<!--Überschrift der Seite (I) --> 
	<h1 align="center">Musikschule Röhrig</h1>
	
<?php
$Lehrer = $_GET['Lehrer'];
$Wochentag = $_GET['Wochentag'];
echo "<h2>Wenn $Lehrer am $Wochentag fehlt, müssen folgende Schüler benachrichtigt werden:</h2>";

include_once ('conn-inc.php');

//Die hierbeschriebenen Zugangsdaten sind in der Datei conn-inc.php hinterlegt und werden an dieser Stelle eingefügt.
//$con = new PDO ('pgsql:host=localhost;dbname=lewein' , 'lewein' , 'niewel');

$db_erg = $con->query("SELECT k_name,k_uhrzeit,s_vorname,s_nachname,s_strasse,s_hausnummer,s_ort,s_plz,s_telefonnummer
                       FROM   kurse natural join besucht natural join schueler natural join lehrer
                       where l_nachname='$Lehrer' and k_wochentag='$Wochentag';");
$rows = $con->query("SELECT COUNT (k_name)
                       FROM   kurse natural join besucht natural join schueler natural join lehrer
                       where l_nachname='$Lehrer' and k_wochentag='$Wochentag';");
foreach ($rows as $anzahl) {
	$test = $anzahl['count'];
}
echo "<h3>Anzahl der Ergebnisse: $test</h3>";
if ($test > 0) {
	echo "<table><tr><th>Kurs</th><th>Uhrzeit</th><th>Vorname</th><th>Nachname</th><th>Straße</th><th>Hausnr.</th><th>Ort</th><th>PLZ</th><th>Telnr.</th></tr>";
      foreach ($db_erg as $row) {
        echo "<tr>" .
               "<td>" . $row['k_name']   . "</td>" .
               "<td>" . $row['k_uhrzeit']   . "</td>" .
               "<td>" . $row['s_vorname']   . "</td>" .
               "<td>" . $row['s_nachname']  . "</td>" .
               "<td>" . $row['s_strasse']   . "</td>" .
               "<td>" . $row['s_hausnummer']   . "</td>" .
               "<td>" . $row['s_ort']   . "</td>" .
               "<td>" . $row['s_plz']   . "</td>" .
               "<td>" . $row['s_telefonnummer']   . "</td>" .
             "</tr>";
      }
	echo '</table>';
} else {
	echo "<h2>$Lehrer gibt am $Wochentag keinen Kurs. Es sind keine Schüler zu benachrichtigen.</h2>";
}
$con = null;

?>
<br><br><button>Ein weitere/r Lehrer/in fehlt!</button> &ensp; <button formaction="index.php">Zurück zur Startseite!</button> 
</form>

</body>

</html>
