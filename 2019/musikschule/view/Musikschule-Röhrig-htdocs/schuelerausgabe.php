<?php
/*
 * schuelerausgabe.php
 * 
 * Autor: Röhrich
 * Datum: 21.05.2019
 * Zweck: Konstruktion von Abfragen und Views für das SuSCafe
 */

?>
<!--Dekleration der Html-Bedingungen (V)-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="sw">



<!--Kopf und Eigenschaften der Html-Datei (Metadaten) (V)-->
<head>
	<title>Musikschule Röhrig - Schuelerausgabe</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<!--Anbindung der CSS-Datei zur Formatierung (V)-->
	<link rel="stylesheet" href="lwbstyle.css">
</head>

<!--Beginn des eigentlichen Dokuments-->
<body>
	
<form action="kursauswahl.php" method="GET">	
<!--Überschrift der Seite (I) --> 
	<h1 align="center">Musikschule Röhrig</h1>
<?php
/* Ab hier wird eine Verknüpfung zur SQL-Datenbank hergestellt. Die Verknüpfung zur Datenbasis
 * erfolgt per "connect-Befehl". Dieser kann je nach verwendeter Datenbasis unterschiedlich sein.
 * Bei PSQL lautet der Befehl pg_connect ("host=sheep port=5432 dbname=mary user=lamb password=foo");
 * Mit Hilfe von require_once wird eine PHP-Datei (hier: 'konfiguration.php') eingeladen,
 * die die Benutzerangaben und Verbindungen enthält. Die Verbindung wird erst tatsächlich hergestellt,
 * sobald eine Anfrage an die Datenbank gestellt wird. 
 *
 * 
 * !!!!!!!!ACHTUNG: Die konfiguration.php enthält wichtige Daten, die auch Passwörter und persönliche
 * Daten enthalten - diese Datei ist unbedingt weiterzugeben!*/
 
/*!!!!!! Die Verbindung zur Datenbank sollte den SuS bereits 
 * als Vorlage zur Verfügung gestellt werden!!!!!! (V)*/
$Kurs = $_GET['Kurs'];
echo "<h2>Schülerausgabe vom Kurs $Kurs:</h2>";

include_once ('conn-inc.php');

//Die hierbeschriebenen Zugangsdaten sind in der Datei conn-inc.php hinterlegt und werden an dieser Stelle eingefügt.
//$con = new PDO ('pgsql:host=localhost;dbname=lewein' , 'lewein' , 'niewel');
                    
//Die eigentliche SQL-Abfrage wird hier in einer Varaibele eingetragen (II)                    

/* Das Ergebnis wird in der Variablen db_erg gespeichert. Der Nutzer erhält
 * hier ALLE Einträge der Tabelle. (II)*/
$db_erg = $con->query("SELECT s_vorname,s_nachname,s_strasse,s_ort,s_plz,s_hausnummer,s_telefonnummer,l_nachname,k_wochentag,k_uhrzeit
                       FROM   kurse natural join besucht natural join schueler natural join lehrer
                       WHERE k_name='$Kurs'");

/* Hier beginnt die Tabelle, diese Zeigt auf der ersten Seite alle 
 * vorhandenen Produkte.
 * Es beginnt mit <th>. Alles zwischen diesen Bezeichnern stellt die 
 * Überschrift der Tabelle dar. (V, II)*/
echo "<table><tr><th>Vorname</th><th>Nachname</th><th>Straße</th><th>Hausnr.</th><th>Ort</th><th>PLZ</th><th>Telnr.</th><th>Lehrer</th><th>Wochentag</th><th>Uhrzeit</th></tr>";
      foreach ($db_erg as $row) {
        echo "<tr>" .
               "<td>" . $row['s_vorname']   . "</td>" .
               "<td>" . $row['s_nachname']  . "</td>" .
               "<td>" . $row['s_strasse']   . "</td>" .
               "<td>" . $row['s_hausnummer']   . "</td>" .
               "<td>" . $row['s_ort']   . "</td>" .
               "<td>" . $row['s_plz']   . "</td>" .
               "<td>" . $row['s_telefonnummer']   . "</td>" .
               "<td>" . $row['l_nachname']   . "</td>" .
               "<td>" . $row['k_wochentag']   . "</td>" .
               "<td>" . $row['k_uhrzeit']   . "</td>" .
             "</tr>";
      }
echo '</table>'; 
$con = null;
?>

<br><br><button>Einen anderen Kurs wählen!</button> &ensp; <button formaction="index.php">Zurück zur Startseite!</button>

</form>

</body>

</html>
