<?php
/*
 * index.php
 * 
 * Autor: Rörich
 * Datum: 21.05.2019
 * Zweck: Konstruktion von Abfragen und Views für die Musikschule
 */
?>

<!--Dekleration der Html-Bedingungen (V)-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="sw">

<!--Anbindung der CSS-Datei zur Formatierung (V)-->
<link rel="stylesheet" href="lwbstyle.css">

<!--Kopf und Eigenschaften der Html-Datei (Metadaten) (V)-->
<head>
	<title>Musikschule-Lehrerauswahl</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>

<!--Beginn des eigentlichen Dokuments (I)-->
<body>

<!--Form bezeichnet die Seite als Formular und lget die Get- oder 
	Post-Methode fest, mit der die entsprechenden Formular eingaben 
	ausgewertet werden.(I) -->
	
<form action="schuelerausgabe-nach-lehrer.php" method="GET">	
	<h1 align="center">Musikschule Röhrig</h1>
	


<h2>Wählen Sie den Lehrer/in und den Wochentag an dem Sie/Er fehlt aus:</h2>
<!--Beginn des PHP-Abschnitts II-->

<?php

include_once ('conn-inc.php');

//Die hierbeschriebenen Zugangsdaten sind in der Datei conn-inc.php hinterlegt und werden an dieser Stelle eingefügt.
//$con = new PDO ('pgsql:host=localhost;dbname=lewein' , 'lewein' , 'niewel');
                    
//Die eigentliche SQL-Abfrage wird hier in einer Varaibele eingetragen
                    
$lehrer = $con->query ("SELECT l_nachname FROM lehrer;");

echo '<select name="Lehrer">';


foreach ($lehrer as $row) {
	echo '<option value="'.$row['l_nachname'].'">'.$row['l_nachname'].'</option>';
}

echo '</select> &ensp;';

unset ($row);

$wotag = $con->query ("SELECT distinct k_wochentag FROM kurse order by k_wochentag;");

echo '<select name="Wochentag">';


foreach ($wotag as $row) {
	echo '<option value="'.$row['k_wochentag'].'">'.$row['k_wochentag'].'</option>';
}

echo '</select> &emsp;';

unset ($row);

$con = null;

?>
&ensp; <button type="submit">Auswahl bestätigen!</button>

<br><br><br><br><button formaction="index.php">Zurück zur Startseite!</button><br><br>
</form>

</body>

</html>
