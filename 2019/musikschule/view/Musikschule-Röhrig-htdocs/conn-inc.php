<?php
/*
 * conn-inc.php
 * 
 * Autor: Röhrig
 * Datum: 21.05.2019
 * Zweck: Konstruktion von Abfragen und Views für die Musikschule Röhrig
 * 
 * 
 * Dies php-Seite dient dazu, dass die Verbindung zur Datenbank nur an einerstelle geändert werden muss und nicht auf allen Ansichtsseiten.
 */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Verbindungskonfiguration</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.34.1" />
</head>

<body>
	<?php
// die Konstanten auslagern in eigene Datei
// die dann per require_once ('conn-inc.php'); 
// geladen wird.
 
// Damit alle Fehler angezeigt werden
error_reporting(E_ALL);
 
// Zum Aufbau der Verbindung zur Datenbank
// die Zugangsdaten zur Datenbank erhalten Sie von Ihrem Provider
$con = new PDO ('pgsql:host=localhost;dbname=lewein' , 'lewein' , 'niewel');
?>
</body>

</html>
