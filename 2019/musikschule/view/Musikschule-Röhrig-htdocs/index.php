<?php
/*
 * index.php
 * 
 * Autor: Röhrig
 * Datum: 21.05.2019
 * Zweck: Konstruktion von Abfragen und Views für die Musikschule Röhrig
 */

?>
<!--Dekleration der Html-Bedingungen (V)-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="sw">

<!--Anbindung der CSS-Datei zur Formatierung (V)-->
<link rel="stylesheet" href="lwbstyle.css">

<!--Kopf und Eigenschaften der Html-Datei (Metadaten) (V)-->
<head>
	<title>Musikschule Röhrig</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>

<!--Beginn des eigentlichen Dokuments (I)-->
<body>

<!--Form bezeichnet die Seite als Formular und legt die Get- oder 
	Post-Methode fest, mit der die entsprechenden Formular eingaben 
	ausgewertet werden.(I) -->
	
<form action="kursauswahl.php" method="POST">	
<!--Überschrift der Seite (I) --> 
	<h1 align="center">Musikschule Röhrig</h1>
	
<!--Einleitungstext-->
<br><h2>Hier sind alle Schüler angezeigt, die an der Musikschule Röhricht Musikschüler sind.</h2>

<!--Beginn des PHP-Abschnitts-->

<?php
/* Ab hier wird eine Verknüpfung zur SQL-Datenbank hergestellt. Die Verknüpfung zur Datenbasis
 * erfolgt per "connect-Befehl". Dieser kann je nach verwendeter Datenbasis unterschiedlich sein.
 * Bei PSQL lautet der Befehl pg_connect ("host=sheep port=5432 dbname=mary user=lamb password=foo");
 * Mit Hilfe von require_once wird eine PHP-Datei (hier: 'konfiguration.php') eingeladen,
 * die die Benutzerangaben und Verbindungen enthält. Die Verbindung wird erst tatsächlich hergestellt,
 * sobald eine Anfrage an die Datenbank gestellt wird. 
 *
 * 
 * !!!!!!!!ACHTUNG: Die konfiguration.php enthält wichtige Daten, die auch Passwörter und persönliche
 * Daten enthalten - diese Datei ist unbedingt weiterzugeben!*/
 
/*!!!!!! Die Verbindung zur Datenbank ist in der Datei conn-inc.php hinterlegt*/

include_once ('conn-inc.php');

//Die hierbeschriebenen Zugangsdaten sind in der Datei conn-inc.php hinterlegt und werden an dieser Stelle eingefügt.
//$con = new PDO ('pgsql:host=localhost;dbname=lewein' , 'lewein' , 'niewel');

                    
//Die eigentliche SQL-Abfrage wird hier in einer Varaibele eingetragen (II)                    

/* Das Ergebnis wird in der Variablen db_erg gespeichert. Der Nutzer erhält
 * hier ALLE Einträge der Tabelle.*/
$db_erg = $con->query("SELECT *
                       FROM   schueler");

/* Hier beginnt die Tabelle, diese Zeigt auf der ersten Seite alle 
 * vorhandenen Produkte.
 * Es beginnt mit <th>. Alles zwischen diesen Bezeichnern stellt die 
 * Überschrift der Tabelle dar. (V, II)*/
echo "<table><tr><th>Vorname</th><th>Nachname</th><th>Straße</th><th>Hausnr.</th><th>Ort</th><th>PLZ</th><th>Telnr.</th></tr>";
      foreach ($db_erg as $row) {
        echo "<tr>" .
               "<td>" . $row['s_vorname']   . "</td>" .
               "<td>" . $row['s_nachname']  . "</td>" .
               "<td>" . $row['s_strasse']   . "</td>" .
               "<td>" . $row['s_hausnummer']   . "</td>" .
               "<td>" . $row['s_ort']   . "</td>" .
               "<td>" . $row['s_plz']   . "</td>" .
               "<td>" . $row['s_telefonnummer']   . "</td>" .
             "</tr>";
      }
echo '</table>'; 
$con = null;
//Hier endet die Tabelle.
?>

<br><br><p><b>Mit dem Klick auf den Button gelangt man zur Kursauswahl, bzw. zur Auswahl bei Lehrer-Ausfall.</b><br></p>
<!--Hier können noch weitere Ausführungen folgen was auf der Seite getan werden kann-->

<!-- Mit dem Klick auf den Button öffnet sich die in <form> angegebene 
	 Seite. Diese ist in diesem Fall die Seite Produktsuche.php-->
<button>Nach Kurs filtern!</button> &ensp; <button formaction="lehrerauswahl.php">Ein/e Lehrer/in fehlt!</button><br><br>

</form>

</body>

</html>
