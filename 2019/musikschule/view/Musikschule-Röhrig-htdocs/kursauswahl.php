
<!--Dekleration der Html-Bedingungen (V)-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="sw">

<!--Anbindung der CSS-Datei zur Formatierung (V)-->
<link rel="stylesheet" href="lwbstyle.css">

<!--Kopf und Eigenschaften der Html-Datei (Metadaten) (V)-->
<head>
	<title>Musikschule Röhrig</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>

<!--Beginn des eigentlichen Dokuments (I)-->
<body>


	
<form action="schuelerausgabe.php" method="GET">	
<!--Überschrift der Seite (I) --> 
	<h1 align="center">Musikschule Röhrig</h1>
	
<!--Einleitungstext-->
<br><h2>Wähle Sie einen Kurs:</h2>


<!--Beginn des PHP-Abschnitts-->

<?php

include_once ('conn-inc.php');

//Die hierbeschriebenen Zugangsdaten sind in der Datei conn-inc.php hinterlegt und werden an dieser Stelle eingefügt.
//$con = new PDO ('pgsql:host=localhost;dbname=lewein' , 'lewein' , 'niewel');
                    
//Die eigentliche SQL-Abfrage wird hier in einer Varaibele eingetragen (II)
                    
$kurse = $con->query ("SELECT k_name FROM kurse");
echo '<select name="Kurs">';

foreach ($kurse as $row) {
	echo '<option value="'. $row['k_name']   .'">'. $row['k_name']   .'</option>';
}
echo '</select>';
$con = null;
?>

&ensp; <button type="submit">Kurs bestätigen!</button><br><br>

<br><br><button type="submit" formaction="index.php">Zurück zur Startseite!</button>



</form>

</body>

</html>
