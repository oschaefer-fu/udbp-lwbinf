Angabe der funktionalen Abhängigkeiten der Relationen

Relation: Faelle
ID_Fall -> F_Aktenzeichen, F_Bezeichnung
F_Aktenzeichen -> F_Bezeichnung


Relation: Mitarbeiter
ID_Mitarbeiter -> Mi_Nachname, Mi_Vorname, Mi_Funktion, Mi_Email
da eine e-mail immer eindeutig ist, gilt auch:
Mi_Email -> Mi_Nachname, Mi_Vorname, Mi_Funktion


Relation: Mandanten
ID_Mandanten -> Ma_Nachname, Ma_Vorname, Ma_Geburtsdatum, Ma_Strasse, Ma_Hausnummer, Ma_PLZ, Ma_Ort, Ma_Telefonnummer, Ma_Email, Ma_Geschlecht
eine Telefonnummer kann mehreren Mitarbeitern zugeordnet sein, aber e-mail ist eindeutig
Ma_Email -> Ma_Nachname, Ma_Vorname, Ma_Geburtsdatum, Ma_Strasse, Ma_Hausnummer, Ma_PLZ, Ma_Ort, Ma_Telefonnummer, Ma_Geschlecht


Relation: Rechnung
ID_Rechnung -> R_Datum, R_Rechnungsbetrag

Relation: Prozessgegner
ID_Prozessgegner -> P_Nachname, P_Vorname, P_Strasse, P_Hausnummer, P_PLZ, P_Ort, P_Telefonnummer, P_Email
da eine e-mail immer eindeutig ist, gilt auch:
P_Email -> P_Nachname, P_Vorname, P_Strasse, P_Hausnummer, P_PLZ, P_Ort, P_Telefonnummer


Relation: Gericht
ID_Gericht -> G_Name, G_Strasse, G_Hausnummer, G_PLZ, G_Ort, G_Email
da eine e-mail immer eindeutig ist, gilt auch:
G_Email -> G_Name, G_Strasse, G_Hausnummer, G_PLZ, G_Ort


Relation: Zuordnung
ID_Fall, ID_Mitarbeiter  -> ID_Fall, ID_Mitarbeiter

 
Relation: Verhandlung
ID_Gericht, ID_Fall -> ID_Gericht, ID_Fall, V_Datum, V_Uhrzeit











