/sql
/multiline    on
/type_casting on

CREATE TABLE gericht 
(
	id_gericht	 			INTEGER NOT NULL PRIMARY KEY, 
	g_name		 			VARCHAR (40), 
	g_strasse 				VARCHAR (40), 
	g_hausnummer 			VARCHAR (10), 
	g_plz 					CHAR (5), 
	g_ort 					VARCHAR (40), 
	g_email 				VARCHAR (40) 
);

CREATE TABLE mandanten 
(
	id_mandant 			INTEGER NOT NULL PRIMARY KEY, 
	ma_nachname 		VARCHAR (40), 
	ma_vorname 			VARCHAR (40), 
	ma_geburtsdatum 	DATE, 
	ma_strasse 			VARCHAR (40), 
	ma_hausnummer 		VARCHAR (10), 
	ma_plz 				CHAR (5), 
	ma_ort 				VARCHAR (40), 
	ma_telefonnummer 	VARCHAR (40), 
	ma_email 			VARCHAR (40), 
	ma_geschlecht 		VARCHAR (1) CHECK (ma_geschlecht in ('m','w','d'))
);

CREATE TABLE mitarbeiter 
(
	id_mitarbeiter 		INTEGER NOT NULL PRIMARY KEY, 
	mi_nachname 		VARCHAR (40), 
	mi_vorname 			VARCHAR (40), 
	mi_funktion 		VARCHAR (40), 
	mi_email 			VARCHAR (40) 
);

CREATE TABLE faelle 
(
	id_fall 			INTEGER NOT NULL PRIMARY KEY, 
	f_aktenzeichen		VARCHAR(40), 
	f_bezeichnung 		VARCHAR(50),
	id_mandant 			INTEGER REFERENCES mandanten (id_mandant)		-- Fremdschlüssel
);

CREATE TABLE prozessgegner 
(
	id_prozessgegner 	INTEGER NOT NULL PRIMARY KEY, 
	p_nachname 			VARCHAR (40), 
	p_vorname 			VARCHAR (40), 
	p_strasse 			VARCHAR (40), 
	p_hausnummer 		VARCHAR (10), 
	p_plz 				CHAR (5), 
	p_ort 				VARCHAR (40), 
	p_telefonnummer 	VARCHAR (40), 
	p_email 			VARCHAR (40),
	id_fall 			INTEGER REFERENCES faelle (id_fall)				-- Fremdschlüssel
);

CREATE TABLE rechnung 
(
	id_rechnung 		INTEGER NOT NULL PRIMARY KEY, 
	r_datum 			DATE, 
	r_rechnungsbetrag 	NUMERIC (8,2), 									-- Währung 2 Nachkommastellen, Betrag bis 999999.99 € 
	id_fall 			INTEGER REFERENCES faelle (id_fall)				-- Fremdschlüssel
);

CREATE TABLE zuordnung
(
	id_fall 			INTEGER REFERENCES faelle (id_fall),				-- Fremdschlüssel
	id_mitarbeiter		INTEGER REFERENCES mitarbeiter (id_mitarbeiter)		-- Fremdschlüssel
);

CREATE TABLE verhandlung
(
	id_gericht			INTEGER REFERENCES gericht (id_gericht),			-- Fremdschlüssel
	id_fall 			INTEGER REFERENCES faelle (id_fall),				-- Fremdschlüssel
	v_datum 			DATE,
	v_uhrzeit			TIME
);

--- Kanzlei
--- bufler, helfer, küppers, michel
--- Erstellung einer Population in SQL

insert into gericht (id_gericht,g_name,g_strasse,g_hausnummer,g_plz,g_ort,g_email) values (111,'Kammergericht','Grunewaldstrasse','23','14444','Berlin','kammergericht@justiz-berlin.de');
insert into gericht values (222,'Arbeitsgericht','Magdeburger PLatz','1','10785','Berlin','Arbeitsgericht@justiz-berlin.de');
insert into gericht values (223,'Landesarbeitsgericht','Magdeburger PLatz','1','10785','Berlin','Landesarbeitsgericht@justiz-berlin.de');
insert into gericht values (333,'Verwaltungsgericht','Kirchstraße','7','10557','Berlin','Verwaltungsgericht@justiz-berlin.de');
insert into gericht values (334,'Oberverwaltungsgericht','Hardenbergstraße','31','10623','Berlin','Oberverwaltungsgericht@justiz-berlin.de');
insert into gericht values (444,'Amtsgericht Wedding','Brunnenplatz','1','13357','Berlin','Amtsgericht-Wedding@justiz-berlin.de');
insert into gericht values (445,'Amtsgericht Schöneberg','Grunewaldstraße','66-67','10823','Berlin','Amtsgericht-Schöneberg@justiz-berlin.de');

insert into mandanten (id_mandant,ma_nachname,ma_vorname,ma_geburtsdatum,ma_strasse,ma_hausnummer,ma_plz,ma_ort,ma_telefonnummer,ma_email,ma_geschlecht) values (1111,'Schmidt','Stefan',date '1973-02-01','Poststrasse','12','13594','Berlin','030-2342348','st.schmidt@gmail.com','m');
insert into mandanten values (1112,'Gerlach','Dieter',date '1963-04-02','Bahnhofstrasse','31','12203','Berlin','030-2342334','d.Gerlach@gerlach.de','m');
insert into mandanten values (1113,'Peters','Peter',date '1923-05-03','Waldstrasse','2','12204','Berlin','030-2342322','pp@petersmobile.de','m');
insert into mandanten values (1114,'Michel','Michael',date '1965-07-20','Dr.Michel-Strasse','1','12203','Berlin','030-23433333','BigBadDrM@mafiamichel.de','d');
insert into mandanten values (1115,'Lustig','Peter',date '1977-11-14','Kurfürstendamm','200','10499','Leipzig','0341-1234567','Löwenzahn@t-online.de','m');
insert into mandanten values (1116,'Alwadi','Memmet',date '2000-12-24','Unter den Linden','2','10455','Leipzig','0341-2020345','ichhabnixgemacht@innocent.de','m');
insert into mandanten values (1117,'Tschaikowsky','Petra',date '1982-09-23','Philharmoniestrasse','101','13595','Berlin','030-2342348','Nummer5@online.de','w');
insert into mandanten values (1118,'Wagner','Richard',date '1949-04-01','Bayreuther Strasse ','3','13595','Berlin','030-2342348','Thor@odin.de','m');
insert into mandanten values (1119,'Bach','Sebastian',date '1986-05-30','Leipziger Strasse','58','10100','Dresden','0311- 34593459','Fugenmeister@dt.de','m');
insert into mandanten values (1120,'Beethoven','Ludwig',date '1982-09-23','Mendelssohn-Strasse','11','12120','Radebeul','03221-4958576','LvB@LvB.de','m');
insert into mandanten values (1121,'Shnurov','Igor',date '1940-04-04','St. Petersburger-Allee','34','95448','Eckersdorf','0921-450453','Shnur@leningrad.ru','m');
insert into mandanten values (1122,'Gomez','Marion',date '1989-01-21','Looser Weg','2','95544','Bayreuth','0921-229228','Gomez@vfb.de','w');
insert into mandanten values (1123,'Reus','Toni',date '1978-10-28','Dortmunder Strasse','1','13599','Berlin','030-2342348','Toni@bvb.de','m');
insert into mandanten values (1124,'Ribery','Franque',date '1988-01-23','Münchner Strasse','2019','12201','Berlin','030-29933945','jaderFranqu@fcb.de','m');
insert into mandanten values (1125,'Simmons','Gene',date '1980-01-31','Kiss-Promenade','666','11011','Potsdam','0335-4596853','Tongue@Kiss.com','m');
insert into mandanten values (1126,'De Guzman','Peer',date '1988-03-24','Fantasie Strasse','33','12203','Berlin','030-2342348','Peer@Leer.de','m');
insert into mandanten values (1127,'Ling','Mai',date '2000-05-17','Falkenkorso','22','13959','Berlin','030-2342348','MaiLing@polt.de','w');
insert into mandanten values (1128,'Thai','Mai',date '1999-09-09','Promillenweg','11','13959','Berlin','030-2342348','MaiThai@cocktails.com','w');
insert into mandanten values (1129,'Bond','James',date '1967-02-03','Aston-Martin-Weg','7','13955','Berlin','030-2342348','007@MI6.uk','m');
insert into mandanten values (1130,'Skunk','Anansie',date '1967-03-02','Biene Maja Parcours','23','14555','Hamburg','040-2000456','rockon@skunk.com','w');
insert into mandanten values (1131,'Bond','Jane',date '1966-02-04','Moneypenny-Lane','700','80222','München','089-21234556','Bondgirl@MI6.uk','w');

insert into mitarbeiter (id_mitarbeiter,mi_nachname,mi_vorname,mi_funktion,mi_email) values (111,'Dr. Rechtsbeuger','Friedrich','Geschäftsführer','rechtsbeuger@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (112,'Dr. Fürchtegott','Hermann','Anwalt','fürchtegott@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (113,'Dr.Watts','Hermine','Geschäftsführer','watts@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (114,'Schmidt','Latetia','ReNo','schmidt@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (115,'Lolle','Lotta','ReNo','lolle@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (116,'Boffler','Erik','Anwalt','boffler@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (117,'Mathula','Heinz','Detektiv','mathula@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (118,'Kersten','Kerstin','Sekretärin','kersten@kanzlei-rechtsbeuger.de');

insert into faelle (id_fall,f_aktenzeichen,f_bezeichnung,id_mandant) values (12345,'2019_12345_bk','Fahrradunfall',1114);
insert into faelle values (12346,'2019_12346_bl','Falschparken',1116);
insert into faelle values (12347,'2019_12347_rb','Arbeitsvertragsverletzung',1118);
insert into faelle values (12348,'2019_12348_ra','Schwarzfahren',1120);
insert into faelle values (12349,'2019_12349_bl','Wohnungskündigung',1122);
insert into faelle values (12350,'2019_12350_ts','Wohnungseinbruch',1124);
insert into faelle values (12351,'2019_12351_wl','Datendiebstahl',1126);
insert into faelle values (12352,'2019_12352_gb','Beleidigung',1128);
insert into faelle values (12353,'2019_12353_ls','Diebstahl',1130);
insert into faelle values (12354,'2019_12354_zy','Einbruch',1131);


insert into prozessgegner (id_prozessgegner,p_nachname,p_vorname,p_strasse,p_hausnummer,p_plz,p_ort,p_telefonnummer,p_email,id_fall) values (1111,'Liszt','Franz','Opernallee','7','12222','Berlin','030-222333445','liszt@gmail.com',12346);
insert into prozessgegner values (1112,'BVB','Dortmund','An der Westfalenhalle','1','17777','Dortmund','020-3334445566','anwalt@bvb.de',12347);
insert into prozessgegner values (1113,'Artemis','Jean','Hohenzollerndamm','6','12222','Berlin','030-6666661','lust@artemis.de',12348);
insert into prozessgegner values (1114,'Berlin','Land','Unter den Linden','1','12222','Berlin','030-11111123','justizstelle@berlinonline.de',12349);
insert into prozessgegner values (1115,'Sony','Karl','Sonycenter','22','12444','Berlin','030-232323234','karl@sony.de',12350);
insert into prozessgegner values (1116,'Phillips','Sean','Kleiststrasse','2A','10787','Berlin','030-232325634','phillips@sony.de',12350);
insert into prozessgegner values (1117,'Krause','Fritz','Unter den Brücken','2','12445','Berlin','030-2343454569','Fritzschlaeftgut@draussen.de',12351);
insert into prozessgegner values (1118,'Hoeneß','Ulrich','Säbener Strasse ','1','80001','München','089-10101010','Festgeldkonto@fcb.de',12352);
insert into prozessgegner values (1119,'Stadt','München','Maximilianstrasse','2','80003','München','089-14141414','Ordnungsamt_Abt_234@muenchen.de',12353);


insert into rechnung (id_rechnung,r_datum,r_rechnungsbetrag,id_fall) values (2019001,date '2019-01-02',3601.98,12346);
insert into rechnung values (2019002,date '2019-01-02',370.00,12347);
insert into rechnung values (2019003,date '2019-01-05',2156.90,12348);
insert into rechnung values (2019004,date '2019-01-06',2100.00,12349);
insert into rechnung values (2019005,date '2019-01-11',789.00,12350);
insert into rechnung values (2019006,date '2019-01-11',499.00,12351);
insert into rechnung values (2019007,date '2019-01-11',5499.00,12352);
insert into rechnung values (2019008,date '2019-01-20',6499.00,12353);


insert into zuordnung (id_fall,id_mitarbeiter) values (12345,111);
insert into zuordnung values (12345,112);
insert into zuordnung values (12345,117);
insert into zuordnung values (12346,114);
insert into zuordnung values (12347,116);
insert into zuordnung values (12347,118);
insert into zuordnung values (12348,112);
insert into zuordnung values (12348,117);
insert into zuordnung values (12348,118);
insert into zuordnung values (12349,115);
insert into zuordnung values (12350,116);
insert into zuordnung values (12350,118);
insert into zuordnung values (12351,112);
insert into zuordnung values (12351,114);
insert into zuordnung values (12352,111);
insert into zuordnung values (12352,116);
insert into zuordnung values (12353,113);

insert into verhandlung (id_gericht,id_fall,v_datum,v_uhrzeit) values (444,12345,date '2019-08-04',time '10:35:00');
insert into verhandlung values (333,12346,date '2019-10-08',time '09:00:00');
insert into verhandlung values (444,12353,date '2019-12-10',time '14:00:00');
insert into verhandlung values (222,12347,date '2019-09-01',time '10:00:00');
insert into verhandlung values (445,12350,date '2022-09-14',time '12:00:00');
insert into verhandlung values (445,12348,date '2020-02-02',time '09:00:00');
insert into verhandlung values (444,12349,date '2020-03-03',time '11:00:00');