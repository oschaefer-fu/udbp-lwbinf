#!/bin/bash
# Skript zur Konvertierung von (postgre)SQL-Dateien in DES-Dateien
# Das Skript übernimmt folgende Aufgaben:

#   o Umwandeln der postgreSQL-Typen in DES-Typen
#     - date:
#         'dd.mm.yyyy' -> date 'yyyy-mm-dd' (ungünstig)
#         'dd.mm.yy'   -> date 'yy-mm-dd'
#     - time:
#         'hh:mm:ss'   -> time 'hh:mm:ss'
#         'hh:mm'      -> time 'hh:mm:00'

# Nicht möglich ist das skriptgesteuerte Transformieren des Datentyps bool in integer.
# DER DATENTYP BOOL DARF IN DES NICHT VERWENDET WERDEN! Möglich wäre ein Ersetzen durch
# den Typ integer mit den Werten 0 und 1.

# Ebenso nicht möglich sind in DES Datums- und Zeit-Vergleiche wie in SQL, wo z.B.
# ... where datum < '13.04.2019' ... möglich ist. Das hat aber nur Auswirkungen auf die
# ausgewählten Anfragen, weswegen ich auf RA-Anfragen mit Datums- und Zeittypen ver-
# zichten würde.

# Im DDL-Teil der Datenbank muss man auf explizite Erzeugung von Primary-Keys verzichten,
# diese müssen direkt hinter dem deklarierten Attribut angegeben werden:
# ... isbn    varchar (11) primary key ...

# Besonders wichtig ist noch, dass alle Tabellen und Attributnamen immer und überall klein
# geschrieben werden. Der ODBC-SQL-Treiber unterscheidet nämlich Groß- und Kleinschreibung,
# postgreSQL aber nicht.

# INFILES enthält die Liste aller Dateien (in angegebener Reihenfolge), die die Schemas
# erzeugen und die Tabellen befüllen.
INFILES='Create-Kanzlei.sql Insert-Kanzlei-data.sql'
# OUTFILE ist die DES-Datei, die anschließend für die Erzeugung und Befüllung in DES ein-
# gelesen werden muss.
OUTFILE='Kanzlei_des.ddl'

echo '/sql'              > $OUTFILE
echo '/multiline    on' >> $OUTFILE
echo '/type_casting on' >> $OUTFILE

for DATEI in $INFILES;
do
  echo >> $OUTFILE
  cat $DATEI \
      | sed "s/'\([0-9][0-9]\):\([0-9][0-9]\):\([0-9][0-9]\)'/time '\1:\2:\3'/g" \
      | sed "s/'\([0-9][0-9]\):\([0-9][0-9]\)'/time '\1:\2:00'/g" \
      | sed "s/'\([0-9][0-9]\)\.\([0-9][0-9]\)\.\([0-9][0-9][0-9][0-9]\)'/date '\3-\2-\1'/g" \
      | sed "s/'\([0-9][0-9]\)\.\([0-9][0-9]\)\.\([0-9][0-9]\)'/date '\3-\2-\1'/g" \
      >> $OUTFILE
done
