CREATE TABLE gericht 
(
	id_gericht	 			INTEGER NOT NULL PRIMARY KEY, 
	g_name		 			VARCHAR (40), 
	g_strasse 				VARCHAR (40), 
	g_hausnummer 			VARCHAR (10), 
	g_plz 					CHAR (5), 
	g_ort 					VARCHAR (40), 
	g_email 				VARCHAR (40) 
);

CREATE TABLE mandanten 
(
	id_mandant 			INTEGER NOT NULL PRIMARY KEY, 
	ma_nachname 		VARCHAR (40), 
	ma_vorname 			VARCHAR (40), 
	ma_geburtsdatum 	DATE, 
	ma_strasse 			VARCHAR (40), 
	ma_hausnummer 		VARCHAR (10), 
	ma_plz 				CHAR (5), 
	ma_ort 				VARCHAR (40), 
	ma_telefonnummer 	VARCHAR (40), 
	ma_email 			VARCHAR (40), 
	ma_geschlecht 		VARCHAR (1) CHECK (ma_geschlecht in ('m','w','d'))
);

CREATE TABLE mitarbeiter 
(
	id_mitarbeiter 		INTEGER NOT NULL PRIMARY KEY, 
	mi_nachname 		VARCHAR (40), 
	mi_vorname 			VARCHAR (40), 
	mi_funktion 		VARCHAR (40), 
	mi_email 			VARCHAR (40) 
);

CREATE TABLE faelle 
(
	id_fall 			INTEGER NOT NULL PRIMARY KEY, 
	f_aktenzeichen		VARCHAR(40), 
	f_bezeichnung 		VARCHAR(50),
	id_mandant 			INTEGER REFERENCES mandanten (id_mandant)		-- Fremdschlüssel
);

CREATE TABLE prozessgegner 
(
	id_prozessgegner 	INTEGER NOT NULL PRIMARY KEY, 
	p_nachname 			VARCHAR (40), 
	p_vorname 			VARCHAR (40), 
	p_strasse 			VARCHAR (40), 
	p_hausnummer 		VARCHAR (10), 
	p_plz 				CHAR (5), 
	p_ort 				VARCHAR (40), 
	p_telefonnummer 	VARCHAR (40), 
	p_email 			VARCHAR (40),
	id_fall 			INTEGER REFERENCES faelle (id_fall)				-- Fremdschlüssel
);

CREATE TABLE rechnung 
(
	id_rechnung 		INTEGER NOT NULL PRIMARY KEY, 
	r_datum 			DATE, 
	r_rechnungsbetrag 	NUMERIC (8,2), 									-- Währung 2 Nachkommastellen, Betrag bis 999999.99 € 
	id_fall 			INTEGER REFERENCES faelle (id_fall)				-- Fremdschlüssel
);

CREATE TABLE zuordnung
(
	id_fall 			INTEGER REFERENCES faelle (id_fall),				-- Fremdschlüssel
	id_mitarbeiter		INTEGER REFERENCES mitarbeiter (id_mitarbeiter)		-- Fremdschlüssel
);

CREATE TABLE verhandlung
(
	id_gericht			INTEGER REFERENCES gericht (id_gericht),			-- Fremdschlüssel
	id_fall 			INTEGER REFERENCES faelle (id_fall),				-- Fremdschlüssel
	v_datum 			DATE,
	v_uhrzeit			TIME
);
