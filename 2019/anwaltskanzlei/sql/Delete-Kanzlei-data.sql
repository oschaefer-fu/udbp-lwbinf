-- Kanzlei
-- bufler, helfer, küppers, michel
-- Löschen der Inhalte der Relationen in SQL


Truncate TABLE verhandlung		CASCADE;
Truncate TABLE zuordnung		CASCADE;
Truncate TABLE rechnung			CASCADE;
Truncate TABLE prozessgegner	CASCADE;
Truncate TABLE faelle			CASCADE;
Truncate TABLE mitarbeiter		CASCADE;
Truncate TABLE mandanten		CASCADE;
Truncate TABLE gericht			CASCADE;




