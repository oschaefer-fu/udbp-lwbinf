--- Kanzlei
--- bufler, helfer, küppers, michel
--- Erstellung einer Population in SQL

insert into gericht (id_gericht,g_name,g_strasse,g_hausnummer,g_plz,g_ort,g_email) values (111,'Kammergericht','Grunewaldstrasse','23','14444','Berlin','kammergericht@justiz-berlin.de');
insert into gericht values (222,'Arbeitsgericht','Magdeburger PLatz','1','10785','Berlin','Arbeitsgericht@justiz-berlin.de');
insert into gericht values (223,'Landesarbeitsgericht','Magdeburger PLatz','1','10785','Berlin','Landesarbeitsgericht@justiz-berlin.de');
insert into gericht values (333,'Verwaltungsgericht','Kirchstraße','7','10557','Berlin','Verwaltungsgericht@justiz-berlin.de');
insert into gericht values (334,'Oberverwaltungsgericht','Hardenbergstraße','31','10623','Berlin','Oberverwaltungsgericht@justiz-berlin.de');
insert into gericht values (444,'Amtsgericht Wedding','Brunnenplatz','1','13357','Berlin','Amtsgericht-Wedding@justiz-berlin.de');
insert into gericht values (445,'Amtsgericht Schöneberg','Grunewaldstraße','66-67','10823','Berlin','Amtsgericht-Schöneberg@justiz-berlin.de');

insert into mandanten (id_mandant,ma_nachname,ma_vorname,ma_geburtsdatum,ma_strasse,ma_hausnummer,ma_plz,ma_ort,ma_telefonnummer,ma_email,ma_geschlecht) values (1111,'Schmidt','Stefan','01.02.1973','Poststrasse','12','13594','Berlin','030-2342348','st.schmidt@gmail.com','m');
insert into mandanten values (1112,'Gerlach','Dieter','02.04.1963','Bahnhofstrasse','31','12203','Berlin','030-2342334','d.Gerlach@gerlach.de','m');
insert into mandanten values (1113,'Peters','Peter','03.05.1923','Waldstrasse','2','12204','Berlin','030-2342322','pp@petersmobile.de','m');
insert into mandanten values (1114,'Michel','Michael','20.07.1965','Dr.Michel-Strasse','1','12203','Berlin','030-23433333','BigBadDrM@mafiamichel.de','d');
insert into mandanten values (1115,'Lustig','Peter','14.11.1977','Kurfürstendamm','200','10499','Leipzig','0341-1234567','Löwenzahn@t-online.de','m');
insert into mandanten values (1116,'Alwadi','Memmet','24.12.2000','Unter den Linden','2','10455','Leipzig','0341-2020345','ichhabnixgemacht@innocent.de','m');
insert into mandanten values (1117,'Tschaikowsky','Petra','23.09.1982','Philharmoniestrasse','101','13595','Berlin','030-2342348','Nummer5@online.de','w');
insert into mandanten values (1118,'Wagner','Richard','01.04.1949','Bayreuther Strasse ','3','13595','Berlin','030-2342348','Thor@odin.de','m');
insert into mandanten values (1119,'Bach','Sebastian','30.05.1986','Leipziger Strasse','58','10100','Dresden','0311- 34593459','Fugenmeister@dt.de','m');
insert into mandanten values (1120,'Beethoven','Ludwig','23.09.1982','Mendelssohn-Strasse','11','12120','Radebeul','03221-4958576','LvB@LvB.de','m');
insert into mandanten values (1121,'Shnurov','Igor','04.04.1940','St. Petersburger-Allee','34','95448','Eckersdorf','0921-450453','Shnur@leningrad.ru','m');
insert into mandanten values (1122,'Gomez','Marion','21.01.1989','Looser Weg','2','95544','Bayreuth','0921-229228','Gomez@vfb.de','w');
insert into mandanten values (1123,'Reus','Toni','28.10.1978','Dortmunder Strasse','1','13599','Berlin','030-2342348','Toni@bvb.de','m');
insert into mandanten values (1124,'Ribery','Franque','23.01.1988','Münchner Strasse','2019','12201','Berlin','030-29933945','jaderFranqu@fcb.de','m');
insert into mandanten values (1125,'Simmons','Gene','31.01.1980','Kiss-Promenade','666','11011','Potsdam','0335-4596853','Tongue@Kiss.com','m');
insert into mandanten values (1126,'De Guzman','Peer','24.03.1988','Fantasie Strasse','33','12203','Berlin','030-2342348','Peer@Leer.de','m');
insert into mandanten values (1127,'Ling','Mai','17.05.2000','Falkenkorso','22','13959','Berlin','030-2342348','MaiLing@polt.de','w');
insert into mandanten values (1128,'Thai','Mai','09.09.1999','Promillenweg','11','13959','Berlin','030-2342348','MaiThai@cocktails.com','w');
insert into mandanten values (1129,'Bond','James','03.02.1967','Aston-Martin-Weg','7','13955','Berlin','030-2342348','007@MI6.uk','m');
insert into mandanten values (1130,'Skunk','Anansie','02.03.1967','Biene Maja Parcours','23','14555','Hamburg','040-2000456','rockon@skunk.com','w');
insert into mandanten values (1131,'Bond','Jane','04.02.1966','Moneypenny-Lane','700','80222','München','089-21234556','Bondgirl@MI6.uk','w');

insert into mitarbeiter (id_mitarbeiter,mi_nachname,mi_vorname,mi_funktion,mi_email) values (111,'Dr. Rechtsbeuger','Friedrich','Geschäftsführer','rechtsbeuger@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (112,'Dr. Fürchtegott','Hermann','Anwalt','fürchtegott@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (113,'Dr.Watts','Hermine','Geschäftsführer','watts@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (114,'Schmidt','Latetia','ReNo','schmidt@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (115,'Lolle','Lotta','ReNo','lolle@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (116,'Boffler','Erik','Anwalt','boffler@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (117,'Mathula','Heinz','Detektiv','mathula@kanzlei-rechtsbeuger.de');
insert into mitarbeiter values (118,'Kersten','Kerstin','Sekretärin','kersten@kanzlei-rechtsbeuger.de');

insert into faelle (id_fall,f_aktenzeichen,f_bezeichnung,id_mandant) values (12345,'2019_12345_bk','Fahrradunfall',1114);
insert into faelle values (12346,'2019_12346_bl','Falschparken',1116);
insert into faelle values (12347,'2019_12347_rb','Arbeitsvertragsverletzung',1118);
insert into faelle values (12348,'2019_12348_ra','Schwarzfahren',1120);
insert into faelle values (12349,'2019_12349_bl','Wohnungskündigung',1122);
insert into faelle values (12350,'2019_12350_ts','Wohnungseinbruch',1124);
insert into faelle values (12351,'2019_12351_wl','Datendiebstahl',1126);
insert into faelle values (12352,'2019_12352_gb','Beleidigung',1128);
insert into faelle values (12353,'2019_12353_ls','Diebstahl',1130);
insert into faelle values (12354,'2019_12354_zy','Einbruch',1131);


insert into prozessgegner (id_prozessgegner,p_nachname,p_vorname,p_strasse,p_hausnummer,p_plz,p_ort,p_telefonnummer,p_email,id_fall) values (1111,'Liszt','Franz','Opernallee','7','12222','Berlin','030-222333445','liszt@gmail.com',12346);
insert into prozessgegner values (1112,'BVB','Dortmund','An der Westfalenhalle','1','17777','Dortmund','020-3334445566','anwalt@bvb.de',12347);
insert into prozessgegner values (1113,'Artemis','Jean','Hohenzollerndamm','6','12222','Berlin','030-6666661','lust@artemis.de',12348);
insert into prozessgegner values (1114,'Berlin','Land','Unter den Linden','1','12222','Berlin','030-11111123','justizstelle@berlinonline.de',12349);
insert into prozessgegner values (1115,'Sony','Karl','Sonycenter','22','12444','Berlin','030-232323234','karl@sony.de',12350);
insert into prozessgegner values (1116,'Phillips','Sean','Kleiststrasse','2A','10787','Berlin','030-232325634','phillips@sony.de',12350);
insert into prozessgegner values (1117,'Krause','Fritz','Unter den Brücken','2','12445','Berlin','030-2343454569','Fritzschlaeftgut@draussen.de',12351);
insert into prozessgegner values (1118,'Hoeneß','Ulrich','Säbener Strasse ','1','80001','München','089-10101010','Festgeldkonto@fcb.de',12352);
insert into prozessgegner values (1119,'Stadt','München','Maximilianstrasse','2','80003','München','089-14141414','Ordnungsamt_Abt_234@muenchen.de',12353);


insert into rechnung (id_rechnung,r_datum,r_rechnungsbetrag,id_fall) values (2019001,'02.01.2019',3601.98,12346);
insert into rechnung values (2019002,'02.01.2019',370.00,12347);
insert into rechnung values (2019003,'05.01.2019',2156.90,12348);
insert into rechnung values (2019004,'06.01.2019',2100.00,12349);
insert into rechnung values (2019005,'11.01.2019',789.00,12350);
insert into rechnung values (2019006,'11.01.2019',499.00,12351);
insert into rechnung values (2019007,'11.01.2019',5499.00,12352);
insert into rechnung values (2019008,'20.01.2019',6499.00,12353);


insert into zuordnung (id_fall,id_mitarbeiter) values (12345,111);
insert into zuordnung values (12345,112);
insert into zuordnung values (12345,117);
insert into zuordnung values (12346,114);
insert into zuordnung values (12347,116);
insert into zuordnung values (12347,118);
insert into zuordnung values (12348,112);
insert into zuordnung values (12348,117);
insert into zuordnung values (12348,118);
insert into zuordnung values (12349,115);
insert into zuordnung values (12350,116);
insert into zuordnung values (12350,118);
insert into zuordnung values (12351,112);
insert into zuordnung values (12351,114);
insert into zuordnung values (12352,111);
insert into zuordnung values (12352,116);
insert into zuordnung values (12353,113);

insert into verhandlung (id_gericht,id_fall,v_datum,v_uhrzeit) values (444,12345,'04.08.2019','10:35');
insert into verhandlung values (333,12346,'08.10.2019','09:00');
insert into verhandlung values (444,12353,'10.12.2019','14:00');
insert into verhandlung values (222,12347,'01.09.2019','10:00');
insert into verhandlung values (445,12350,'14.09.2022','12:00');
insert into verhandlung values (445,12348,'02.02.2020','09:00');
insert into verhandlung values (444,12349,'03.03.2020','11:00');