<!DOCTYPE html>
<?php
//Datenbank includieren
include("conn-inc.php");
?>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="Kanzlei.css">
  </style>
</head>
<body>

 
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.html">Advocat</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="../index.html">Home</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mandanten
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../mandantsuchen.php">Mandanten suchen</a></li>
		  <li><a href="../mandantenanlegen.php">Mandant anlegen</a></li>
		  <li><a href="../allemandanten.php">Mandanten</a></li>
        </ul>
      </li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Fälle
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../fallsuchen.php">Fall suchen</a></li>
		  <li><a href="../fallanlegen.php">Fall anlegen</a></li>
		  <li><a href="../allefaelle.php">Fälle</a></li>
        </ul>
	</li>
    <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gerichte
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allegerichte.php">Gerichte</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Verhandlungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleverhandlungen.php">Verhandlungen</a></li>
        </ul>
	</li>
	<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Prozessgegner
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleprozessgegner.php">Prozessgegner</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Rechnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allerechnungen.php">Rechnungen</a></li>
        </ul>
	</li>
	  <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Mitarbeiter
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allemitarbeiter.php">Mitarbeiter</a></li>
        </ul>
	</li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Zuordnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allezuordnungen.php">Zuordnungen</a></li>
        </ul>
	</li>
    </ul>
  </div>
</nav>

<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left"> 
	<div class="page-header">
  <h2>Fall bearbeiten</h2>
</div>
     <form name="" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" >

	  
	  <?php 
	  //Wenn ein Aktenzeichen mitgegeben wurde, werden die Daten des Falls abgefragt und ausgegeben.
	  $sql = "SELECT * FROM faelle WHERE id_fall = ".$_GET['id_fall'];
	  $result = pg_query( $db, $sql );
	  
	  	if ($result) {
		
	while($row = pg_fetch_assoc($result)){
		
echo " <div class='form-group'>";
  echo "<label for='id_fall'>id_fall:</label>";
  echo "<input type='text' class='form-control' name='id_fall' value=".$row["id_fall"]." readonly>";
	echo "</div>";
echo " <div class='form-group'>";
  echo "<label for='f_aktenzeichen'>f_aktenzeichen:</label>";
  echo "<input type='text' class='form-control' name='f_aktenzeichen' value=".$row["f_aktenzeichen"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='f_bezeichnung'>f_bezeichnung:</label>";
  echo "<input type='text' class='form-control' name='f_bezeichnung' value=".$row["f_bezeichnung"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='id_mandant'>id_mandant:</label>";
  echo "<input type='text' class='form-control' name='id_mandant' value=".$row["id_mandant"].">";
	echo "</div>";
	
	
	}
		}
  ?>


 

<input type="Submit" class="btn btn-info btn-sm" value="Speichern" name="submit">
<input type="Submit" class="btn btn-danger btn-sm" value="Löschen" name="delete">
</form>

<?php
if (isset($_POST['submit'])) {//wenn der Speichern-Button gedrückt wird, wird der Altdatenbestand in der Datenbank durch die neuen Daten ersetzt
								
		
											$sql = " UPDATE faelle ";
											$sql .= " SET ";
											$sql .= " id_fall   ='". $_POST['id_fall'] ."', ";
											$sql .= " f_aktenzeichen   ='". $_POST['f_aktenzeichen'] ."', ";
											$sql .= " f_bezeichnung   ='". $_POST['f_bezeichnung'] ."', ";
											$sql .= " id_mandant   ='". $_POST['id_mandant'] ."' ";
											
											$sql .= " WHERE id_fall = ". $_POST['id_fall'] ."";
		
											
																						
											$db_erg = pg_query( $db, $sql );
											
											
											if(!$db_erg){
												die('Ungültige Abfrage: ' . pg_error()); 
											}				
											
																					
											header("location: ../allefaelle.php");
											
									}

?>

<?php
if (isset($_POST['delete'])) {//wenn der Löschen-Button gedrückt wird, werden alle Daten mit dem entsprechenden Aktenzeichen gelöscht
										
		
											$sql = " DELETE ";
											$sql .= " FROM faelle ";
											$sql .= " WHERE id_fall = ". $_POST['id_fall'];
			
			
											
											$db_erg = pg_query( $db, $sql );
											
											
											if(!$db_erg){
												die('Ungültige Abfrage: ' . pg_error()); 
											}										

											header("location: ../allefaelle.php");
											
									}

?>



    </div>
    <div class="col-sm-2 sidenav">

      </div>
    </div>
  </div>
</div>


</body>
</html>
