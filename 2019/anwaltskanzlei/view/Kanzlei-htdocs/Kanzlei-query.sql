------------------------------------------------------------------------------
--1. Aufgabe: 
-- Wer arbeitet in der Kanzlei?
-- Gib die Nachnamen aus!

-- Projektion aus einer Tabelle
-- Schwierigkeitsgrad: leicht
SELECT mi_nachname
FROM   mitarbeiter
GROUP BY mi_nachname;

------------------------------------------------------------------------------
--2. Aufgabe: 
-- Welche Prozessgegner wohnen in Berlin?

-- Projektion aus einer Tabelle, die durch eine Bedingung eingeschränkt wurde
-- Schwierigkeitsgrad: leicht
SELECT p_vorname, p_nachname
FROM   prozessgegner
WHERE  p_ort = 'Berlin';

------------------------------------------------------------------------------
--3. Aufgabe: 
-- Welche Mandanten sind männlich?

-- Projektion aus einer Tabelle, die durch eine Bedingung eingeschränkt wurde
-- Schwierigkeitsgrad: leicht
SELECT ma_vorname, ma_nachname
FROM mandanten
WHERE ma_geschlecht = 'm';

------------------------------------------------------------------------------
--4. Aufgabe: 
-- Welche Mandanten wohnen in Leipzig?

-- Projektion aus einer Tabelle, die durch eine Bedingung eingeschränkt wurde
-- Schwierigkeitsgrad: leicht
SELECT ma_vorname, ma_nachname
FROM mandanten
WHERE ma_ort = 'Leipzig';

------------------------------------------------------------------------------
---5. Aufgabe: 
-- Stelle die Anzahl der Personen in der Mandantendatei fest.

-- Aggregatsfunktion Count verwenden
-- Schwierigkeitsgrad: leicht
SELECT COUNT(*) AS "Gesamtmandantenzahl"
FROM mandanten;

------------------------------------------------------------------------------
-- 6. Aufgabe: 
-- Welche Mandanten haben in Berlin einen Festnetzanschluss?

-- Zählung und vergleichende Musterabfrage
-- Schwierigkeitsgrad: mittel
SELECT COUNT(*) AS "Anzahl Mandanten aus Berlin mit Festnetzanschluss"
FROM mandanten
WHERE ma_ort = 'Berlin' AND ma_telefonnummer LIKE '030%' ;

------------------------------------------------------------------------------
-- 7. Aufgabe: 
-- Gebe eine alphabetisch sortierte Liste aller Mandanten aus, die nicht in Berlin wohnen!

-- Auf- oder Absteigend sortieren (ASC and DESC)
-- Schwierigkeitsgrad: mittel
SELECT ma_nachname AS "Nachname", ma_vorname AS "Vorname"
FROM mandanten
WHERE ma_ort = 'Berlin' ORDER BY ma_nachname ASC;

------------------------------------------------------------------------------
--8. Aufgabe: 
-- Welche Rechnung liegt zwischen 1000 € und 3000 €?

-- einfache Abfrage, mit Angabe von Grenzen
-- Schwierigkeitsgrad: mittel
SELECT id_rechnung, r_rechnungsbetrag
FROM rechnung
WHERE 1000 <= r_rechnungsbetrag AND 3000 >= r_rechnungsbetrag ;

------------------------------------------------------------------------------
--9. Aufgabe: 
-- Welche Berliner Mandanten sind männlich und älter als 50 Jahre?

-- einfache Abfrage, mit mehreren Bedingungen
-- Schwierigkeitsgrad: mittel
SELECT   ma_nachname, ma_geburtsdatum
FROM mandanten 
WHERE ma_geburtsdatum < '01.06.1969'
	AND ma_ort = 'Berlin'
	AND ma_geschlecht = 'm';	
	
------------------------------------------------------------------------------
--10. Aufgabe: 
-- Wie alt sind die weiblichen Mandanten?
-- Gib Vornamen und Nachnamen der Mandanten an!

-- Hinweis: Age-Funktion von SQL 
-- Age (datum1, datum2) gibt die Differenz zwischen datum1 und datum2 aus 
-- also datum1 - datum2
-- Schwierigkeitsgrad: mittel
SELECT ma_vorname, ma_nachname, AGE (Current_Date, ma_geburtsdatum)
FROM mandanten
WHERE ma_geschlecht = 'w';		

------------------------------------------------------------------------------
--11. Aufgabe:
-- Welche Streitfälle kommen am häufigsten vor? 

-- einfache Abfrage, mit mehreren Aggregatsfunktion und Sortierung
-- Schwierigkeitsgrad: mittel
SELECT f_bezeichnung AS streit, COUNT(*) AS anzahl
FROM   faelle
GROUP BY f_bezeichnung
ORDER BY anzahl DESC
limit 1;

------------------------------------------------------------------------------
-- 12. Aufgabe: 
-- Welche Mandanten wohnen in Berlin und sind älter als 50 Jahre?
-- Gebe die Namen und ihr Alter an? 

-- einfache Abfrage, mit mehreren Aggregatsfunktion und Sortierung
-- Schwierigkeitsgrad: mittel
SELECT ma_nachname, AGE (Current_Date, ma_geburtsdatum)
FROM mandanten
WHERE AGE (Current_Date, ma_geburtsdatum) > '50 years';

------------------------------------------------------------------------------
--13. Aufgabe: 
-- Wurden schon Rechnungen für Fälle gestellt, die erst im Jahr 2022
-- verhandelt werden? Gebe die Aktenzeichen an?

-- Abfrage über mehrere Tabellen, mit mehreren Bedingungen und Gruppierung
-- Schwierigkeitsgrad: schwer
SELECT f_aktenzeichen
FROM   (rechnung NATURAL JOIN faelle NATURAL JOIN verhandlung)
WHERE  v_datum > '2021-12-31'
AND v_datum < '2023-01-01'
GROUP BY f_aktenzeichen;

------------------------------------------------------------------------------
--14. Aufgabe: 
-- Wieviele Fälle bearbeiten die Mitarbeiter derzeit?

-- Abfrage über mehrere Tabellen mit Alias
-- Schwierigkeitsgrad: schwer
SELECT   mi_nachname AS Nachname, COUNT(*) AS anzahl
FROM  mitarbeiter, zuordnung 
WHERE mitarbeiter.id_mitarbeiter = zuordnung.id_mitarbeiter 
GROUP BY mi_nachname;

------------------------------------------------------------------------------
-- 15. Aufgabe:
-- Welche Mitarbeiter bearbeiten Fahrradunfälle

-- Projektion mit einer einfachen select-Anweisung aus zwei Tabellen
-- Schwierigkeitsgrad: schwer
Select mi_vorname, mi_nachname
FROM (mitarbeiter NATURAL JOIN zuordnung  NATURAL JOIN faelle)
WHERE f_bezeichnung =  'Fahrradunfall';

-------------------------------------------------------------------------------
-- 16. Aufgabe: 
-- Bei welchem Gericht gibt es keinen Termin?
-- Wir suchen alle Termine.

-- Projektion mit einer einfachen select-Anweisung aus zwei Tabellen, ohne erfüllte Eigenschaft
-- Schwierigkeitsgrad: schwer

SELECT g_name
FROM   gericht
GROUP BY g_name

-- die keine
EXCEPT

-- Eigenschaft besitzen.
SELECT g_name
FROM   gericht NATURAL JOIN verhandlung
GROUP BY g_name;

------------------------------------------------------------------------------
--17. Aufgabe: 
-- Wie viele Faelle bearbeitet Herr Rechtsbeuger?

-- Hier werden einfach die Einträge in Zuordnung gezählt
-- Schwierigkeitsgrad: schwer
SELECT   mi_nachname, COUNT(*) AS Anzahl
FROM     mitarbeiter NATURAL JOIN zuordnung
GROUP BY mi_nachname;	

------------------------------------------------------------------------------
--18. Aufgabe: 
-- Welcher durchschnittliche Betrag hat eine Rechnung?	

-- Berechnung eines Mittelwertes auf zwei Nachkommastellen
-- Schwierigkeitsgrad: schwer
SELECT COUNT (id_rechnung) AS anzahl_der_rechnungen, 
ROUND(AVG(r_rechnungsbetrag),2) AS durchschnittlicher_rechnungsbetrag
FROM	 rechnung NATURAL JOIN faelle;

------------------------------------------------------------------------------
-- 19. Aufgabe: 
-- Welches Fälle haben Mandanten aus Berlin?

-- Schnittmenge aller Mandanten mit dem Wohnort Berlin und der Fälle 
-- Schwierigkeitsgrad: schwer
SELECT id_fall, f_bezeichnung, ma_vorname, ma_nachname
FROM (faelle NATURAL JOIN mandanten)
WHERE ma_ort = 'Berlin';



------------------------------------------------------------------------------
--20. Aufgabe: 
--	Welche Fälle mit welchen Mandanten werden 2019 verhandelt?
--  Gib das Datum, das Aktenzeichen, die Bezeichnung der Fälle und Namen der Mandanten an!
--  Sortiere nach Datum!

-- Abfrage über mehrere Tabellen mit Datumsbereich
-- Schwierigkeitsgrad: mittel
SELECT v_datum, f_aktenzeichen, f_bezeichnung, ma_nachname
FROM (verhandlung NATURAL JOIN faelle NATURAL JOIN mandanten)
WHERE v_datum BETWEEN '2018-12-31' AND '2020-01-01'
ORDER BY v_datum;
