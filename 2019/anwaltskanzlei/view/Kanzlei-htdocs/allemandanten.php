<!DOCTYPE html>
<?php
//Datenbank includieren
include("conn-inc.php");
?>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="Kanzlei.css">
</head>
<body>

  
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.html">Advocat</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="../index.html">Home</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mandanten
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../mandantsuchen.php">Mandanten suchen</a></li>
		  <li><a href="../mandantenanlegen.php">Mandant anlegen</a></li>
		  <li><a href="../allemandanten.php">Mandanten</a></li>
        </ul>
      </li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Fälle
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../fallsuchen.php">Fall suchen</a></li>
		  <li><a href="../fallanlegen.php">Fall anlegen</a></li>
		  <li><a href="../allefaelle.php">Fälle</a></li>
        </ul>
	</li>
    <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gerichte
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allegerichte.php">Gerichte</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Verhandlungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleverhandlungen.php">Verhandlungen</a></li>
        </ul>
	</li>
	<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Prozessgegner
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleprozessgegner.php">Prozessgegner</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Rechnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allerechnungen.php">Rechnungen</a></li>
        </ul>
	</li>
	  <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Mitarbeiter
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allemitarbeiter.php">Mitarbeiter</a></li>
        </ul>
	</li>
		<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Zuordnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allezuordnungen.php">Zuordnungen</a></li>
        </ul>
	</li>
    </ul>
  </div>
</nav>
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left"> 
	<div class="page-header">
  <h2>Alle Mandanten</h2>
</div>
	<div class="tableDiv"> 
 <table class="table tableSmall">
    <thead>
      <tr>
		<th>id_mandant</th>
        <th>ma_nachname</th>
        <th>ma_vorname</th>
 		<th>ma_geburtsdatum</th>
		<th>ma_strasse</th>
		<th>ma_hausnummer</th>
		<th>ma_plz</th>
		<th>ma_ort</th>
		<th>ma_telefonnummer</th>
		<th>ma_email</th>
		<th>ma_geschlecht</th>
		<th>   </th>
      </tr>
    </thead>
	<tbody>
	
	<?php
	//Je nachdem ob und welche Parameter über den Link übergeben werden, wird ein anderer Select an die Datenbank geschickt.
	if((isset($_GET['ma_vorname'])) && (!empty($_GET['ma_vorname'])) && (empty($_GET['ma_nachname']))){ //vorname als parameter mitgegeben, daher sql mit vorname in Bedingung
		$sql = "SELECT * FROM mandanten where ma_vorname = '". $_GET['ma_vorname']."'";	
	}elseif((isset($_GET['ma_nachname'])) && (!empty($_GET['ma_nachname'])) && (empty($_GET['ma_vorname']))) {//nachname als parameter übergeben, daher sql mit nachname in bedingung
		$sql = "SELECT * FROM mandanten where ma_nachname = '". $_GET['ma_nachname']."'";
	}elseif((isset($_GET['ma_nachname'])) && (isset($_GET['ma_vorname']) && (!empty($_GET['ma_nachname'])) && (!empty($_GET['ma_vorname'])))) { //vor-und nachname als parameter übergeben, daher beide in der bedingung
		$sql = "SELECT * FROM mandanten where ma_vorname = '" . $_GET['ma_vorname'] . "' and ma_nachname = '" . $_GET['ma_nachname']."'";
	}else{//kein parameter mitgegeben
	$sql = "SELECT * FROM mandanten";						
	}
	//Datenbankanfrage abschicken und Ergebnis in result Variable schreiben
	$result = pg_query( $db, $sql );

	//Result iterieren
	if ($result) {
		
	//Inhalte aus Result in Tabellenform bringen
	while($row = pg_fetch_assoc($result)){
		echo "<tr>";
        echo "<td  class ='breakTd'>".$row["id_mandant"]."</td>";
        echo "<td  class ='breakTd'>".$row["ma_nachname"]."</td>";
        echo "<td class = 'breakTd'>".$row["ma_vorname"]."</td>";
		echo "<td class = 'breakTd'>".$row["ma_geburtsdatum"]."</td>";
		echo "<td class = 'breakTd'>".$row["ma_strasse"]."</td>";
		echo "<td class = 'breakTd'>".$row["ma_hausnummer"]."</td>";
		echo "<td class = 'breakTd'>".$row["ma_plz"]."</td>";
		echo "<td class = 'breakTd'>".$row["ma_ort"]."</td>";
		echo "<td class = 'breakTd'>".$row["ma_telefonnummer"]."</td>";
		echo "<td class = 'breakTd'>".$row["ma_email"]."</td>";
		echo "<td class = 'breakTd'>".$row["ma_geschlecht"]."</td>";
		echo "<td><a href='../mandant.php/?id_mandant=".$row['id_mandant']."' class='btn btn-info btn-xs' role='button'>zum Mandant</a></td>";
		echo "</tr>";
		
	}
		} else {
		echo "Es konnten keine Daten gefunden werden";
	}
	
	?>
	</tbody>
 
  </table>
  </div>
  </div>
    <div class="col-sm-2 sidenav">

      </div>
    </div>
  </div>
</div>


</body>
</html>
