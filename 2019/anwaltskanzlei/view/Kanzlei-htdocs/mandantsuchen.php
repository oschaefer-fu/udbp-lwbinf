<!DOCTYPE html>
<?php
//Datenbank includieren
include("conn-inc.php");
?>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="Kanzlei.css">
</head>
<body>

  
   <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.html">Advocat</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="../index.html">Home</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mandanten
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../mandantsuchen.php">Mandanten suchen</a></li>
		  <li><a href="../mandantenanlegen.php">Mandant anlegen</a></li>
		  <li><a href="../allemandanten.php">Mandanten</a></li>
        </ul>
      </li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Fälle
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../fallsuchen.php">Fall suchen</a></li>
		  <li><a href="../fallanlegen.php">Fall anlegen</a></li>
		  <li><a href="../allefaelle.php">Fälle</a></li>
        </ul>
	</li>
    <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gerichte
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allegerichte.php">Gerichte</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Verhandlungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleverhandlungen.php">Verhandlungen</a></li>
        </ul>
	</li>
	<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Prozessgegner
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleprozessgegner.php">Prozessgegner</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Rechnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allerechnungen.php">Rechnungen</a></li>
        </ul>
	</li>
	  <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Mitarbeiter
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allemitarbeiter.php">Mitarbeiter</a></li>
        </ul>
	</li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Zuordnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allezuordnungen.php">Zuordnungen</a></li>
        </ul>
	</li>
    </ul>
  </div>
</nav>
  <div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
	
    <div class="col-sm-8 text-left"> 
	<div class="page-header">
  <h2>Mandanten suchen</h2>
</div>
     <div class="form-group">
	<form name="" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" >
	
  <label for="MandantenNr">id_mandant (optional):</label>
  <input type="number" class="form-control" id="id_mandant" value="">
  
   <label for="MandantenNr">ma_vorname:</label>
  <input type="text" class="form-control" id="ma_vorname" value="">
  
   <label for="MandantenNr">ma_nachname:</label>
  <input type="text" class="form-control" id="ma_nachname" value="">


</form>


<br>

<button class="btn btn-info btn-sm marginRight" onclick="myFunction()">Suchen</button>



<a href="../allemandanten.php" class="btn btn-info btn-sm" role="button">Alle Mandanten anzeigen</a>

<script>
//wenn der Suchen Button betätigt wurde, wird geschaut welche Felder Inhalt haben. Abhängig davon, wird
//die Link-Parameterübergabe anders gestaltet und auf die Seite weitergeleitet. 
function myFunction() {
	
	if(!isNaN(document.getElementById('id_mandant').value) && document.getElementById('id_mandant').value != "" ){
	
window.location = "../mandant.php/?id_mandant="+document.getElementById('id_mandant').value;

	}else{
		window.location = "../allemandanten.php/?ma_vorname="+document.getElementById('ma_vorname').value+"&ma_nachname="+document.getElementById('ma_nachname').value;
	}
   
}

 </script>

<br>
</div>
    </div>
    <div class="col-sm-2 sidenav">

      </div>
    </div>
  </div>
</div>


</body>
</html>
