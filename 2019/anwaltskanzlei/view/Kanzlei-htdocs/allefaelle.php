<!DOCTYPE html>
<?php
//Datenbank includieren
include("conn-inc.php");
?>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="Kanzlei.css">
</head>
<body>

 
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.html">Advocat</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="../index.html">Home</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mandanten
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../mandantsuchen.php">Mandanten suchen</a></li>
		  <li><a href="../mandantenanlegen.php">Mandant anlegen</a></li>
		  <li><a href="../allemandanten.php">Mandanten</a></li>
        </ul>
      </li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Fälle
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../fallsuchen.php">Fall suchen</a></li>
		  <li><a href="../fallanlegen.php">Fall anlegen</a></li>
		  <li><a href="../allefaelle.php">Fälle</a></li>
        </ul>
	</li>
    <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gerichte
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allegerichte.php">Gerichte</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Verhandlungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleverhandlungen.php">Verhandlungen</a></li>
        </ul>
	</li>
	<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Prozessgegner
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleprozessgegner.php">Prozessgegner</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Rechnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allerechnungen.php">Rechnungen</a></li>
        </ul>
	</li>
	  <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Mitarbeiter
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allemitarbeiter.php">Mitarbeiter</a></li>
        </ul>
	</li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Zuordnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allezuordnungen.php">Zuordnungen</a></li>
        </ul>
	</li>
    </ul>
  </div>
</nav>
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left"> 
	<div class="page-header">
  <h2>Alle Fälle</h2>
</div>
	<div class="tableDiv"> 
 <table class="table tableSmall">
    <thead>
      <tr>
        <th>id_fall</th>
        <th>f_aktenzeichen</th>
        <th>f_bezeichnung</th>
		<th>id_mandant</th>
		<th>   </th>
      </tr>
    </thead>
  
	<tbody>
	
	<?php
	//wenn mandantennummer mitgegeben wird, werden alle fälle zugehörig zum mandanten angezeigt
	//ansonsten werden alle fälle aus der datenbank ausgegeben. 
	//die ergebnisse der datenbankanfrage werden iteriert und als html tabelle ausgegeben.
	if(isset($_GET['id_mandant'])){
	
	$sql = "SELECT * FROM faelle where id_mandant =" . $_GET['id_mandant'];						
	}else{
		$sql = "SELECT * FROM faelle";	
	}
			//Datenbankanfrage abschicken und Ergebnis in result Variable schreiben
			$result = pg_query( $db, $sql );

		
	if ($result) {
		
	while($row = pg_fetch_assoc($result)){
		echo "<tr>";
        echo "<td>".$row["id_fall"]."</td>";
        echo "<td>".$row["f_aktenzeichen"]."</td>";
		echo "<td>".$row["f_bezeichnung"]."</td>";
		echo "<td>".$row["id_mandant"]."</td>";
		echo "<td><a href='../fall.php/?id_fall=".$row['id_fall']."' class='btn btn-info btn-xs' role='button'>zum Fall</a></td>";
		echo "</tr>";
		
	}
		} else {
		echo "Es konnten keine Daten gefunden werden";
	}
	
	?>
	</tbody>
 
  </table>
  </div>
  </div>
    <div class="col-sm-2 sidenav">

      </div>
    </div>
  </div>
</div>


</body>
</html>
