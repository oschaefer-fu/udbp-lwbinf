<!DOCTYPE html>
<?php
//Datenbank includieren
include("conn-inc.php");
?>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="Kanzlei.css">
</head>
<body>

  
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.html">Advocat</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="../index.html">Home</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mandanten
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../mandantsuchen.php">Mandanten suchen</a></li>
		  <li><a href="../mandantenanlegen.php">Mandant anlegen</a></li>
		  <li><a href="../allemandanten.php">Mandanten</a></li>
        </ul>
      </li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Fälle
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../fallsuchen.php">Fall suchen</a></li>
		  <li><a href="../fallanlegen.php">Fall anlegen</a></li>
		  <li><a href="../allefaelle.php">Fälle</a></li>
        </ul>
	</li>
    <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gerichte
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allegerichte.php">Gerichte</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Verhandlungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleverhandlungen.php">Verhandlungen</a></li>
        </ul>
	</li>
	<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Prozessgegner
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleprozessgegner.php">Prozessgegner</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Rechnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allerechnungen.php">Rechnungen</a></li>
        </ul>
	</li>
	  <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Mitarbeiter
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allemitarbeiter.php">Mitarbeiter</a></li>
        </ul>
	</li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Zuordnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allezuordnungen.php">Zuordnungen</a></li>
        </ul>
	</li>
    </ul>
  </div>
</nav>
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left">
<div class="page-header">
  <h2>Mandanten bearbeiten</h2>
</div>	
     <form name="" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" >

	  
	  <?php 
	  //Wenn eine mandantenummer mitgegeben wurde, werden die Daten des Mandanten abgefragt und ausgegeben.
	  $sql = "SELECT * FROM mandanten WHERE id_mandant = ".$_GET['id_mandant'];
	  $result = pg_query( $db, $sql );
	  
	  	if ($result) {
		
	while($row = pg_fetch_assoc($result)){

echo " <div class='form-group'>";
  echo "<label for='id_mandant'>id_mandant:</label>";
  echo "<input type='text' class='form-control' name='id_mandant' value=".$row["id_mandant"]." readonly>";
	echo "</div>";
echo " <div class='form-group'>";
  echo "<label for='ma_nachname'>ma_nachname:</label>";
  echo "<input type='text' class='form-control' name='ma_nachname' value=".$row["ma_nachname"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_vorname'>ma_vorname:</label>";
  echo "<input type='text' class='form-control' name='ma_vorname' value=".$row["ma_vorname"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_geburtsdatum'>ma_geburtsdatum:</label>";
  echo "<input type='text' class='form-control' name='ma_geburtsdatum' value=".$row["ma_geburtsdatum"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_strasse'>ma_strasse:</label>";
  echo "<input type='text' class='form-control' name='ma_strasse' value=".$row["ma_strasse"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_hausnummer'>ma_hausnummer:</label>";
  echo "<input type='text' class='form-control' name='ma_hausnummer' value=".$row["ma_hausnummer"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_plz'>ma_plz:</label>";
  echo "<input type='text' class='form-control' name='ma_plz' value=".$row["ma_plz"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_ort'>ma_ort:</label>";
  echo "<input type='text' class='form-control' name='ma_ort' value=".$row["ma_ort"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_telefonnummer'>ma_telefonnummer:</label>";
  echo "<input type='text' class='form-control' name='ma_telefonnummer' value=".$row["ma_telefonnummer"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_email'>ma_email:</label>";
  echo "<input type='text' class='form-control' name='ma_email' value=".$row["ma_email"].">";
	echo "</div>";
	echo " <div class='form-group'>";
  echo "<label for='ma_geschlecht'>ma_geschlecht:</label>";
  echo "<input type='text' class='form-control' name='ma_geschlecht' value=".$row["ma_geschlecht"].">";
	echo "</div>";
		
	
	}
		}
  ?>


 

 
<input type="Submit" class="btn btn-info btn-sm" value="Speichern" name="submit">
<input type="Submit" class="btn btn-danger btn-sm" value="Löschen" name="delete">
</form>

<?php
if (isset($_POST['submit'])) {//wenn der Speichern-Button gedrückt wird, wird der Altdatenbestand in der Datenbank durch die neuen Daten ersetzt
										
		
											$sql = " UPDATE mandanten ";
											$sql .= " SET ";
											$sql .= " id_mandant   ='". $_POST['id_mandant'] ."', ";
											$sql .= " ma_nachname   ='". $_POST['ma_nachname'] ."', ";
											$sql .= " ma_vorname   ='". $_POST['ma_vorname'] ."', ";
											$sql .= " ma_geburtsdatum   ='". $_POST['ma_geburtsdatum'] ."', ";
											$sql .= " ma_strasse   ='". $_POST['ma_strasse'] ."', ";
											$sql .= " ma_hausnummer   ='". $_POST['ma_hausnummer'] ."', ";
											$sql .= " ma_plz   ='". $_POST['ma_plz'] ."', ";
											$sql .= " ma_ort  ='". $_POST['ma_ort'] ."', ";
											$sql .= " ma_telefonnummer   ='". $_POST['ma_telefonnummer'] ."', ";
											$sql .= " ma_email   ='". $_POST['ma_email'] ."', ";
											$sql .= " ma_geschlecht  ='". $_POST['ma_geschlecht'] ."' ";
											
											$sql .= " WHERE id_mandant = ". $_POST['id_mandant'] ."";
										
											
											$db_erg = pg_query( $db, $sql );
											
										
											if(!$db_erg){
												die('Ungültige Abfrage: ' . pg_error()); 
											}
											
											header("location: ../allemandanten.php");
											
									}

?>

<?php
if (isset($_POST['delete'])) { //wenn der Löschen-Button gedrückt wird, werden alle Daten mit der entsprechenden Mandantennummer gelöscht
										
		
											$sql = " DELETE ";
											$sql .= " FROM mandanten ";
											$sql .= " WHERE id_mandant = ". $_POST['id_mandant'];
											
											
											
											$db_erg = pg_query( $db, $sql );
											
										
											if(!$db_erg){
												die('Ungültige Abfrage: ' . pg_error()); 
											}
											
											header("location: ../allemandanten.php");
											
									}

?>

    </div>
    <div class="col-sm-2 sidenav">

      </div>
    </div>
  </div>
</div>


</body>
</html>
