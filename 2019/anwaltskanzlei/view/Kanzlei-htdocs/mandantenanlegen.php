<!DOCTYPE html>
<?php
//Datenbank includieren
include("conn-inc.php");
?>
	

<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="Kanzlei.css">
</head>
<body>

 
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.html">Advocat</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="../index.html">Home</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mandanten
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../mandantsuchen.php">Mandanten suchen</a></li>
		  <li><a href="../mandantenanlegen.php">Mandant anlegen</a></li>
		  <li><a href="../allemandanten.php">Mandanten</a></li>
        </ul>
      </li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Fälle
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="../fallsuchen.php">Fall suchen</a></li>
		  <li><a href="../fallanlegen.php">Fall anlegen</a></li>
		  <li><a href="../allefaelle.php">Fälle</a></li>
        </ul>
	</li>
    <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gerichte
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allegerichte.php">Gerichte</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Verhandlungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleverhandlungen.php">Verhandlungen</a></li>
        </ul>
	</li>
	<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Prozessgegner
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../alleprozessgegner.php">Prozessgegner</a></li>
        </ul>
	</li>
      <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Rechnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allerechnungen.php">Rechnungen</a></li>
        </ul>
	</li>
	  <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Mitarbeiter
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allemitarbeiter.php">Mitarbeiter</a></li>
        </ul>
	</li>
			<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Zuordnungen
	  <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="../allezuordnungen.php">Zuordnungen</a></li>
        </ul>
	</li>
    </ul>
  </div>
</nav>
  <div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left">
	<div class="page-header">
  <h2>Mandanten anlegen</h2>
</div>
     <div class="form-group">
     <form name="" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" >
<div class="form-group">
  <label for="Vorname">id_mandant:</label>
  <input type="text" class="form-control" name="id_mandant">
</div>
<div class="form-group">
  <label for="Vorname">ma_nachname:</label>
  <input type="text" class="form-control" name="ma_nachname">
</div>
 <div class="form-group">
  <label for="Name">ma_vorname:</label>
  <input type="text" class="form-control" name="ma_vorname">
</div>
<div class="form-group">
  <label for="Strasse">ma_geburtsdatum</label>
  <input type="date" class="form-control" name="ma_geburtsdatum">
</div>
      <div class="form-group">
  <label for="Hausnummer">ma_strasse:</label>
  <input type="text" class="form-control" name="ma_strasse">
</div>
      <div class="form-group">
  <label for="PLZ">ma_hausnummer:</label>
  <input type="text" class="form-control" name="ma_hausnummer">
</div>
      <div class="form-group">
  <label for="Ort">ma_plz:</label>
  <input type="text" class="form-control" name="ma_plz">
</div>
      <div class="form-group">
  <label for="Telefon">ma_ort:</label>
  <input type="text" class="form-control" name="ma_ort">
</div>
      <div class="form-group">
  <label for="Geburtstag">ma_telefonnummer:</label>
  <input type="text" class="form-control" name="ma_telefonnummer">
</div>
      <div class="form-group">
  <label for="Geburtstag">ma_email:</label>
  <input type="text" class="form-control" name="ma_email">
</div>
      <div class="form-group">
  <label for="Geschlecht">ma_geschlecht:</label>
  <select class="form-control" name="ma_geschlecht">
    <option>w</option>
    <option>m</option>
    <option>d</option>
  </select>
</div>

</div>
<input type="Submit" class="btn btn-info" value="Anlegen" name="submit">
</form>
<?php
//Wenn der anlegen Button betätigt wurde, wird die Sql Anweisung mit den Inhalten des Formulars zususammengebaut. Anschließend wird das Sql abgeschickt.
if (isset($_POST['submit'])) {
										
		
											$sql = " INSERT INTO mandanten";
											$sql .= " (id_mandant, ma_nachname, ma_vorname, ma_geburtsdatum, ma_strasse, ma_hausnummer, ma_plz, ma_ort, ma_telefonnummer, ma_email, ma_geschlecht) VALUES ";
											$sql .= "(". $_POST['id_mandant'] .", ";
											$sql .= "'". $_POST['ma_nachname'] ."', ";
											$sql .= "'". $_POST['ma_vorname'] ."', ";
											$sql .= "'". $_POST['ma_geburtsdatum'] ."', ";
											$sql .= "'". $_POST['ma_strasse'] ."', ";
											$sql .= "'". $_POST['ma_hausnummer'] ."', ";
											$sql .= "'". $_POST['ma_plz'] ."', ";
											$sql .= "'". $_POST['ma_ort'] ."', ";
											$sql .= "'". $_POST['ma_telefonnummer'] ."', ";
											$sql .= "'". $_POST['ma_email'] ."', ";
											$sql .= "'". $_POST['ma_geschlecht'] ."');";

											$db_erg = pg_query( $db, $sql );

									}

?>

    </div>
    <div class="col-sm-2 sidenav">

      </div>
    </div>
  </div>
</div>


</body>
</html>
