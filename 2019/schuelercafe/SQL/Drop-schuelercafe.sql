--Miniwelt Schuelercafe
--erstellt von T.Gerl, A.Jahn, M.Stark, J.Weiss

--Personen
DROP TABLE Stammkunden      CASCADE;
DROP TABLE Schueler       	CASCADE;
DROP TABLE Lehrer          	CASCADE;

--Verkauf und Arbeit
DROP TABLE Verkauf   		CASCADE;
DROP TABLE Arbeit       	CASCADE;
DROP TABLE Arbeitszeiten  	CASCADE;

--Produkte
DROP TABLE Produkte 		CASCADE;
DROP TABLE Snacks       	CASCADE;
DROP TABLE Getraenke        CASCADE;

--Lieferanten
DROP TABLE Lieferanten		CASCADE;
DROP TABLE Lieferung       	CASCADE;
