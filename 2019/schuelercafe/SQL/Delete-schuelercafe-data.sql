--Miniwelt Schuelercafe
--erstellt von T.Gerl, A.Jahn, M.Stark, J.Weiss

--Verkauf und Arbeit-----------------------------------------------------------

--Verkauf
DELETE FROM Verkauf;

--Arbeit
DELETE FROM Arbeit;

--Arbeitszeiten
DELETE FROM Arbeitszeiten;

--Lieferanten und Lieferung------------------------------------------------------

--Lieferung
DELETE FROM Lieferung;

--Lieferanten
DELETE FROM Lieferanten;

--Produktangebot--------------------------------------------------------------

--Snacks
DELETE FROM Snacks;

--Getränke
DELETE FROM Getraenke;

--Produkte
DELETE FROM Produkte;

--Personen---------------------------------------------------------------------

--Schüler
DELETE FROM Schueler;

--Lehrer
DELETE FROM Lehrer;

--Stammkunden
DELETE FROM Stammkunden;
