--SQL-Abfragen-------------------------------------

--Aufgabenstellung
--Ermitteln Sie SQL-Abfragen, die folgende Ergebnisse zeigen sollen.
--Angaben, die in Klammern stehen, sind zusätzliche Anforderungen, die die Abfrage erfüllen soll und können in einem zweiten Arbeitsschritt eingearbeitet werden.

--I. Schwierigkeitsstufe:
-- - nur einzelne Tabellen zum Kennenlernen der Miniwelt Schülercafe

--1. Alle Attribute der Stammkunden
SELECT 	*
FROM  	Stammkunden;

--2. Die Namen und Vornamen aller Stammkunden, die im Datenbanksystem gespeichert sind
SELECT 		Vorname, Name
FROM 		Stammkunden;

--3. Eine Angabe, wie viele Stammkunden in der Datenbank gespeichert sind
SELECT		Count(*)
FROM 		Stammkunden; 

--4. Die Anzahl der Lehrer, die in der Datenbank gespeichert sind (benannt mit lehreranzahl)
SELECT 		Count(*)	AS lehrerAnzahl
FROM 		Lehrer;

--5. Eine Liste der drei ältesten Stammkunden (Vorname, Name, Geburtsdatum), die in der Datenbank gespeichert sind
SELECT		Vorname, Name, GebDatum
FROM		Stammkunden
ORDER BY 	GebDatum
LIMIT 3; 

--6.a Eine Liste der Produkte (Bezeichnung), die im Schülercafe angeboten werden, mit Verkaufspreis
SELECT 		Bezeichnung, Verkaufspreis
FROM 		Produkte;
  
--6.b Eine Liste der Produkte (Bezeichnung), die im Schülercafe angeboten werden, mit Verkaufspreis und aphabetisch nach Bezeichnung sortiert
SELECT 		Bezeichnung, Verkaufspreis
FROM 		Produkte
ORDER BY 	Bezeichnung;

--6.c Eine Liste der Produkte (Bezeichnung), die im Schülercafe angeboten werden, mit Verkaufspreis und nach Verkaufspreis aufsteigend sortiert
SELECT 		Bezeichnung, Verkaufspreis
FROM 		Produkte
ORDER BY 	Verkaufspreis;

--6.d Eine (alphabetisch sortierte) Liste der drei Produkte, die den höchsten Verkaufspreis haben
SELECT 		Bezeichnung, Verkaufspreis
FROM 		Produkte
ORDER BY 	Verkaufspreis DESC, Bezeichnung
LIMIT 3;

--7.a Eine Liste der Produkte (Bezeichnung), die Gluten enthalten
SELECT 		Bezeichnung, Gluten
FROM 		Produkte
WHERE		Gluten = True;

--7.b Eine Liste der Produkte (Bezeichnung), die alle drei Allergene Lactose, Gluten und Nüsse enthalten
SELECT 		Bezeichnung, Gluten, Lactose, Nuesse
FROM 		Produkte
WHERE		Gluten = True AND Lactose = TRUE AND Nuesse = TRUE;

--7.c Eine Liste der Stammkunden (Name, Vorname), die sich bereits wieder abgemeldet haben
SELECT 		Name, Vorname
FROM		Stammkunden
WHERE		Abmeldedatum NOTNULL;

--7.d Eine (alphabetisch sortierte) Liste der Stammkunden (Name, Vorname), die sich am 05.08.2019 im System registriert haben
SELECT 		Name, Vorname
FROM		Stammkunden
WHERE		Registrierungsdatum = '2019-08-05'
ORDER BY	Name;

--7.e Eine Liste von Produkt-IDS von Getränken, die mit Pfand verkauft werden
SELECT 		PID
FROM		Getraenke
WHERE		Pfand > 0; --oder NOTNULL

--7.f Die Anzahl der gelieferten Produkte am 05.08.2019
SELECT		SUM(LAnzahl)
FROM		Lieferung
WHERE		LDatum = '2019-08-05';

--II. Schwierigkeitsstufe:
--  - eine Tabelle reicht nun nicht mehr aus

--8.a Eine Liste von Produkten (PID, Bezeichnung) der Getränke, die mit Pfand verkauft werden
SELECT 		PID, Bezeichnung
FROM		Getraenke
NATURAL JOIN Produkte
WHERE		Pfand > 0;

--8.b Eine (alphabetisch sortierte) Liste der Snacks (nur Bezeichnung), die im Schülercafe verkauft werden
SELECT 		Bezeichnung
FROM		Snacks
NATURAL JOIN Produkte
ORDER BY 	Bezeichnung;

--8.c Eine Liste (Name, Vorname) der Schüler, die in der Datenbank gespeichert sind
SELECT		Name, Vorname
FROM		Stammkunden
NATURAL JOIN Schueler;

--8.d Eine Liste der Produkte (Bezeichnung), die am 05.08.2019 verkauft wurden
SELECT 		Bezeichnung
FROM		Produkte
NATURAL JOIN Verkauf
WHERE		VDatum = '2019-08-05';	

--9.a Eine Liste der Schüler (Name, Vorname), die im Schülercafe verkauft haben
SELECT		A.Name, A.Vorname
FROM 		Stammkunden A ,	Verkauf B
WHERE 		A.StID = B.VID;

--9.b Die Anzahl der Schüler, die im Schülercafe verkauft haben (benannt mit verkaeufer)
SELECT		COUNT(*) AS verkaeufer
FROM 		Stammkunden A ,	Verkauf B
WHERE 		A.StID = B.VID;

--9.c Eine Liste der Schüler (Name, Vorname), die im Schülercafe ein Getränk gekauft haben
SELECT		A.Name, A.Vorname
FROM 		Stammkunden A ,	Verkauf B, Getraenke G
WHERE 		A.StID = B.KID AND B.PID = G.PID;

--9.d Eine Liste der Schüler (Name, Vorname), die im Schülercafe ein Getränk gekauft haben mit Angabe des Getränkenamens
SELECT		A.Name, A.Vorname, P.Bezeichnung
FROM 		Stammkunden A ,	Verkauf B, Getraenke G, Produkte P
WHERE 		A.StID = B.KID AND B.PID = G.PID AND B.PID = P.PID;

--9.e Eine Liste der Verkäufer (Name, Vorname), an der ablesbar ist, wieviele Pausen er/sie gearbeitet hat
SELECT 		Name, Vorname, COUNT(*)
FROM		Stammkunden
NATURAL JOIN ARBEIT
GROUP BY 	Name, Vorname;

--9.f Der Verkäufer (Name, Vorname), der die meisten Pausen gearbeitet hat
SELECT 		Name, Vorname, COUNT(*) AS Pausen
FROM		Stammkunden
NATURAL JOIN ARBEIT
GROUP BY 	Name, Vorname
ORDER BY 	Pausen DESC
LIMIT 1;

--III. Schwierigkeitsstufe:
-- - mit having, Aggregatfunktionen und Unterabfragen

--10.a Eine Liste der Kunden (Name, Vorname) mit der Anzahl der Produkte, die sie gekauft haben
SELECT		S.Name, S.Vorname, Sum(V.VAnzahl) AS Anzahl
FROM		Stammkunden S, Verkauf V
WHERE		S.StID = V.KID
GROUP BY	S.Name, S.Vorname;

--10.b Eine Liste der Kunden (Name, Vorname), die mindestens 2 Produkte gekauft haben
SELECT		S.Name, S.Vorname, Sum(V.VAnzahl) AS Anzahl
FROM		Stammkunden S, Verkauf V
WHERE		S.StID = V.KID
GROUP BY	S.Name, S.Vorname
HAVING 		Sum(VAnzahl) >= 2;

--11.a Der Umsatz, der bisher durch das Schülercafe erreicht wurde
SELECT		SUM(Verkaufspreis * VAnzahl) AS Umsatz  --VAnzahl unbedingt beachten!!
FROM		Produkte
NATURAL JOIN Verkauf;


--11.b Eine Liste der Produkte mit Bezeichnung, Verkaufspreis, Lieferpreis und Gewinn (Verkaufspreis minus Lieferpreis)
SELECT		Bezeichnung, Verkaufspreis, Lieferpreis, Verkaufspreis - Lieferpreis AS Gewinn
FROM		Produkte
NATURAL JOIN Lieferung;

--11.c Der Gewinn, der mit den bisherigen Verkäufen erreicht wurde
SELECT SUM(A.Gewinn * VAnzahl) AS Gesamtgewinn
FROM ( SELECT	PID, Verkaufspreis - Lieferpreis AS Gewinn
		FROM	Produkte
		NATURAL JOIN Lieferung) AS A
NATURAL JOIN	Verkauf;
