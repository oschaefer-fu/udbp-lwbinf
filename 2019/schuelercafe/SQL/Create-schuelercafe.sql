--Miniwelt Schuelercafe
--erstellt von T.Gerl, A.Jahn, M.Stark, J.Weiss


--Personen
CREATE TABLE Stammkunden (
	StID					INTEGER   		NOT NULL	PRIMARY KEY, 
	Name					VARCHAR(100)	NOT NULL, --Name des Stammkunden max. 100 Zeichen
	Vorname					VARCHAR(100)	NOT NULL, --Vorname des Stammkunden max. 100 Zeichen
	GebDatum				DATE			NOT NULL,
	Registrierungsdatum		DATE			NOT NULL,
	Abmeldedatum			DATE			NULL
) ;
	
CREATE TABLE Schueler (
	StID					INTEGER			NOT NULL 	REFERENCES Stammkunden, --Verweis auf die Stammkunden-ID
													  -- Tupel kann nur eingefügt werden, wenn die StID bereits bei den Stammkunden vorhanden ist
	TelNr					VARCHAR(12)		NOT NULL, --Angabe der Telefonnummer als String, da als Integer die Zahl nicht mehr im gültigen Bereich liegen würde
	Klasse					VARCHAR(3)		NOT NULL
) ;

CREATE TABLE Lehrer	(
	StID					INTEGER			NOT NULL	REFERENCES Stammkunden, --Verweis auf die Stammkunden-ID
	Klassenlehrer			VARCHAR(3)		NULL      --Angabe der Klasse
) ;


--Produktangebot
CREATE TABLE Produkte (
	PID						INTEGER			NOT NULL	PRIMARY KEY, --ProduktID
	Bezeichnung				VARCHAR(250)	NOT NULL, --Produktbezeichnung
	Verkaufspreis			NUMERIC(7,2)	NOT NULL, --Verkaufspreis des Produkts
	Lactose					BOOLEAN,			--True gibt an, dass das Allergen vorhanden ist
	Gluten					BOOLEAN,			--True gibt an, dass das Allergen vorhanden ist
	Nuesse					BOOLEAN				--True gibt an, dass das Allergen vorhanden ist
) ;

CREATE TABLE Snacks	(
	PID						INTEGER			NOT NULL	REFERENCES Produkte, --Verweis auf die Produkt-ID
	Zubereitungszeit		INTEGER			NULL
) ;

CREATE TABLE Getraenke (
	PID						INTEGER			NOT NULL	REFERENCES Produkte, --Verweis auf die Produkt-ID
	Pfand					NUMERIC(3,2)	NULL, --Kosten Pfand mit einer Vorstelle und zwei Nachkommerstellen	
	Koffeinhaltig			BOOLEAN			  	  --Koffeinhaltig true/false
) ;


--Verkauf und Arbeit
CREATE TABLE Verkauf (
	KID						INTEGER			NOT NULL	REFERENCES Stammkunden(StID), --Verweis auf die Stammkunden-ID -> Käufer-ID
	PID						INTEGER			NOT NULL    REFERENCES Produkte,          --Verweis auf die Produkt-ID
	VID						INTEGER			NOT NULL  	REFERENCES Stammkunden(StID)  --Verweis auf die Stammkunden-ID -> Verkäufer-ID
														CHECK(VID <> KID),
											-- Integritätsbedingung: Der Verkäufer darf nicht an sich selbst ein Produkt verkaufen.
											-- Daher muss die VerkaufsID ungleich der KaufID sein.
	VUhrzeit				TIME			NOT NULL, --Uhrzeit des Verkaufes
	VDatum					DATE			NOT NULL, --Datum, an dem das Produkt verkauft wurde
	VAnzahl					INTEGER			CHECK(VAnzahl> 0) --Anzahl der verkauften Produkte
) ;

CREATE TABLE Arbeitszeiten	(
	AID						INTEGER			NOT NULL	PRIMARY KEY,			
	UhrzeitVonBis			VARCHAR(11)		NOT NULL	--UhrzeitVonBis der Arbeitszeit als String max. 11 Zeichen
) ;
	
CREATE TABLE Arbeit	(
	StID					INTEGER			NOT NULL 	REFERENCES Stammkunden,   --Verweis auf die Stammkunden-ID
	AID						INTEGER			NOT NULL	REFERENCES Arbeitszeiten, --Verweis auf die AID
	ADatum					DATE			NOT NULL
) ;
	

--Lieferanten	
CREATE TABLE Lieferanten (
	LID						INTEGER			NOT NULL	PRIMARY KEY,
	LName					VARCHAR(200)	NOT NULL    --Firmenname max. 200 Zeichen
) ;

CREATE TABLE Lieferung (
	PID						INTEGER			NOT NULL	REFERENCES Produkte,    --Verweis auf die Produkt-ID
	LID						INTEGER			NOT NULL	REFERENCES Lieferanten, --Verweis auf die Lieferanten-ID
	LAnzahl					INTEGER			NOT NULL, --Anzahl der gelieferten Produkte
	LDatum					DATE			NOT NULL, --Datum der Lieferung
	Lieferpreis				NUMERIC(7,2)	NOT NULL --Lieferpreis des Produkts
) ;
