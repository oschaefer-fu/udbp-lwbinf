--SQL-Abfragen-------------------------------------

--Aufgabenstellung
--Ermitteln Sie SQL-Abfragen, die folgende Ergebnisse zeigen sollen.
--Angaben, die in Klammern stehen, sind zusätzliche Anforderungen, die die Abfrage erfüllen soll und können in einem zweiten Arbeitsschritt eingearbeitet werden.

--I. Schwierigkeitsstufe:
-- - nur einzelne Tabellen zum Kennenlernen der Miniwelt Schülercafe

--1. Alle Attribute der Stammkunden
SELECT 	*
FROM  	Stammkunden;

 stid |   name   | vorname  |  gebdatum  | registrierungsdatum | abmeldedatum 
------+----------+----------+------------+---------------------+--------------
    1 | Meier    | Lisa     | 2003-02-28 | 2019-08-05          | 
    2 | Petersen | Richard  | 1980-08-20 | 2019-08-05          | 
    3 | Andante  | Alfredo  | 2007-05-03 | 2019-08-05          | 2019-08-07
    4 | Yilmaz   | Fatma    | 2005-12-31 | 2019-08-05          | 
    5 | Glück    | Johannes | 2004-05-16 | 2019-08-05          | 
    6 | Dickens  | Charlie  | 2006-11-21 | 2019-08-06          | 
    7 | Clarks   | Suzanne  | 2005-03-14 | 2019-08-06          | 
    8 | Meyer    | Sophie   | 2007-10-27 | 2019-08-06          | 
    9 | Bordeaux | Marie    | 2007-08-16 | 2019-08-06          | 2019-12-11
   10 | Müller   | Justin   | 2006-12-24 | 2019-08-06          | 
   11 | Hut      | Barbara  | 1975-02-28 | 2019-08-06          | 
   12 | Puchert  | Jürgen   | 1972-06-08 | 2019-08-06          | 
(12 Zeilen)

--2. Die Namen und Vornamen aller Stammkunden, die im Datenbanksystem gespeichert sind
SELECT 		Vorname, Name
FROM 		Stammkunden;

 vorname  |   name   
----------+----------
 Lisa     | Meier
 Richard  | Petersen
 Alfredo  | Andante
 Fatma    | Yilmaz
 Johannes | Glück
 Charlie  | Dickens
 Suzanne  | Clarks
 Sophie   | Meyer
 Marie    | Bordeaux
 Justin   | Müller
 Barbara  | Hut
 Jürgen   | Puchert
(12 Zeilen)

--3. Eine Angabe, wie viele Stammkunden in der Datenbank gespeichert sind
SELECT		Count(*)
FROM 		Stammkunden;

 count 
-------
    12
(1 Zeile) 

--4. Die Anzahl der Lehrer, die in der Datenbank gespeichert sind (benannt mit lehreranzahl)
SELECT 		Count(*)	AS lehrerAnzahl
FROM 		Lehrer;

 lehreranzahl 
--------------
            3
(1 Zeile)

--5. Eine Liste der drei ältesten Stammkunden (Vorname, Name, Geburtsdatum), die in der Datenbank gespeichert sind
SELECT		Vorname, Name, GebDatum
FROM		Stammkunden
ORDER BY 	GebDatum
LIMIT 3;

 vorname |   name   |  gebdatum  
---------+----------+------------
 Jürgen  | Puchert  | 1972-06-08
 Barbara | Hut      | 1975-02-28
 Richard | Petersen | 1980-08-20
(3 Zeilen)

--6.a Eine Liste der Produkte (Bezeichnung), die im Schülercafe angeboten werden, mit Verkaufspreis
SELECT 		Bezeichnung, Verkaufspreis
FROM 		Produkte;

    bezeichnung    | verkaufspreis 
-------------------+---------------
 Snickers          |          1.20
 Milkyway          |          1.20
 Apfelschorle      |          1.50
 Cola              |          1.70
 Kaffee            |          1.00
 Belegtes Brötchen |          2.00
 Banane            |          0.50
 Apfel             |          0.50
 Couscous-Salat    |          1.50
 Gemüsechips       |          2.00
 Kakao             |          1.00
 Käse-Sandwich     |          2.00
(12 Zeilen)
  
--6.b Eine Liste der Produkte (Bezeichnung), die im Schülercafe angeboten werden, mit Verkaufspreis und aphabetisch nach Bezeichnung sortiert
SELECT 		Bezeichnung, Verkaufspreis
FROM 		Produkte
ORDER BY 	Bezeichnung;

    bezeichnung    | verkaufspreis 
-------------------+---------------
 Apfel             |          0.50
 Apfelschorle      |          1.50
 Banane            |          0.50
 Belegtes Brötchen |          2.00
 Cola              |          1.70
 Couscous-Salat    |          1.50
 Gemüsechips       |          2.00
 Kaffee            |          1.00
 Kakao             |          1.00
 Käse-Sandwich     |          2.00
 Milkyway          |          1.20
 Snickers          |          1.20
(12 Zeilen)

--6.c Eine Liste der Produkte (Bezeichnung), die im Schülercafe angeboten werden, mit Verkaufspreis und nach Verkaufspreis aufsteigend sortiert
SELECT 		Bezeichnung, Verkaufspreis
FROM 		Produkte
ORDER BY 	Verkaufspreis;

    bezeichnung    | verkaufspreis 
-------------------+---------------
 Banane            |          0.50
 Apfel             |          0.50
 Kaffee            |          1.00
 Kakao             |          1.00
 Snickers          |          1.20
 Milkyway          |          1.20
 Couscous-Salat    |          1.50
 Apfelschorle      |          1.50
 Cola              |          1.70
 Belegtes Brötchen |          2.00
 Gemüsechips       |          2.00
 Käse-Sandwich     |          2.00
(12 Zeilen)

--6.d Eine (alphabetisch sortierte) Liste der drei Produkte, die den höchsten Verkaufspreis haben
SELECT 		Bezeichnung, Verkaufspreis
FROM 		Produkte
ORDER BY 	Verkaufspreis DESC, Bezeichnung
LIMIT 3;

    bezeichnung    | verkaufspreis 
-------------------+---------------
 Belegtes Brötchen |          2.00
 Gemüsechips       |          2.00
 Käse-Sandwich     |          2.00
(3 Zeilen)

--7.a Eine Liste der Produkte (Bezeichnung), die Gluten enthalten
SELECT 		Bezeichnung, Gluten
FROM 		Produkte
WHERE		Gluten = True;

    bezeichnung    | gluten 
-------------------+--------
 Belegtes Brötchen | t
 Couscous-Salat    | t
 Käse-Sandwich     | t
(3 Zeilen)

--7.b Eine Liste der Produkte (Bezeichnung), die alle drei Allergene Lactose, Gluten und Nüsse enthalten
SELECT 		Bezeichnung, Gluten, Lactose, Nuesse
FROM 		Produkte
WHERE		Gluten = True AND Lactose = TRUE AND Nuesse = TRUE;

    bezeichnung    | gluten | lactose | nuesse 
-------------------+--------+---------+--------
 Belegtes Brötchen | t      | t       | t
(1 Zeile)

--7.c Eine Liste der Stammkunden (Name, Vorname), die sich bereits wieder abgemeldet haben
SELECT 		Name, Vorname
FROM		Stammkunden
WHERE		Abmeldedatum NOTNULL;

   name   | vorname 
----------+---------
 Andante  | Alfredo
 Bordeaux | Marie
(2 Zeilen)

--7.d Eine (alphabetisch sortierte) Liste der Stammkunden (Name, Vorname), die sich am 05.08.2019 im System registriert haben
SELECT 		Name, Vorname
FROM		Stammkunden
WHERE		Registrierungsdatum = '2019-08-05'
ORDER BY	Name;

   name   | vorname  
----------+----------
 Andante  | Alfredo
 Glück    | Johannes
 Meier    | Lisa
 Petersen | Richard
 Yilmaz   | Fatma
(5 Zeilen)

--7.e Eine Liste von Produkt-IDS von Getränken, die mit Pfand verkauft werden
SELECT 		PID
FROM		Getraenke
WHERE		Pfand > 0; --oder NOTNULL

 pid 
-----
   3
   4
(2 Zeilen)

--7.f Die Anzahl der gelieferten Produkte am 05.08.2019
SELECT		SUM(LAnzahl)
FROM		Lieferung
WHERE		LDatum = '2019-08-05';

 sum 
-----
 400
(1 Zeile)

--II. Schwierigkeitsstufe:
--  - eine Tabelle reicht nun nicht mehr aus

--8.a Eine Liste von Produkten (PID, Bezeichnung) der Getränke, die mit Pfand verkauft werden
SELECT 		PID, Bezeichnung
FROM		Getraenke
NATURAL JOIN Produkte
WHERE		Pfand > 0;

 pid | bezeichnung  
-----+--------------
   3 | Apfelschorle
   4 | Cola
(2 Zeilen)

--8.b Eine (alphabetisch sortierte) Liste der Snacks (nur Bezeichnung), die im Schülercafe verkauft werden
SELECT 		Bezeichnung
FROM		Snacks
NATURAL JOIN Produkte
ORDER BY 	Bezeichnung;

    bezeichnung    
-------------------
 Apfel
 Banane
 Belegtes Brötchen
 Couscous-Salat
 Käse-Sandwich
 Milkyway
 Snickers
(7 Zeilen)

--8.c Eine Liste (Name, Vorname) der Schüler, die in der Datenbank gespeichert sind
SELECT		Name, Vorname
FROM		Stammkunden
NATURAL JOIN Schueler;

   name   | vorname  
----------+----------
 Meier    | Lisa
 Andante  | Alfredo
 Yilmaz   | Fatma
 Glück    | Johannes
 Dickens  | Charlie
 Clarks   | Suzanne
 Meyer    | Sophie
 Bordeaux | Marie
 Müller   | Justin
(9 Zeilen)

--8.d Eine Liste der Produkte (Bezeichnung), die am 05.08.2019 verkauft wurden
SELECT 		Bezeichnung
FROM		Produkte
NATURAL JOIN Verkauf
WHERE		VDatum = '2019-08-05';	

    bezeichnung    
-------------------
 Snickers
 Belegtes Brötchen
(2 Zeilen)

--9.a Eine Liste der Schüler (Name, Vorname), die im Schülercafe verkauft haben
SELECT		A.Name, A.Vorname
FROM 		Stammkunden A ,	Verkauf B
WHERE 		A.StID = B.VID;

  name  | vorname  
--------+----------
 Yilmaz | Fatma
 Glück  | Johannes
 Clarks | Suzanne
 Yilmaz | Fatma
 Yilmaz | Fatma
 Clarks | Suzanne
(6 Zeilen)

--9.b Die Anzahl der Schüler, die im Schülercafe verkauft haben (benannt mit verkaeufer)
SELECT		COUNT(*) AS verkaeufer
FROM 		Stammkunden A ,	Verkauf B
WHERE 		A.StID = B.VID;

 verkaeufer 
------------
          6
(1 Zeile)

--9.c Eine Liste der Schüler (Name, Vorname), die im Schülercafe ein Getränk gekauft haben
SELECT		A.Name, A.Vorname
FROM 		Stammkunden A ,	Verkauf B, Getraenke G
WHERE 		A.StID = B.KID AND B.PID = G.PID;

  name   | vorname 
---------+---------
 Dickens | Charlie
 Puchert | Jürgen
 Müller  | Justin
(3 Zeilen)

--9.d Eine Liste der Schüler (Name, Vorname), die im Schülercafe ein Getränk gekauft haben mit Angabe des Getränkenamens
SELECT		A.Name, A.Vorname, P.Bezeichnung
FROM 		Stammkunden A ,	Verkauf B, Getraenke G, Produkte P
WHERE 		A.StID = B.KID AND B.PID = G.PID AND B.PID = P.PID;

  name   | vorname | bezeichnung  
---------+---------+--------------
 Dickens | Charlie | Apfelschorle
 Puchert | Jürgen  | Kaffee
 Müller  | Justin  | Kakao
(3 Zeilen)

--9.e Eine Liste der Verkäufer (Name, Vorname), an der ablesbar ist, wieviele Pausen er/sie gearbeitet hat
SELECT 		Name, Vorname, COUNT(*)
FROM		Stammkunden
NATURAL JOIN ARBEIT
GROUP BY 	Name, Vorname;

  name  | vorname  | count 
--------+----------+-------
 Glück  | Johannes |     2
 Clarks | Suzanne  |     1
 Yilmaz | Fatma    |     3
(3 Zeilen)

--9.f Der Verkäufer (Name, Vorname), der die meisten Pausen gearbeitet hat
SELECT 		Name, Vorname, COUNT(*) AS Pausen
FROM		Stammkunden
NATURAL JOIN ARBEIT
GROUP BY 	Name, Vorname
ORDER BY 	Pausen DESC
LIMIT 1;

  name  | vorname | pausen 
--------+---------+--------
 Yilmaz | Fatma   |      3
(1 Zeile)

--III. Schwierigkeitsstufe:
-- - mit having, Aggregatfunktionen und Unterabfragen

--10.a Eine Liste der Kunden (Name, Vorname) mit der Anzahl der Produkte, die sie gekauft haben
SELECT		S.Name, S.Vorname, Sum(V.VAnzahl) AS Anzahl
FROM		Stammkunden S, Verkauf V
WHERE		S.StID = V.KID
GROUP BY	S.Name, S.Vorname;

   name   | vorname | anzahl 
----------+---------+--------
 Müller   | Justin  |      1
 Andante  | Alfredo |      1
 Petersen | Richard |      2
 Meyer    | Sophie  |      1
 Puchert  | Jürgen  |      1
 Dickens  | Charlie |      1
(6 Zeilen)

--10.b Eine Liste der Kunden (Name, Vorname), die mindestens 2 Produkte gekauft haben
SELECT		S.Name, S.Vorname, Sum(V.VAnzahl) AS Anzahl
FROM		Stammkunden S, Verkauf V
WHERE		S.StID = V.KID
GROUP BY	S.Name, S.Vorname
HAVING 		Sum(VAnzahl) >= 2;

   name   | vorname | anzahl 
----------+---------+--------
 Petersen | Richard |      2
(1 Zeile)

--11.a Der Umsatz, der bisher durch das Schülercafe erreicht wurde
SELECT		SUM(Verkaufspreis * VAnzahl) AS Umsatz  --VAnzahl unbedingt beachten!!
FROM		Produkte
NATURAL JOIN Verkauf;

 umsatz 
--------
   9.20
(1 Zeile)

--11.b Eine Liste der Produkte mit Bezeichnung, Verkaufspreis, Lieferpreis und Gewinn (Verkaufspreis minus Lieferpreis)
SELECT		Bezeichnung, Verkaufspreis, Lieferpreis, Verkaufspreis - Lieferpreis AS Gewinn
FROM		Produkte
NATURAL JOIN Lieferung;

    bezeichnung    | verkaufspreis | lieferpreis | gewinn 
-------------------+---------------+-------------+--------
 Snickers          |          1.20 |        1.00 |   0.20
 Milkyway          |          1.20 |        1.00 |   0.20
 Apfelschorle      |          1.50 |        1.30 |   0.20
 Cola              |          1.70 |        1.50 |   0.20
 Kaffee            |          1.00 |        0.80 |   0.20
 Belegtes Brötchen |          2.00 |        1.50 |   0.50
 Banane            |          0.50 |        0.25 |   0.25
 Apfel             |          0.50 |        0.25 |   0.25
 Couscous-Salat    |          1.50 |        1.00 |   0.50
 Gemüsechips       |          2.00 |        1.50 |   0.50
 Kakao             |          1.00 |        0.80 |   0.20
 Käse-Sandwich     |          2.00 |        1.50 |   0.50
(12 Zeilen)

--11.c Der Gewinn, der mit den bisherigen Verkäufen erreicht wurde
SELECT SUM(A.Gewinn * VAnzahl) AS Gesamtgewinn
FROM ( SELECT	PID, Verkaufspreis - Lieferpreis AS Gewinn
		FROM	Produkte
		NATURAL JOIN Lieferung) AS A
NATURAL JOIN	Verkauf;

 gesamtgewinn 
--------------
         2.05
(1 Zeile)
