<?php
/*
 * index.php
 * 
 * Autor: Julian Weiß
 * Datum: 30.04.2019
 * Zweck: Konstruktion von Abfragen und Views für das SuSCafe
 * Didaktische Hinweise:
 * V=Vorbereitet oder als Template auszugeben
 *  I= Einstiegsaufgaben Html
 * II= Einstiegsaufgaben PHP
 *III= Fortgeschritttene HTML/PHP 
 */

?>
<!--Dekleration der Html-Bedingungen (V)-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="sw">

<!--Anbindung der CSS-Datei zur Formatierung (V)-->
<link rel="stylesheet" href="produktstyle.css">

<!--Kopf und Eigenschaften der Html-Datei (Metadaten) (V)-->
<head>
	<title>Produktsuche SuS-Café</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>

<!--Beginn des eigentlichen Dokuments (I)-->
<body>

<!--Form bezeichnet die Seite als Formular und lget die Get- oder 
	Post-Methode fest, mit der die entsprechenden Formular eingaben 
	ausgewertet werden.(I) -->
	
<form action="produktausgabe.php" method="GET">	
<!--Überschrift der Seite (I) --> 
	<h1 align="center">Produktsuche Schüler-Café</h1>
	<h2 align="center">Musterschule XYZ</h2>
	
<!--Einleitungstext (I) mit dem Button 
gelangt man zurück zur Startseite-->
<p><br>Wähle die Eigenschaften aus, die dein Produkt erfüllen soll.</p>
<button formaction="index.php">Zurück zur Startseite!</button><br><br>
<!--Hier können noch weitere Ausführungen folgen was auf der Seite getan werden kann-->

<!--Beginn des PHP-Abschnitts II-->

<?php
/* Ab hier wird eine Verknüpfung zur SQL-Datenbank hergestellt. Die Verknüpfung zur Datenbasis
 * erfolgt per "connect-Befehl". Dieser kann je nach verwendeter Datenbasis unterschiedlich sein.
 * Bei PSQL lautet der Befehl pg_connect ("host=sheep port=5432 dbname=mary user=lamb password=foo");
 * Mit Hilfe von require_once wird eine PHP-Datei (hier: 'konfiguration.php') eingeladen,
 * die die Benutzerangaben und Verbindungen enthält. Die Verbindung wird erst tatsächlich hergestellt,
 * sobald eine Anfrage an die Datenbank gestellt wird. 
 *
 * 
 * !!!!!!!!ACHTUNG: Die konfiguration.php enthält wichtige Daten, die auch Passwörter und persönliche
 * Daten enthalten - diese Datei ist unbedingt weiterzugeben!*/
 
/*!!!!!! Die Verbindung zur Datenbank sollte den SuS bereits 
 * als Vorlage zur Verfügung gestellt werden!!!!!! (V)*/

require_once ('konfiguration.php');
require_once ('conn.inc.php');
                    
//Die eigentliche SQL-Abfrage wird hier in einer Varaibele eingetragen (II)                    
$sql= "SELECT * FROM produkte";

/* Das Ergebnis wird in der Variablen db_erg gespeichert. Der Nutzer erhält
 * hier ALLE Einträge der Tabelle. (II)*/
$db_erg = $con->query( $sql );


//Überprüfen, ob es ein Ergebnis gibt. Wenn nicht: Error!
try {
  $db_erg = $con->query ( $sql );
} catch (PDOException $e) {
  // Überprüfen, ob es ein Ergebnis gibt. Wenn nicht: Error!
  echo "Fehler: " . htmlspecialchars ($e->getMessage ());
  die ();
}

/* Hier beginnt die Tabelle, diese Zeigt auf der ersten Seite alle 
 * vorhandenen Produkte.
 * Es beginnt mit <th>. Alles zwischen diesen Bezeichnern stellt die 
 * Überschrift der Tabelle dar. (V, II)*/

echo '<table>';
echo '<th'  .($i%2==0?' style="background-color:#00ddba;"':''). '> Bezeichnung Sortierung </th>';
echo '<th'  .($i%2==0?' style="background-color:#00ddba;"':''). '> Verkaufspreis </th>';
echo '<th'  .($i%2==0?' style="background-color:#00ddba;"':''). '> Allergene </th>';

/* Hier ist die Spalte der entsprechenden Filter. Wird kein Filter gewählt
 * werden standardmäßig alle Produkte ausgegeben.
 * Die entsprechenden Sortierungen sind sehr komplex. Es wird daher empfohlen,
 * jeweils nur eine Sortierung im Unterricht zu wählen. Die Sortierung kann
 * arbeitsteilig bearbeitet werden. Hier ist auch (dieser Ansatz wird vom
 * Autorenteam bevorzugt) die Methode des Reverse-Engeneering empfohlen werden.*/
 
 
 
 echo '<tr' .(' style="background-color:#b7f6ec"'). '>';
 echo '<td>' . 'Wie sollen die Daten sortiert sein?' . '<br>'.
	//Es beginnt das Dropdownmenü dieses wird mit Select gekennzeichnet
	'<select name="sortierung">	
		<option value="">Sortieren nach</option>
		<option value="ascABC">ABC aufsteigend</option>
		<option value="dscABC">ABC absteigend</option>
		<option value="ascPREIS">Preis Aufsteigend</option>
		<option value="dscPREIS">Preis Absteigend</option>
	</select>'.
  
 '</td>';//Hier ist die Tabellenzelle Sortierung erst beendet (III)
 echo '<td>' . 'Was darf dein Produkt maximal kosten?' . '<br>'. 
 '<input type="number" name="preis" min=0 value="">'.'€</input> '.
 '</td>';//Hier ist die Tabellenzelle Preis erst beendet (III)
 echo '<td>'. 'Welche Allergene dürfen nicht enthalten sein? Kreuze an!' . '<br>' . 
 '<input type="checkbox" name="lactose" value="Ja">'.'Lactose</input> '. 
 '<input type="checkbox" name="gluten" value="Ja">'.'Gluten</input> ' .
 '<input type="checkbox" name="nuesse" value="Ja">'.'Nüsse</input> '.
 '</td>';//Hier ist die Tabellenzelle Allergene erst beendet (III)
 
 echo '</tr>';
 echo '</table>';
 
?>
<!-- Mit dem Klick auf den Button öffnet sich die in <form> angegebene 
	 Seite. Diese ist in diesem Fall die Seite Produktsuche.php (II)-->
<br>
<button>Produkte filtern!</button> 
</form>

</body>

</html>
