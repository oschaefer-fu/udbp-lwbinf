<?php
/*
 * index.php
 * 
 * Autor: Julian Weiß
 * Datum: 30.04.2019
 * Zweck: Konstruktion von Abfragen und Views für das SuSCafe
 * Didaktische Hinweise:
 * V=Vorbereitet oder als Template auszugeben
 *  I= Einstiegsaufgaben Html
 * II= Einstiegsaufgaben PHP
 *III= Fortgeschritttene HTML/PHP 
 */

?>
<!--Dekleration der Html-Bedingungen (V)-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="sw">

<!--Anbindung der CSS-Datei zur Formatierung (V)-->
<link rel="stylesheet" href="produktstyle.css">

<!--Kopf und Eigenschaften der Html-Datei (Metadaten) (V)-->
<head>
	<title>Produktsuche SuS-Café</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>

<!--Beginn des eigentlichen Dokuments (I)-->
<body>

<!--Form bezeichnet die Seite als Formular und legt die Get- oder 
	Post-Methode fest, mit der die entsprechenden Formular eingaben 
	ausgewertet werden.(I) -->
	
<form action="produktsuche.php" method="GET">	
<!--Überschrift der Seite (I) --> 
	<h1 align="center">Produktsuche Schüler-Café</h1>
	<h2 align="center">Musterschule XYZ</h2>
	
<!--Einleitungstext (I)-->
<p><br>Hier sind alle Produkte angezeigt, die im Schüler-Café angeboten werden.</p>
<p>Mit dem Klick auf den Button gelangt man zur Produktsuche.<br></p>
<!--Hier können noch weitere Ausführungen folgen was auf der Seite getan werden kann-->

<!-- Mit dem Klick auf den Button öffnet sich die in <form> angegebene 
	 Seite. Diese ist in diesem Fall die Seite Produktsuche.php-->
<button>Ich möchte Produkte filtern!</button><br><br>
<!--Beginn des PHP-Abschnitts II-->

<?php
/* Ab hier wird eine Verknüpfung zur SQL-Datenbank hergestellt. Die Verknüpfung zur Datenbasis
 * erfolgt per "connect-Befehl". Dieser kann je nach verwendeter Datenbasis unterschiedlich sein.
 * Bei PSQL lautet der Befehl pg_connect ("host=sheep port=5432 dbname=mary user=lamb password=foo");
 * Mit Hilfe von require_once wird eine PHP-Datei (hier: 'konfiguration.php') eingeladen,
 * die die Benutzerangaben und Verbindungen enthält. Die Verbindung wird erst tatsächlich hergestellt,
 * sobald eine Anfrage an die Datenbank gestellt wird. 
 *
 * 
 * !!!!!!!!ACHTUNG: Die konfiguration.php enthält wichtige Daten, die auch Passwörter und persönliche
 * Daten enthalten - diese Datei ist unbedingt weiterzugeben!*/
 
/*!!!!!! Die Verbindung zur Datenbank sollte den SuS bereits 
 * als Vorlage zur Verfügung gestellt werden!!!!!! (V)*/

require_once ('konfiguration.php');
require_once ('conn.inc.php');
                    
//Die eigentliche SQL-Abfrage wird hier in einer Varaibele eingetragen (II)                    
$sql= "SELECT   *
       FROM     produkte
       ORDER BY bezeichnung ASC";

/* Das Ergebnis wird in der Variablen db_erg gespeichert. Der Nutzer erhält
 * hier ALLE Einträge der Tabelle. (II)*/
try {
  $db_erg = $con->query ( $sql );
} catch (PDOException $e) {
  // Überprüfen, ob es ein Ergebnis gibt. Wenn nicht: Error!
  echo "Fehler: " . htmlspecialchars ($e->getMessage ());
  die ();
}

/* Hier beginnt die Tabelle, diese Zeigt auf der ersten Seite alle 
 * vorhandenen Produkte.
 * Es beginnt mit <th>. Alles zwischen diesen Bezeichnern stellt die 
 * Überschrift der Tabelle dar. (V, II)*/

echo '<table>';
echo '<th'  .($i%2==0?' style="background-color:#00ddba;"':''). '> Bezeichnung </th>';
echo '<th'  .($i%2==0?' style="background-color:#00ddba;"':''). '> Verkaufspreis </th>';
echo '<th'  .($i%2==0?' style="background-color:#00ddba;"':''). '> Allergene </th>';


/* Die Anfrage wird in das Array $db_erg geladen. Die Ergebnisse befinden
 * sich somit hintereinander sortiert in einem "Feld". Die Foreach-
 * Schleife geht das Array durch und gibt jeweils die entsprechenden 
 * Werte in je einer Zelle zurück. Dies geschieht angebunden, an die 
 * entsprechenden Attribut-Überschriften. (V, II)*/
 
 //Die Variable $i ist mit 1 intitalisiert und zählz die Zeilen der Tabelle (II)

  $i=1;
foreach ($db_erg as $row) {
if ($row['lactose']==1){$lac='Ja  ';}else {$lac= 'Nein';}	
if ($row['Gluten']==1){$glu='Ja  ';}else {$glu= 'Nein';}		
if ($row['nuesse']==1){$nue='Ja  ';}else {$nue= 'Nein';}
	//Hier wird jeweils jede 2. Zeile mit einer anderen 
	//Hintergrundfarbe belegt.
  echo '<tr' .($i%2==0?' style="background-color:#b7f6ec;"':'').'>';
  echo  '<td>' . $row['bezeichnung'] . '</td>';
  echo	'<td>' . $row['verkaufspreis'] . '€' . '</td>';
  echo	'<td>' . 'Lactose: '.$lac . ' Gluten: '. $glu . ' Nüsse: ' . $nue . '</td>';
  echo '</tr>';
  $i++; //Hier wird die Zeile gezählt (II)
}
echo '</table>'; 

//Hier endet die Tabelle.
$a=0;

?>

</form>

</body>

</html>
