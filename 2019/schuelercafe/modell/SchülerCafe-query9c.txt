--9.c Eine Liste der Schüler (Name, Vorname), die im Schülercafe ein Getränk gekauft haben
SELECT		A.Name, A.Vorname
FROM 		Stammkunden A ,	Verkauf B, Getraenke G
WHERE 		A.StID = B.KID AND B.PID = G.PID;