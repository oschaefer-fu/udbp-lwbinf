--9.e Eine Liste der Verkäufer (Name, Vorname), an der ablesbar ist, wieviele Pausen er/sie gearbeitet hat
SELECT 		Name, Vorname, COUNT(*)
FROM		Stammkunden
NATURAL JOIN ARBEIT
GROUP BY 	Name, Vorname;