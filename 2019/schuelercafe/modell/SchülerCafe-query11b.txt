--11.b Eine Liste der Produkte mit Bezeichnung, Verkaufspreis, Lieferpreis und Gewinn (Verkaufspreis minus Lieferpreis)
SELECT		Bezeichnung, Verkaufspreis, Lieferpreis, Verkaufspreis - Lieferpreis AS Gewinn
FROM		Produkte
NATURAL JOIN Lieferung;