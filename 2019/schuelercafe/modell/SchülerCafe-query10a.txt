--10.a Eine Liste der Kunden (Name, Vorname) mit der Anzahl der Produkte, die sie gekauft haben
SELECT		S.Name, S.Vorname, Sum(V.VAnzahl) AS Anzahl
FROM		Stammkunden S, Verkauf V
WHERE		S.StID = V.KID
GROUP BY	S.Name, S.Vorname;