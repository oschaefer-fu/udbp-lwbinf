--10.b Eine Liste der Kunden (Name, Vorname), die mindestens 2 Produkte gekauft haben
SELECT		S.Name, S.Vorname, Sum(V.VAnzahl) AS Anzahl
FROM		Stammkunden S, Verkauf V
WHERE		S.StID = V.KID
GROUP BY	S.Name, S.Vorname
HAVING 		Sum(VAnzahl) >= 2;