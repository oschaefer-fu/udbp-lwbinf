--9.b Die Anzahl der Schüler, die im Schülercafe verkauft haben (benannt mit verkaeufer)
SELECT		COUNT(*) AS verkaeufer
FROM 		Stammkunden A ,	Verkauf B
WHERE 		A.StID = B.VID;