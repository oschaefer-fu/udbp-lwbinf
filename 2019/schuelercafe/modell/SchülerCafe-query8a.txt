--8.a Eine Liste von Produkten (PID, Bezeichnung) der Getränke, die mit Pfand verkauft werden
SELECT 		PID, Bezeichnung
FROM		Getraenke
NATURAL JOIN Produkte
WHERE		Pfand > 0;