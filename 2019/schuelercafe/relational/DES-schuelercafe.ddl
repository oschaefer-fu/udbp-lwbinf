/sql
/multiline    on
/type_casting on

--Miniwelt Schuelercafe
--erstellt von T.Gerl, A.Jahn, M.Stark, J.Weiss


--Personen
CREATE TABLE Stammkunden (
	StID					INTEGER   		NOT NULL	PRIMARY KEY, 
	Name					VARCHAR(100)		NOT NULL, 
	Vorname					VARCHAR(100)		NOT NULL,
	GebDatum				DATE			NOT NULL,
	Registrierungsdatum			DATE			NOT NULL,
	Abmeldedatum				DATE			NULL
) ;
	
CREATE TABLE Schueler (
	StID					INTEGER			NOT NULL 	REFERENCES Stammkunden(StID),
	TelNr					VARCHAR(12)		NOT NULL,
	Klasse					VARCHAR(3)		NOT NULL
) ;

CREATE TABLE Lehrer	(
	StID					INTEGER			NOT NULL	REFERENCES Stammkunden(StID),
	Klassenlehrer				VARCHAR(3)		NULL      
) ;


--Produktangebot
CREATE TABLE Produkte (
	PID					INTEGER			NOT NULL	PRIMARY KEY,
	Bezeichnung				VARCHAR(250)		NOT NULL,
	Verkaufspreis				FLOAT			NOT NULL,
	Lactose					INTEGER,		
	Gluten					INTEGER,		
	Nuesse					INTEGER		
) ;

CREATE TABLE Snacks	(
	PID					INTEGER			NOT NULL	REFERENCES Produkte(PID),
	Zubereitungszeit			INTEGER			NULL
) ;

CREATE TABLE Getraenke (
	PID					INTEGER			NOT NULL	REFERENCES Produkte(PID), 
	Pfand					FLOAT			NULL,
	Koffeinhaltig				INTEGER		
) ;


--Verkauf und Arbeit
CREATE TABLE Verkauf (
	KID					INTEGER			NOT NULL	  REFERENCES Stammkunden(StID), 
	PID					INTEGER			NOT NULL   REFERENCES Produkte(PID),          
	VID					INTEGER			NOT NULL  	REFERENCES Stammkunden(StID),
	VUhrzeit				TIME			NOT NULL,
	VDatum					DATE			NOT NULL,
	VAnzahl					INTEGER
) ;

CREATE TABLE Arbeitszeiten	(
	AID					INTEGER			NOT NULL	PRIMARY KEY,			
	UhrzeitVonBis				VARCHAR(11)		NOT NULL
) ;
	
CREATE TABLE Arbeit	(
	StID					INTEGER			NOT NULL 	REFERENCES Stammkunden(StID),  
	AID					INTEGER			NOT NULL	 REFERENCES Arbeitszeiten(AID),
	ADatum					DATE			NOT NULL
) ;
	

--Lieferanten	
CREATE TABLE Lieferanten (
	LID					INTEGER			NOT NULL	PRIMARY KEY,
	LName					VARCHAR(200)		NOT NULL  
) ;

CREATE TABLE Lieferung (
	PID					INTEGER			NOT NULL	REFERENCES Produkte(PID),   
	LID					INTEGER			NOT NULL	REFERENCES Lieferanten(LID),
	LAnzahl					INTEGER			NOT NULL, 
	LDatum					DATE			NOT NULL,
	Lieferpreis				FLOAT			NOT NULL 
) ;

--Miniwelt Schuelercafe
--erstellt von T.Gerl, A.Jahn, M.Stark, J.Weiss


--Personen---------------------------------------------------------------------

--Stammkunden
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (1,'Meier','Lisa', date '2003-08-05',date '2019-08-05',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (2,'Petersen','Richard', date '1980-08-20',date '2019-08-05',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (3,'Andante','Alfredo', date '2007-05-03',date '2019-08-05','2019-08-07');
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (4,'Yilmaz','Fatma', date '2005-12-31',date '2019-08-05',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (5,'Glück','Johannes', date '2004-05-16',date '2019-08-05',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (6,'Dickens','Charlie', date '2006-11-21',date '2019-08-06',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (7,'Clarks','Suzanne', date '2005-03-14',date '2019-08-06',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (8,'Meyer','Sophie', date '2007-10-27',date '2019-08-06',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (9,'Bordeaux','Marie', date '2007-08-16',date '2019-08-06','2019-12-11');
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (10,'Müller','Justin', date '2006-12-24',date '2019-08-06',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (11,'Hut','Barbara', date '1975-02-28',date '2019-08-06',NULL);
INSERT INTO Stammkunden (StID,Name,Vorname,GebDatum,Registrierungsdatum,Abmeldedatum) VALUES (12,'Puchert','Jürgen', date '1972-06-08',date '2019-08-06',NULL);

--Schüler
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (1,'11','017518475632');
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (3,'7a','017648162930');
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (4,'9b','017593479404');
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (5,'10a','017854028561');
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (6,'8a','017836451937');
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (7,'9b','017756398204');
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (8,'7b','017898859021');
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (9,'7a','017577759434');
INSERT INTO Schueler (StID,Klasse,TelNr) VALUES (10,'8a','017824223257');

--Lehrer
INSERT INTO Lehrer (StID,Klassenlehrer) VALUES (2,'8b');
INSERT INTO Lehrer (StID,Klassenlehrer) VALUES (11,'10a');
INSERT INTO Lehrer (StID,Klassenlehrer) VALUES (12,'7a');


--Produktangebot--------------------------------------------------------------

--Produkte
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (1,'Snickers', 1.20, 1, 0, 1);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (2,'Milkyway', 1.20, 1, 0, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (3,'Apfelschorle', 1.50, 0, 0, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (4,'Cola', 1.70, 0, 0, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (5,'Kaffee', 1.00, 0, 0, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (6,'Belegtes Brötchen', 2.00, 1, 1, 1);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (7,'Banane', 0.50, 0, 0, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (8,'Apfel', 0.50, 0, 0, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (9,'Couscous-Salat', 1.50, 0, 1, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (10,'Gemüsechips', 2.00, 0, 0, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (11,'Kakao', 1.00, 1, 0, 0);
INSERT INTO Produkte (PID,Bezeichnung,Verkaufspreis,Lactose, Gluten, Nuesse) VALUES (12,'Käse-Sandwich', 2.00, 1, 1, 0);

--Snacks
INSERT INTO Snacks (PID,Zubereitungszeit) VALUES (1,0);
INSERT INTO Snacks (PID,Zubereitungszeit) VALUES (2,0);
INSERT INTO Snacks (PID,Zubereitungszeit) VALUES (6,0);
INSERT INTO Snacks (PID,Zubereitungszeit) VALUES (7,0);
INSERT INTO Snacks (PID,Zubereitungszeit) VALUES (8,0);
INSERT INTO Snacks (PID,Zubereitungszeit) VALUES (9,0);
INSERT INTO Snacks (PID,Zubereitungszeit) VALUES (12,3);

--Getränke
INSERT INTO Getraenke (PID,Pfand,Koffeinhaltig) VALUES (3,0.25,0);
INSERT INTO Getraenke (PID,Pfand,Koffeinhaltig) VALUES (4,0.25,1);
INSERT INTO Getraenke (PID,Pfand,Koffeinhaltig) VALUES (5,0.00,1);
INSERT INTO Getraenke (PID,Pfand,Koffeinhaltig) VALUES (11,0.00,0);


--Lieferanten und Lieferung------------------------------------------------------

--Lieferanten
INSERT INTO Lieferanten (LID,LName) VALUES (1,'Decca Food Service');
INSERT INTO Lieferanten (LID,LName) VALUES (2,'Black Box Delivery');

--Lieferung
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (1,1,100,'2019-08-02',1.00);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (2,1,100,'2019-08-02',1.00);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (3,1,200,'2019-08-02',1.30);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (4,1,200,'2019-08-02',1.50);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (5,2,300,'2019-08-02',0.80);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (6,2,100,'2019-08-05',1.50);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (7,2,100,'2019-08-05',0.25);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (8,2,100,'2019-08-05',0.25);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (9,2,100,'2019-08-05',1.00);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (10,1,100,'2019-08-02',1.50);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (11,2,100,'2019-08-02',0.80);
INSERT INTO Lieferung (PID,LID,LAnzahl,LDatum,Lieferpreis) VALUES (12,2,50,'2019-08-02',1.50);

--Verkauf und Arbeit-----------------------------------------------------------

--Verkauf
INSERT INTO Verkauf (KID,PID,VID,VUhrzeit,VDatum,VAnzahl) VALUES (3,1,4,time '09:35:42','2019-08-05',1);
INSERT INTO Verkauf (KID,PID,VID,VUhrzeit,VDatum,VAnzahl) VALUES (12,5,5,time '11:37:13','2019-08-06',1);
INSERT INTO Verkauf (KID,PID,VID,VUhrzeit,VDatum,VAnzahl) VALUES (6,3,7,time '09:40:19','2019-08-06',1);
INSERT INTO Verkauf (KID,PID,VID,VUhrzeit,VDatum,VAnzahl) VALUES (2,6,4,time '09:42:40','2019-08-05',2);
INSERT INTO Verkauf (KID,PID,VID,VUhrzeit,VDatum,VAnzahl) VALUES (8,8,4,time '11:34:32','2019-08-06',1);
INSERT INTO Verkauf (KID,PID,VID,VUhrzeit,VDatum,VAnzahl) VALUES (10,11,7,time '09:41:54','2019-08-06',1);

--Arbeitszeiten
INSERT INTO Arbeitszeiten (AID,UhrzeitVonBis) VALUES (1,'9.30-10.00');
INSERT INTO Arbeitszeiten (AID,UhrzeitVonBis) VALUES (2,'11.30-12.00');
INSERT INTO Arbeitszeiten (AID,UhrzeitVonBis) VALUES (3,'13.30-14.00');

--Arbeit
INSERT INTO Arbeit (StID,AID,ADatum) VALUES (4,1,'2019-08-05');
INSERT INTO Arbeit (StID,AID,ADatum) VALUES (5,2,'2019-08-05');
INSERT INTO Arbeit (StID,AID,ADatum) VALUES (5,3,'2019-08-05');
INSERT INTO Arbeit (StID,AID,ADatum) VALUES (7,1,'2019-08-06');
INSERT INTO Arbeit (StID,AID,ADatum) VALUES (4,2,'2019-08-06');
INSERT INTO Arbeit (StID,AID,ADatum) VALUES (4,3,'2019-08-06');
